"""
Initialization of tt_common module to use a LOG_LEVEL environment
value to control level of log details written.

If no value specified, or value is not a supported tt_common level name, INFO
level is used.
"""
import logging
import os

# The tt_common level to use when not specified by LOG_LEVEL env value
default_log_level = "INFO"
env_log_level = os.getenv("LOG_LEVEL", default_log_level).upper()
log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"


class Logging:

    @staticmethod
    def get_numeric_log_level(log_level):
        """
        Obtain the numeric value corresponding to the log_level name provided, defaulting
        to internal default value if an unknown name.

        :param log_level: str - name of log level to obtain numeric value of
        :return: int - log level corresponding to log_level or default level if an unknown name
        """
        numeric_level = getattr(logging, str(log_level).upper(), None)
        if not isinstance(numeric_level, int):
            numeric_level = getattr(logging, default_log_level, logging.INFO)
            print("Invalid log level '{}'; using '{}' level".format(log_level, logging.getLevelName(numeric_level)))

        return numeric_level

    def set_log_level(self, log_level):
        """
        Set tt_common level for root logger.
        Will force configurations on to the root logger if previously initialized.

        :param log_level: str - name of log level to set to root logger
        :return: No return value
        """
        numeric_level = self.get_numeric_log_level(log_level)
        rootlogger = logging.getLogger(None)
        rootlogger.setLevel(numeric_level)
        # logging.basicConfig(level=numeric_level, format=log_record_format, force=True)

    def init_logging(self):
        """
        Read the environment LOG_LEVEL and initialize tt_common module.
        Will use INFO level if no environment value specified or is not
        a valid tt_common level name.

        :return: No return value
        """
        self.set_log_level(env_log_level)
