"""
Unit tests for common_logging initialization
"""
import logging

from tt_common.tt_logging import Logging


def test_init_logger(monkeypatch):
    log_level = "CRITICAL"
    monkeypatch.setattr("tt_common.tt_logging.env_log_level", log_level)
    Logging().init_logging()
    logger = logging.getLogger()
    logger.debug("Testing debug message, should not see this.")
    logger.critical("Testing critical message, should see this.")
    assert logging.getLevelName(logger.getEffectiveLevel()) == log_level


def test_get_numeric_log_level():
    # will default to INFO if name not known
    unknown_result = Logging().get_numeric_log_level("unknown")
    assert unknown_result == logging.INFO

    # will obtain name level when valid
    debug_result = Logging().get_numeric_log_level("debug")
    assert debug_result == logging.DEBUG