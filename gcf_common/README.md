# TechTarget Google Cloud Function Common Project

This project provides common functionality to TechTarget's google cloud functions. The source is installed and 
distributed as a Python Wheel (see reference section) to be included in the cloud function source.

## Project Layout

|Directory                       | Description                                          |
|--------------------------------|------------------------------------------------------|
| setup.py                       | Build script for setuptools                          |
| src/tt_common                  | Project source code                                  |
| src/test                       | Pytest test code                                     |
| src/test/resources             | Test configurations                                  |
| build                          | Build output (created by Python)                     |
| dist                           | Final built distribution output (created by Python)  |
| venv                           | Virtual environment (created by PyCharm)             |

## Package Contents

The build script (setup.py) used by setuptools will generate a package for each directory found in /src/tt_common
and is accessible in the .whl file.

## Package Creation

Run from /gcp_inboundconverter/gcf_common
```
python3 setup.py bdist_wheel
```
This .whl file generated is placed in /gcf_common/dist

## Package Usage

To use the common package in a cloud function:
1. Build the Python Wheel 
```
python3 setup.py bdist_wheel
```
2. Copy /dist/gcf_common-0.0.1-py3-none-any.whl to /ref in the cloud function source
3. Add the following to requirements.txt in the cloud function source
```
./ref/gcf_common-0.0.1-py3-none-any.whl 
```
4. Force update of the requirements by running: 
```
pip install -r requirements.txt --force-reinstall
```
5. Import the desired package in cloud function source, e.g. 
```
from tt_common.tt_logging import Logging
```

## Package Versioning

The version of the package is included in the file name and is updated in setup.py

## Development Setup
The following instructions use PyCharm as IDE and utilizing a virtual environment (venv)
for isolating python dependencies.  A clean checkout with no python cache folders,
no .idea files at main level or in cloud function folders, and no venv folders
from previous startups.

### IDE Setup
1. Start PyCharm
2. File->New Project...
3. Modify the Location selection to the cloud function folder (gcp_inboundconverter/gcf_common)
4. Be sure New environment using is set to Virualenv
5. The Location of New environment will be in gcf_common/venv
6. Base interpreter is set to a Python 3.7+ reference
7. Inherit global site-packages is not checked
8. Make available to all projects is not checked
9. Create a main.py welcome script is not checked
10. Click Create
11. Confirm dialog - Create from Existing Sources
12. Another dialog may appear if an existing project was open when New Project was started.
    Use a New Window is choice made during write-up.
13. Dialog will appear - Creating Virtual Environment
14. After dialog closes and virtual environment is initialized, open requirements.txt file
15. Banner at top will indicate package requirements are missing - choose Install requirements
16. Dialog should have all components selected for packages to install - click Install
17. Package installation progress can be followed in bottom bar of project window
18. When package installation is complete - open test_nlp_server.py (if in ctax_cf - or other cf test)
19. If green arrow for 'run' is not present, project is not configured to use pytest
    - Go to File-Settings-Python Integrated Tools
    - Change Testing entry to pytest
20. Run test to confirm success.

## Reference

What Is a Python Wheel?
A Python .whl file is essentially a ZIP (.zip) archive with a specially crafted filename that tells installers what 
Python versions and platforms the wheel will support. A wheel is a type of built distribution. In this case, built 
means that the wheel comes in a ready-to-install format and allows you to skip the build stage required with 
source distributions.

Note: It’s worth mentioning that despite the use of the term built, a wheel doesn’t contain .pyc files, or compiled 
Python bytecode.

A wheel filename is broken down into parts separated by hyphens:

```{dist}-{version}(-{build})?-{python}-{abi}-{platform}.whl```

Each section in {brackets} is a tag, or a component of the wheel name that carries some meaning about what the wheel 
contains and where the wheel will or will not work.

[Learn More](https://realpython.com/python-wheels/)