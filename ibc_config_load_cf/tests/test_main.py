import json
import pytest

from unittest.mock import patch

import main
from tests.lib.gcp import firestore
from datetime import datetime, timezone

# This is a test that connects to real Firestore and BigQuery DBs in the current active Google
# project configured in your environment. It should only be run manually
@pytest.mark.skip(reason="this test has to be run manually")
def test_load_config_manual():
    print("Testing load_config() manually ...")
    main.load_config()


# TODO add a full test with mocked Firestore and BigQuery - and test for happy path and Exceptions from each


"""
Test getting empty lists from both BQ and Firestore
Expected result: no update required
"""
def test_empty_both_lists():
    expired_cids_list_from_bq = []
    expired_cids_list_from_firestore = []
    need_to_update, expired_cids_list = main.get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore)
    assert need_to_update is False
    assert not expired_cids_list


"""
Test getting empty list from BQ and not empty from Firestore
Expected result: update required, to empty list
"""
def test_empty_BQ_list():
    expired_cids_list_from_bq = []
    expired_cids_list_from_firestore = [111,222,333]
    need_to_update, expired_cids_list = main.get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore)
    assert need_to_update is True
    assert expired_cids_list == expired_cids_list_from_bq


"""
Test getting empty list from Firestore and not empty from BQ
Expected result: update required, to BQ list
"""
def test_empty_firestore_list():
    expired_cids_list_from_bq = [111,222,333]
    expired_cids_list_from_firestore = []
    need_to_update, expired_cids_list = main.get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore)
    assert need_to_update is True
    assert expired_cids_list == expired_cids_list_from_bq


"""
Test getting not empty lists from both BQ and Firestore, with the same CIDs but in different order
Expected result: no update required
"""
def test_both_lists_same_unordered():
    expired_cids_list_from_bq = [111,222,333]
    expired_cids_list_from_firestore = [222,111,333]
    need_to_update, expired_cids_list = main.get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore)
    assert need_to_update is False
    assert not expired_cids_list


"""
Test getting not empty lists from both BQ and Firestore, BQ list is different
Expected result: update required, to BQ list
"""
def test_both_lists_same_unordered():
    expired_cids_list_from_bq = [111,333,444]
    expired_cids_list_from_firestore = [222,111,333]
    need_to_update, expired_cids_list = main.get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore)
    assert need_to_update is True
    assert expired_cids_list == expired_cids_list_from_bq
