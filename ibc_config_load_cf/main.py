"""
Helper file to load the config to Firestore
"""
import logging
import uuid
from datetime import datetime, timezone
from google.cloud import firestore, bigquery


logger = logging.getLogger(__name__)

# TODO add periodic cleanup of old documents in the Firestore

def load_config(event, context):
    # ignoring the event and context - it is needed as a trigger only
    try:
        expired_cids_list_from_bq = get_expired_cids_from_bq()
        expired_cids_list_from_firestore = get_expired_cids_from_firestore()
        need_to_update, expired_cids_list = get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore)
        if not need_to_update:
            logger.info("List of expired CIDs does NOT have to be updated - exiting")
            return

        # we are here because we need to upload a new list of expired CIDs (could be empty as well)
        fs_client = firestore.Client()
        collection_name = 'config_donotprocess_cids'
        # datetime has to be in UTC for the doc_id
        now_datetime_local = datetime.now()
        now_datetime_utc = datetime.utcnow()
        now_timestamp = now_datetime_local.timestamp()
        now_timestamp_millis = int(now_timestamp * 1000)
        datetime_utc_str = now_datetime_local.astimezone(timezone.utc).strftime('%Y-%m-%d_%H%M%S_%Z')

        document_id = datetime_utc_str + '_' + str(uuid.uuid4())
        document_body = {'cids': expired_cids_list,
                         'created_at_timestamp_ms': now_timestamp_millis,
                         'created_at_datetime_utc': now_datetime_utc,
                         'created_at_datetime_utc_str': datetime_utc_str}
        logger.info("Uploading new list of expired CIDs to Firestore: document_id={}, document_body={}".format(
            document_id, document_body))

        fs_client.collection(collection_name).document(document_id).set(document_body)
        logger.info("uploaded document_id={} OK".format(document_id))
    except Exception as e:
        logger.exception("config_loading CF: Exception in load_config(): ERROR: {}".format(str(e)))


def get_expired_cids_list(expired_cids_list_from_bq, expired_cids_list_from_firestore):
    # Figure out if the list in Firestore needs to be updated
    # if both lists are empty - no need to do anything, the latest config in Firestore is correct
    if not expired_cids_list_from_bq and not expired_cids_list_from_firestore:
        logger.info("both BQ and Firestore lists are empty - no need to update")
        return False, None

    # if the BQ list is empty but Firestore is not - upload an empty list as the latest config into Firestore
    if not expired_cids_list_from_bq:
        return True, []

    # if the Firestore list is empty - but BQ list is not - upload the list from BQ
    if not expired_cids_list_from_firestore:
        return True, expired_cids_list_from_bq

    # both lists are non-empty - compare them to decide if the one in Firestore has to be updated
    logger.info("Retrieved non-empty lists of expired CIDs from Firestore (size={}) and BQ (size={})".format(
        len(expired_cids_list_from_firestore), len(expired_cids_list_from_bq)
    ))
    expired_cids_list_from_bq.sort()
    expired_cids_list_from_firestore.sort()
    if expired_cids_list_from_firestore == expired_cids_list_from_bq:
        # the lists are the same - no need to update
        return False, []
    else:
        # there are differences - upload the latest version from BQ
        return True, expired_cids_list_from_bq


def get_expired_cids_from_firestore():
    # TODO figure out size limits of a document in the Firestore collection
    # load the latest donotprocess client ids list from Firestore
    donotprocess_cids_list = None
    firestore_collection = "config_donotprocess_cids"
    fs_client = firestore.Client()
    collection_ref = fs_client.collection(firestore_collection)

    # find the latest document with donotprocess CIDs - based on the created_at timestamp field
    query = collection_ref.order_by('created_at_timestamp_ms', direction=firestore.Query.DESCENDING) \
        .limit(1)
    results = query.stream()
    # stream() returns a list of results, even though in our case there will be only one result
    for document in results:
        doc_as_dict = document.to_dict()
        created_at_datetime_utc_str = doc_as_dict.get('created_at_datetime_utc_str')
        logger.debug("Retrieved donotprocess_cids from Firestore: created_at_datetime_utc_str={}, doc={}".format(
            created_at_datetime_utc_str, doc_as_dict))
        donotprocess_cids_list = doc_as_dict.get('cids')

    return donotprocess_cids_list


def get_expired_cids_from_bq():
    bq_client = bigquery.Client()
    original_query_sql = \
        """
        select distinct
            client_access_id,
            client_access_key,
            expired_client_company.aiv_company_id as client_id,
            company.company_domain as domain,
            date_start,
            date_end
        from
            analytics_pe.client_subscription expired,
            analytics_activity.company,
            unnest(client_company) expired_client_company
        where
            expired_client_company.product_type='WEBSITE_TRACKING'
            and expired_client_company.aiv_company_id = company.aiv_company_id
            and not exists (
                select 1 from analytics_pe.client_subscription valid, unnest(client_company) valid_client_company
                where
                    expired_client_company.aiv_company_id = valid_client_company.aiv_company_id
                    and valid_client_company.product_type='WEBSITE_TRACKING'
                    and date_start <= current_timestamp()
                    and date_end >= timestamp_sub(current_timestamp(), interval 90 day))
        """

    query_view_sql = \
        """
        select distinct client_id
        from
            analytics_inboundconverter.expired_cids
        """
    logger.debug("Running the query to BQ: {}".format(query_view_sql))
    query_job = bq_client.query(query_view_sql)
    results = query_job.result()  # Waits for job to complete
    expired_cids_list = []
    for row in results:
        expired_cids_list.append(row.client_id)
    logger.debug("Retrieved from BQ: expired_cids_list={}".format(expired_cids_list))
    return expired_cids_list


if __name__ == "__main__":
    #expired_cids_list = get_expired_cids_from_bq()
    #print("Retrieved from BQ: expired_cids_list={}".format(expired_cids_list))
    load_config(None)