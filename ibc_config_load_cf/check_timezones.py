"""
Helper script to test datetime with timezones and formatting
"""
import logging
from datetime import datetime, timezone

from tt_common.tt_logging import Logging

Logging().init_logging()
logger = logging.getLogger(__name__)

def check_time_info():
    now_utc_datetime = datetime.utcnow()
    now_utc_timestamp = now_utc_datetime.timestamp()
    now_local_datetime = datetime.now()
    now_local_timestamp = now_local_datetime.timestamp()
    now_timestamp_millis = int(now_local_timestamp * 1000)
    datetime_utc_with_tz_str = now_utc_datetime.astimezone(timezone.utc).strftime('%Y-%m-%d_%H%M%S_%Z')
    datetime_utc_no_tz_str = now_utc_datetime.strftime('%Y-%m-%d_%H%M%S_%Z')
    datetime_local_no_tz_str = now_local_datetime.strftime('%Y-%m-%d_%H%M%S_%Z')
    datetime_local_with_tz_str = now_local_datetime.astimezone(timezone.utc).strftime('%Y-%m-%d_%H%M%S_%Z')

    # now test creation of date objects from timestamp in milliseconds
    datetime_utc_from_ms = datetime.fromtimestamp(now_timestamp_millis / 1000.0, tz=timezone.utc)

    print("time info:\n\t now_utc_datetime={}\n\t now_utc_timestamp={}\n\t now_local_datetime={}\n\t "
          "now_local_timestamp={}\n\t now_timestamp_millis={}\n\t datetime_utc_with_tz_str={}\n\t "
          "datetime_utc_no_tz_str={}\n\t datetime_local_no_tz_str={}\n\t datetime_local_with_tz_str={}\n\t "
          "datetime_utc_from_ms={}"
          .format(now_utc_datetime, now_utc_timestamp, now_local_datetime, now_local_timestamp, now_timestamp_millis,
                  datetime_utc_with_tz_str, datetime_utc_no_tz_str, datetime_local_no_tz_str, datetime_local_with_tz_str,
                  datetime_utc_from_ms))


if __name__ == "__main__":
    check_time_info()