"""
Unit tests for kickfire_insert
"""
import pytest
import logging
import helpers.kickfire_insert as kickfire_insert
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "data_revision, expected", [
        ("Q2-2020-T1", "kickfire-Q2-2020-T1"),
        ("Q1-2121", "kickfire-Q1-2121")
    ]
)
def test_get_collection_name_from_revision(data_revision, expected):
    result = kickfire_insert.get_collection_name_from_revision(data_revision)
    assert result == expected


@pytest.mark.parametrize(
    "website, start_ip, end_ip, expected", [
        # no forward slashes in doc keys
        ("website.com/extra", 15934222, 15934300, "website.comextra_15934222_15934300"),
        # various other tests of concat sequence
        ("somesite.net", "4567", "89555", "somesite.net_4567_89555")
    ]
)
def test_get_document_key(website, start_ip, end_ip, expected):
    record = {
        "Website": website,
        "start_ip_int": int(start_ip),
        "end_ip_int": int(end_ip)
    }
    result = kickfire_insert.get_document_key(record)
    assert result == expected
