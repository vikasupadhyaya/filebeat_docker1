"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
import helpers.kickfire_insert as kickfire_insert
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test requiring special setup")
def test_get_staging_collection_for_completion_check():
    """
    Find a record that exists in innovation kickfire collection and confirm
    1) return value is expected for indicating exists
    2) ensure both a correct true and false result is confirmed

    :return: None
    """
    config.innovation_configs()
    kickfire_insert.ensure_initialized()

    data_revision = "Q2-2021-2m"
    records = []
    record = {
        "Website": "0-ff.net",
        "start_ip_int": 628669783,
        "end_ip_int": 628669783,
        "is_isp": False,
        "is_wifi": True
    }
    records.append(record)
    already_exists = kickfire_insert.records_exist(data_revision, records)
    logger.info("exists: {} - record: {}".format(already_exists, record))
