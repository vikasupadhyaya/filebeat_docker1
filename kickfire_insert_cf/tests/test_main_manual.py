"""
Manual tests used during development
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from main import add_to_firestore

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)

@pytest.mark.skip(reason="manual test")
def test_add_records():
    """
    Add the records to specified firestore collection - kickfire-<revision>

    Requires the 'shards' records to be created:
    "ibc-cf-configurations/kickfire-config/data-loads/" + str(data_revision) + "/shards/" + str(shard)

    Can run from the kickfire_load_cf/tests/helpers/test_shards_helper_manual:test_initialize_shards()

    :return: None
    """
    config.innovation_configs()

    insert_event = {
        "attributes": {
                "REVISION": "Q1-2020-manual-test",
                "SHARD": 0,
                "SHARD_BATCH_ID": 2130,
                "RECORDS": [
                    {
                        "Website": "abc.com",
                        "start_ip_int": 2123,
                        "end_ip_int": 2456,
                        "is_isp": False,
                        "is_wifi": True
                    },
                    {
                        "Website": "abcd.com",
                        "start_ip_int": 1123,
                        "end_ip_int": 1456,
                        "is_isp": True,
                        "is_wifi": True
                    },
                    {
                        "Website": "abcde.com",
                        "start_ip_int": 123000,
                        "end_ip_int": 456999,
                        "is_isp": True,
                        "is_wifi": False
                    }
                ]
          }
        }

    add_to_firestore(insert_event, None)
    logger.info("done")
