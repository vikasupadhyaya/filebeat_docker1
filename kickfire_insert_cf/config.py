"""
Centralized location for configuration options specific to the kickfire processing.

Startup Parameters
PROJECT - stored as GCP_PROJECT in cf_config dictionary
CF_ENV - Necessary when deployed as cloud function with value True
       - to control whether cloud storage or local file access is used

Common configuration options are expected to be present in config based dictionary definitions
for passing to common routines that take in a config object for operation.
GCP_PROJECT - the GCP Project name
LOCAL_DEV - boolean
"""
import os

cf_config = {}


def innovation_configs():
    """
    Example of overriding and setting explicit configuration values
    such as when in development mode and accessing the innovation
    GCP environment and a local microservice

    :return: no return value
    """
    global cf_config

    cf_config["GCP_PROJECT"] = "tt-temp-2021030444"
    cf_config["LOCAL_DEV"] = True


def init_configs():
    """
    The default implementation of configuration process to initialize
    the configurations
    """
    global cf_config
    cf_config = {
        "GCP_PROJECT": os.getenv("PROJECT"),
        "LOCAL_DEV": True,
        # Number of seconds an event must be handled within before discarding
        # Since this cloud function uses retries, we want to impose a limit
        # to fall back to rather than retries for 7 days
        # 6 hours is initial setting - 60s * 60m * 6h = seconds
        "MAX_SECONDS_EVENT_AGE": 60 * 60 * 6
    }

    in_cloud_env = os.getenv("CF_ENV", "False").lower() == "true"
    cf_config["LOCAL_DEV"] = not in_cloud_env


def get_config_value(key):
    """
    Retrieve configuration value with specified key.
    Use a method instead of referring to variable directly in other
    modules since changes to entries are not reflected after initial inclusion
    in other files.  eg. init, change, lookup uses value from init not change

    :param key: to obtain value of
    :return: type varies by key
    """
    return cf_config.get(key)


init_configs()
