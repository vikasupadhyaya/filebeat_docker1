# Test Message Structure
To support direct testing, an unencoded message structure can be found in example-message.json

The size of array of RECORDS will be processed in smaller batches than submitted.

The REVISION property is used to construct the target firestore collection that data will be added to - kickfire-<revision>

