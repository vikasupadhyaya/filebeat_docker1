import logging
import re
from google.cloud import firestore
from config import get_config_value

# Initialize logging
logger = logging.getLogger(__name__)

# Firestore limit is 500 in batch writes, and we require one spot for updating
# shard details to track counts of records inserted.
max_allowed_records = 499

# Make a global so we do not need to re-create on each call.
# Found documentation that suggests this and confirms that re-auth occurs
# when needed by underlying implementation.
# https://cloud.google.com/apis/docs/client-libraries-best-practices
firestore_client = None


def get_collection_name_from_revision(data_revision):
    """
    Obtain the firestore collection name that would be used for the specified data_revision.

    :param data_revision:
    :return: (str): firestore collection name
    """
    return "kickfire-" + str(data_revision)


def ensure_initialized():
    """
    If global firestore client instance is not defined, then perform Client
    creation and set to global.

    :return: None
    """
    global firestore_client

    if firestore_client is None:
        project = get_config_value("GCP_PROJECT")
        logger.info("Creating firestore client for project: {}".format(project))
        firestore_client = firestore.Client(project)


def get_document_key(record):
    """
    Obtains the consistent document id of record so that we replace any recurrences of same record
    in insert process.

    :param (dict) record: to be inserted and used in determining consistent key for record
    :return: (str) document key
    """
    document_id = str(record.get("Website")) + "_" + str(record.get("start_ip_int")) + "_" + str(
        record.get("end_ip_int"))
    doc_key = re.sub("/", "", document_id)
    return doc_key


def records_exist(data_revision, records):
    """
    Do a check before inserting to determine if the first record of batch
    already exists.  Since delivery of pub/sub messages are 'at least once', we
    have seen a large number of occurrences where messages are being submitted twice for
    the kickfire load.  Many factors may contribute to the variance of repeats of messages, so
    adding a check to prevent another 500 inserts since firestore writes are charged per
    write.  Using the same document key structure allows for a quick check via one read to
    potentially save cost of 500 writes per occurrence.

    :param (str) data_revision: to be inserted
    :param (list) records: to be inserted
    :return: (boolean): True if first record from records already exists in firestore collection
    """
    record_already_present = False

    collection = get_collection_name_from_revision(data_revision)
    doc_key = get_document_key(records[0])
    record_doc = firestore_client.collection(collection).document(doc_key).get()
    if record_doc.exists:
        record_already_present = True

    return record_already_present


def insert_records_to_collection(data_revision, shard, shard_batch_id, records):
    """
    Batch insert the records to the specified firestore collection.
    The use of a shard is based on Distributed counters strategy
    as documented here:
    https://firebase.google.com/docs/firestore/solutions/counters#python_2

    The number of records can not be more than 499 since there is a 500 document
    limit in batch writes to firestore and we require one of those documents to be
    the information in shard record.

    By batching inserts along with a shard record update, we can verify that the expected
    count of inserted records matches what is expected by revision file record count prior
    to promoting a new revision from being used by IBC processes.

    :param (str) data_revision: to be inserted
    :param (str) shard: document id of shard instance to track counter and batch id handling
    :param (int) shard_batch_id:  the first records starting_ip_int value which will allow for
                 focused loading of records in case we have a failure
    :param (list) records: to be inserted
    :return: None
    """
    ensure_initialized()

    if not records:
        logger.info("No records provided to insert.")
        return
    elif len(records) > max_allowed_records:
        logger.error("ERROR - Maximum number of records exceeded for shard {}; shard_batch_id: {}; records: {}".format(
            shard, shard_batch_id, records))
        return
    elif records_exist(data_revision, records):
        logger.info("Kickfire insert skipped - records exist for revision '{}' and batch id: {}".format(
            data_revision, shard_batch_id))
        return

    collection = get_collection_name_from_revision(data_revision)
    shard_doc_key = "ibc-cf-configurations/kickfire-config/data-loads/" + str(data_revision) + "/shards/" + str(shard)

    batch = firestore_client.batch()

    cnt = 0
    for current_record in records:
        cnt += 1
        # build a key so same record is not inserted multiple times
        # if we were to re-run loads
        doc_key = get_document_key(current_record)
        doc_ref = firestore_client.collection(collection).document(doc_key)
        batch.set(doc_ref, current_record)

    shard_ref = firestore_client.document(shard_doc_key)

    batch.update(shard_ref, {"handled_batch_ids": firestore.ArrayUnion([shard_batch_id]),
                             "records_written": firestore.Increment(cnt),
                             "last_updated": firestore.SERVER_TIMESTAMP})
    batch.commit()
