"""
This module provides the definition and functionality for the page_type classification based on regex matching of the
bae URL
"""

import re
from urllib.parse import urlparse
import types


CONT_DISALLOW = ['homepage', 'blog', 'video', 'news', 'webinars', 'whitepaper',
                 'training', 'technical-resource', 'resources']
TAG_RULES_DICT = {

    # Content oriented pages.
    'homepage': {"regex": lambda u: re.findall(f"{urlparse(u).netloc}$|{urlparse(u).netloc}/$", u)},
    'blog': {"regex": 'blog'},
    'video': {"regex": '(?<=blog).*video|video.*(?=blog)|(?<=content).*video|video.*(?=content)|(?<=resource).*video|'
                       'video.*(?=resource)|(?<=demo).*video|video.*(?=demo)|(?<=how).*video|video.*(?=how)|'
                       '(?<=train).*video|video.*(?=train)|(?<=learn).*video|video.*(?=learn)|(?<=training).*video|'
                       'video.*(?=training)|(?<=webinar).*video|video.*(?=webinar)'},
    'news': {"regex": 'article|press|news'},
    'webinars': {"regex": 'webinar'},
    'whitepaper': {"regex": 'white-paper|whitepaper'},
    'training': {"regex": 'training|tutorial'},
    'technical-resource': {"regex": '/(developers|developer)[/?]?$|/(developers|develope)/|[^pP]reference|'
                                    '(?<=technical).*guide|(?<=installation).*guide|(?<=developer).*guide|'
                                    '(?<=api).*guide|(?<=reference).*guide|(?<=user).*guide|(?<=upgrad).*guide|docs'},
    'resources': {"disallow": CONT_DISALLOW, "regex": 'resource|knowledge-center\\?|knowledgecenter\\?|'
                                                      'knowledge-base\\?|knowledge-centre\\?|knowledge-center/$|'
                                                      'knowledgecenter/$|knowledge-base/$|knowledge-centre/$|'
                                                      '(?<![Ff]ac)ebook|guide'},

    # Core types - negative.
    'career': {"disallow": CONT_DISALLOW, "regex": 'career|job'},

    # Core types - prod related.
    'solution': {"disallow": CONT_DISALLOW, "regex": 'solution'},
    'product': {"disallow": CONT_DISALLOW, "regex": 'product(?!production)|/(platform|platforms|technology|'
                                                    'technologies|software)/|/(platform|platforms|technology|'
                                                    'technologies|software)[/?]?$'},
    'services': {"disallow": CONT_DISALLOW, "regex": '/(service|services)[?]?$|/(service|services)/|'
                                                     'professional-services|services-support|services-and-support'},
    'demo': {"disallow": CONT_DISALLOW, "regex":  'demo(?!cra)|freetrial|free-trial|free_trial'},
    'pricing': {"disallow": CONT_DISALLOW, "regex": 'price|pricing'},
    'product-comparison': {"regex": 'compare|comparing|comparison'},
    'case-studies': {"regex": 'case-studies|case-study|case_studies|case_study|casestudies|'
                              'casestudy|use_cases|usecase|use-case'},

    #?????????
    'download': {"disallow": CONT_DISALLOW, "regex": 'download|downloading|downloads'},

    # Core types - comp related.
    'legal': {"disallow": CONT_DISALLOW, "regex": '/(terms|legal)[/?]?$|/(terms|legal)/|terms-of-service|terms-of-use|'
                                                  'termsofuse|terms-conditions|terms-hub|eula|licensing|license|'
                                                  'licensing'},
    'privacy': {"disallow": CONT_DISALLOW, "regex": 'privacy'},
    'thankyou': {"disallow": CONT_DISALLOW, "regex": 'thank'},
    'events': {"disallow": CONT_DISALLOW, "regex": '(?<!pr)event(?!ing)|conference'},
    'awards': {"disallow": CONT_DISALLOW, "regex": 'award'},
    'contact': {"disallow": CONT_DISALLOW, "regex": '(contact|contact-us|contact_sales|contact-sales)([/]?$|\\?.*)'},
    'support': {"disallow": CONT_DISALLOW, "regex": '/(support|customer-support|services-and-support|'
                                                    'training-support|support-and-download|support-2|support-portal)/|'
                                                    '/(support|customer-support|services-and-support|training-support|'
                                                    'support-and-download|support-2|support-portal)[.htm|.html]?&'},
    'partner': {"disallow": CONT_DISALLOW, "regex": 'partner'},
    'about': {
        "disallow": CONT_DISALLOW +
                    ['partner', 'career', 'contact', 'awards', 'events', 'privacy', 'services', 'support'],
        "regex": '((about|company|leadership|/management|/customers/clients)[/]?$)|(/leadership/|/management/|'
                 '/clients/)|((about|company|about-us|overview).*(?=leader.*|company.*|executive.*|values.*|mission.*|'
                 'people.*))|(who-we-are|office-locations|contact-us|our-story|join-our-team|what-we-do|'
                 'board-directors|board-of-directors|locations|leadership-team|leadership_team|management-team|'
                 'our-customers|our-customers|customer-success-stories|our-clients|strategic-alliance)'}
}


def classify_url(input_event):
    """
    Add URL classifications - set PAGE_TYPE field of the event
    :param input_event: (dict) Input processing object
    :return: (dict) Updated event with PAGE_TYPE field set
    """
    page_type_field = "PAGE_TYPE"
    output = []
    # Set mather to function.
    for key in TAG_RULES_DICT:
        if isinstance(TAG_RULES_DICT[key]['regex'], types.FunctionType):
            matcher = TAG_RULES_DICT[key]['regex']
        # Set matcher to regex.
        else:
            pattern = re.compile(TAG_RULES_DICT[key]['regex'])
            matcher = (lambda text, grep=pattern: grep.findall(text, re.IGNORECASE))

        if not set(output) & set(TAG_RULES_DICT[key].get('disallow', [])):
            # original Jason's code - for potentially many fields to match for page type ??
            #match = ' '.join([process_data.get(field) for field in
            #                  set(list(process_data.keys())) & set(config.get('fields', []))])
            # simplifying it significantly - since we only ever use REF_PARAM for that
            match = ' '.join([input_event.get('REF_PARAM')])
            if matcher(match):
                output.append(key)

    if not output:
        input_event[page_type_field] = 'other'
    else:
        input_event[page_type_field] = ", ".join(output)

    return input_event
