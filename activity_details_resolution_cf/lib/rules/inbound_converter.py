import json
import logging
import firestore_config
import helpers.collection_helper as collection_helper
from urllib.parse import unquote
from datetime import datetime

from netaddr import IPNetwork, AddrFormatError
from google.cloud import firestore

from config import get_config
from lib.rules.defaults import get_default_dict
from lib.utilities.error_handler import append_error

logger = logging.getLogger(__name__)

# load in Cf configuration from config.py
cf_config = get_config()


def get_missing_required_fields(input_event):
    required_fields = ["LOGSTASH_ID", "CID", "REF_PARAM", "ACTIVITY_DATE"]
    missing_fields = []
    for field in required_fields:
        if not input_event.get(field):
            missing_fields.append(field)

    if missing_fields:
        append_error(input_event, "MISSING_REQUIRED_FIELDS",
                     description="this event is missing required fields: {}".format(missing_fields))
    return missing_fields


def match_blacklisted_urls(input_event, exclusion_rules):
    """
    Function to check if a given URL contains any of the configured blacklisted character patterns.
    Each line in the configuration can contain one or more elements to match;
    ALL elements on the same line have to be matched for a URL to be considered as "matching"
    lines themselves are matched via OR - meaning that if ANY of the lines is matched for the specified URL - this URL is
    considered to be "matched"

    Configuration format:
        line1: element1_1 | element1_2 | ...
        line2: element2_1 | ...
        ...

    :param (dict) input_event: with REF_PARAM field (url) to match
    :param (set) exclusion_rules: list or set but set is more performant of rules to check domains
    :return: (boolean) - matched_blacklisted_urls = False (if the input_url matched some of the lines)
        True - if it did not match anything
    """
    if not exclusion_rules:
        return False

    url_value = unquote(input_event.get("REF_PARAM"))

    for url_match_line in exclusion_rules:
        # skip empty lines
        if not url_match_line:
            continue
        # split the line into individual match elements
        match_elements = url_match_line.split('|')
        # see this for explanation of for-comprehension and all() function in Python:
        # https://www.geeksforgeeks.org/python-test-if-string-contains-element-from-list/
        # 'if element' is added to exclude empty patterns and lines, if any
        # input_url has to contain ALL match elements from the same line - to be considered as "matched"
        # matched = all((element in url_value) for element in match_elements if element)
        all_elements_matched = True
        for element in match_elements:
            if element.strip() not in url_value:
                all_elements_matched = False
        # as soon as url was "matched" for some match line - return, no need to keep matching anymore lines
        if all_elements_matched:
            logger.info("Event URL ({}) matched blacklisted url elements: {}".format(url_value, url_match_line))
            return all_elements_matched

    # if we got here - no lines/elements matched the event's URL - return False
    return False


def filter_visiting_domain(input_event, exclusion_rules):
    """
    Determine if input_event is to be filtered from further processing due to match on
    configured exclusion_rules.

    :param (dict) input_event: with DOMAIN field used to determine matches
    :param (set) exclusion_rules: list or set of DOMAIN values which should be filtered
    :return: (boolean) - True if DOMAIN matched an entry in exclusion rules; False otherwise
    """
    if not exclusion_rules:
        return False

    matched_visiting_domain = False
    domain_value = input_event.get("DOMAIN")

    if domain_value in exclusion_rules:
        # it matched
        logger.debug('this event domain (domain_value={}) matched some visiting domain; event={}'.format(
            domain_value, json.dumps(input_event)))
        matched_visiting_domain = True

    return matched_visiting_domain


def filter_event_by_cid_in_set(input_event, exclusion_set, filter_reason):
    """
    Determine if the CID of associated input_event is in the exclusion_set.
    For different scenarios, such as expired clients, business defined clients that should not be
    processed, etc; perform common check of the CID in a list dictated by purpose.  Since we have no
    idea what the exclusion_set represents, the filter_reason provides more context to this method
    in logging for results of filter request.

    Note: The 'exclusion_set' is named as such as reminder that for larger collections, using a set is more
        performant than a list.  Often we verbally refer to these as exclusion lists, but internally we want
        to ensure we are wrapping and providing values as a set due to significant processing difference between
        a set and a list structure.

    :param (dict) input_event: containing the properties of event being processed
    :param (set) exclusion_set: list or set, but set is more performant; must contain client ids as int or str values
        that are for excluding based on some purpose - such as expired, blacklisted, etc
    :param (str) filter_reason: used in logging to provide details on results of filter check
    :return: (boolean) - True if input_event CID is in exclusion_set; False otherwise
    """
    matched_value_from_list = False
    field_to_check = "CID"
    cid_value = input_event.get(field_to_check)

    # only do check if defined and has values
    if exclusion_set:
        # Use helper for str or int CID in exclusion_set
        if collection_helper.collection_contains(cid_value, exclusion_set):
            matched_value_from_list = True
    else:
        logger.info("List of CIDs to check is empty - not filtering event. ({})".format(filter_reason))

    logger.debug("Result of filtering CID from list ({}): {}; event: {}".format(filter_reason,
                                                                                matched_value_from_list, input_event))

    return matched_value_from_list


def decode_url(input_event):
    """
    Function to decode the fully qualified url path
    :param input_event: (dict) Input processing object
    :return: (dict) Updated event
    """
    orig_url = input_event.get("REF_PARAM")
    input_event["REF_PARAM"] = unquote(orig_url)
    return input_event


def lookup_gdpr_region(input_event):
    """
    Function to update the region in which the incoming IP came from

    :param input_event: (dict) Input processing object
    :return: boolean: call_successful - TRUE is we were able to resolve IP and Region, FALSE if not
    """
    call_successful = True
    gdpr_event_field = "REGION_CD"
    firestore_collection = firestore_config.get_maxmind_collection()
    # set default gdpr region value
    input_event[gdpr_event_field] = get_default_dict()['STRING']
    event_ip = input_event.get('ACTIVITY_IP')
    try:
        network = IPNetwork(event_ip)
    except AddrFormatError as e:
        logger.exception("AddrFormatError while looking up GDPR region for IP={}; error: ".format(event_ip, e))
        append_error(input_event, str(e),
                     description="Error resolving GDPR region - Invalid ACTIVITY_IP value {}".format(event_ip))
        return False

    int_ip = int(network[0])
    # add this value to the event for further use in the resolve_domain_and_isp() method -
    # to avoid doing this IP lookup twice
    input_event["INT_IP"] = int_ip

    fs_client = firestore.Client()
    site = fs_client.collection(firestore_collection)

    # we can do a compound query on different fields in FS using inequalities so we are using a little logic to return
    # the closest value to the lookup. Can think about extending this to > 1 return if desired
    query = site.where('start_ip_int', "<=", int_ip) \
        .order_by('start_ip_int', direction=firestore.Query.DESCENDING) \
        .limit(1)
    res = query.stream()

    # stream returns a generator so we have to loop although there will only be 1 result
    for doc in res:
        res_dict = doc.to_dict()
        if res_dict.get('start_ip_int') <= int_ip <= res_dict.get('end_ip_int'):
            input_event[gdpr_event_field] = res_dict.get('country_iso_code')

    # TODO shouldn't this be unsuccessful if the ip range is NOT in a matching record?

    logger.debug("lookup_gdpr_region() results: event ip={}, int_ip={}, gdpr_region(i.e. country_iso_code)={}"
                 .format(event_ip, int_ip, input_event.get(gdpr_event_field)))
    return call_successful


def set_eu_allowed(input_event, exclusion_rules):
    """
    Function to determine if value is able to be looked up based on GDPR exclusions

    :param input_event: (dict) Input event
    :param (set) exclusion_rules: set of client ids which allow processing even though in an excluded region
    :return: (dict) Updated event
    """
    eu_allowed_field = "EU_ALLOWED"
    exclude_regions = ["SI", "TR", "BY", "LT", "CH", "ES", "GE", "CZ", "AD", "AL", "IS", "IT", "FR", "BE", "LU", "AM",
                       "EE", "VA", "GR", "MD", "FI", "HR", "CY", "DK", "SE", "PT", "IE", "HU", "LV", "MC", "ME", "GB",
                       "DE", "AT", "BA", "RO", "BG", "LI", "MT", "MK", "PL", "UA", "XK", "KZ", "SK", "NL", "AZ"]
    if input_event.get('REGION_CD') in exclude_regions:
        input_event[eu_allowed_field] = False
    else:
        input_event[eu_allowed_field] = True

    # check if there are CID-specific overrides for the GDPR exclusion
    cid_value = input_event.get("CID")

    if exclusion_rules:
        # Use helper for str or int CID in exclusion_set
        if collection_helper.collection_contains(cid_value, exclusion_rules):
            input_event[eu_allowed_field] = True
    else:
        logger.debug("No GDPR override values defined - leaving eu_allowed_field set from excluded region check.")

    return input_event


def add_partition_columns(input_event):
    """
    Function to update the columns to fit the partitioning structure as well as adjust for UTC
    :param input_event: (dict) Input processing object
    :return: (dict) Updated data
    """
    # original input event from Kafka has ACTIVITY_DATE field set as timestamp in milliseconds
    activity_in_milliseconds = input_event.get('ACTIVITY_DATE')
    input_event['ACTIVITY_MILLIS'] = activity_in_milliseconds
    # pylint: disable=unused-argument
    # convert milliseconds into a date in format '%Y-%m-%d'
    # activity_date = activity_in_milliseconds.astimezone(timezone.utc).strftime('%Y-%m-%d')
    activity_date_full = datetime.utcfromtimestamp(int(activity_in_milliseconds) / 1000.0)
    input_event['ACTIVITY_DATE'] = activity_date_full.strftime('%Y-%m-%d')

    return input_event


def add_audit_columns(input_event):
    """
    Function to add audit columns to result set
    :param input_event: (dict) Input processing object
    :return: (dict) Updated data
    """
    # pylint: disable=unused-argument
    now = datetime.utcnow().strftime('%Y-%m-%d %X')
    input_event['DATE_CREATED'] = now
    input_event['DATE_MODIFIED'] = now
    return input_event


def resolve_domain_and_isp(input_event):
    """
    Function to lookup source domain and IS_ISP based on IP
    :param input_event: (dict) Input event
    :return: (dict) Updated event
    """
    domain_field = "DOMAIN"
    # TODO check if we need to ensure collections used are consistent until refresh time
    #  for all instances. Depending on time instance is created and when a new collection
    #  becomes active, we could use different collections until the next day.
    firestore_collection = firestore_config.get_kickfire_collection()
    event_ip = input_event.get('ACTIVITY_IP')
    input_event[domain_field] = get_default_dict()['STRING']
    # set default value for IS_ISP to False
    input_event['IS_ISP'] = False
    int_ip = input_event.get("INT_IP")
    if not int_ip:
        logger.debug("INT_IP field is missing in the input event: {}".format(json.dumps(input_event)))
        # only lookup IP if we do not have an already resolved int_ip value (should never happen)
        try:
            network = IPNetwork(event_ip)
        except AddrFormatError as e:
            logger.exception(
                "Failed to resolve DOMAIN for event with ACTIVITY_IP={}; error: {}".format(event_ip, str(e)))
            append_error(input_event, str(e),
                         description="Error resolving DOMAIN - Invalid ACTIVITY_IP value {}".format(event_ip))
            return input_event
        int_ip = int(network[0])

    # TODO future enhancement - move Client initialization to global scope
    #  will require testing over period of time but have recently found
    #  that the google api 'heavy' objects can be created in global scope
    #  https://firebase.google.com/docs/functions/networking
    #  this would apply to pub/sub as well
    fs_client = firestore.Client()
    site = fs_client.collection(firestore_collection)

    # we can do a compound query on different fields in FS using inequalities so we are using a little logic to return
    # the closest value to the lookup. Can think about extending this to > 1 return if desired
    query = site.where('start_ip_int', "<=", int_ip) \
        .order_by('start_ip_int', direction=firestore.Query.DESCENDING) \
        .limit(1)
    res = query.stream()

    # stream returns a generator so we have to loop although there will only be 1 result
    for doc in res:
        res_dict = doc.to_dict()
        if res_dict.get('start_ip_int') <= int_ip <= res_dict.get('end_ip_int'):
            input_event[domain_field] = res_dict.get('Website')
            input_event['IS_ISP'] = res_dict.get('is_isp', False)
        else:
            input_event[domain_field] = get_default_dict()['STRING']

    logger.debug(("resolve_domain_and_isp(): resolved DOMAIN={} and IS_ISP={} " +
                  "for ACTIVITY_IP={} --> int_ip = {}; event: {}").format(input_event.get(domain_field),
                                                                          input_event.get('IS_ISP'),
                                                                          event_ip,
                                                                          int_ip,
                                                                          json.dumps(input_event)))
    return input_event
