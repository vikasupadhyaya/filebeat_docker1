"""
main.py contains the main processing logic for processing
"""
import json
import copy
import os
import base64
import logging
import time
import cached_configs
import requests
from google.cloud import pubsub_v1
from config import get_config
from lib.rules.defaults import get_default_dict
from lib.rules.inbound_converter import add_audit_columns, add_partition_columns, \
    lookup_gdpr_region, set_eu_allowed, resolve_domain_and_isp, decode_url, \
    get_missing_required_fields, filter_visiting_domain, match_blacklisted_urls, \
    filter_event_by_cid_in_set
from lib.rules.page_type import classify_url
from lib.utilities.error_handler import append_error
from tt_common.tt_logging import Logging

project = os.getenv("PROJECT")
EDC_REST_URL = os.getenv("EDC_REST_URL", "default_rest_url")
GLOBAL_WEBSCRAPING_ENABLED_STR = os.getenv("GLOBAL_WEBSCRAPING_ENABLED", True)
Logging().init_logging()
logger = logging.getLogger(__name__)

futures = dict()


def process_event(event, context):
    """
    Main procesing logic to receive data from pub/sub, apply rules to data, and load to BQ
    :param event: (dict) The dictionary with data specific to this type of
         event. The `data` field contains the PubsubMessage message. The
         `attributes` field will contain custom attributes if there are any.
    :param context: (google.cloud.functions.Context) The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time
    :return: (dict) post-processed data dictionary
    """
    if not GLOBAL_WEBSCRAPING_ENABLED_STR or GLOBAL_WEBSCRAPING_ENABLED_STR.lower() == "true":
        GLOBAL_WEBSCRAPING_ENABLED = True
    else:
        GLOBAL_WEBSCRAPING_ENABLED = False
    if not event:
        logger.warning("Recieved an empty event from PubSub - ignoring it")
        return
    else:
        logger.info("processing raw event: {}".format(event))
    config = get_config()
    failed_events_topic = config['failed_events_topic']
    completed_events_topic = config['completed_events_topic']
    in_progress_event = {}
    try:
        if "attributes" in event and event.get('attributes'):
            input_event = json.loads(json.dumps(event['attributes']))
        elif "data" in event and event.get('data'):
            decoded_event = base64.b64decode(event['data']).decode('utf-8')
            logger.info("processing decoded_event: {}".format(decoded_event))
            input_event = json.loads(decoded_event)
        else:
            raise Exception('Recieved an event in unrecognized format - '
                            'data or attributes top element is expected'.format(event))

        if not input_event:
            logger.error("ERROR: event payload is empty or not readable - dropping this event: {}".format(event))
            return input_event

        # copy all fields from the input events
        in_progress_event = copy.deepcopy(input_event)
        # extract event ID from the PubSub message
        if context:
            in_progress_event['event_id'] = context.event_id

        in_progress_event = add_audit_columns(in_progress_event)
        in_progress_event = add_partition_columns(in_progress_event)
        logger.info("Processing event: {}".format(json.dumps(in_progress_event)))
        # check if all required fields are present in the event - if not, send to failed events BQ table
        missing_fields = get_missing_required_fields(in_progress_event)
        if missing_fields:
            # the list of missing fields is not empty - send to failed events
            in_progress_event['STATUS'] = "MISSING_REQUIRED_FIELDS"
            publish_event_to_topic(in_progress_event, failed_events_topic)
            return in_progress_event

        in_progress_event = decode_url(in_progress_event)
        in_progress_event = classify_url(in_progress_event)

        # initialize cache, failing if cache not initialized or error during refresh
        config_init_error = cached_configs.initialize_or_refresh()
        if config_init_error:
            # there were errors while updating/looking up configuration options from a DB
            in_progress_event['STATUS'] = 'ERROR_EXIT'
            append_error(in_progress_event, "Failed during cache initialization or refresh: ", config_init_error)
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "ERROR_EXIT"
            return in_progress_event

        # check if this event's CID is blacklisted ==> not allowed to be processed
        # control over how excluded cids are defined is part of cache layer and
        # delegated retrieval method
        matched_expired_cids = filter_event_by_cid_in_set(in_progress_event, cached_configs.get_expired_cids(),
                                                          "Expired Client Ids")
        if matched_expired_cids:
            # this event's CID matched some blacklisted CID - set corresponding status and send to BQ
            logger.debug("This event's CID matched some expired CIDs: {} ".format(json.dumps(input_event)))
            in_progress_event['STATUS'] = "MATCHED_DO_NOT_PROCESS_CID"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "MATCHED_DO_NOT_PROCESS_CID"
            return in_progress_event

        # check if REF_PARAM url matches any blacklisted URLs
        matched_blacklist_urls = match_blacklisted_urls(in_progress_event, cached_configs.get_blacklist_urls())
        if matched_blacklist_urls:
            # this event's REF_PARAM matched a blacklisted URL - set corresponding status and send to BQ
            logger.debug("this event's REF_PARAM matched some blacklisted URL elements: {} ".format(json.dumps(input_event)))
            in_progress_event['STATUS'] = "MATCHED_BLKLIST_URL"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "MATCHED_BLKLIST_URL"
            return in_progress_event

        # resolve IP and lookup Region - if resolution was successful, these fields will be
        # set in the in_progress_event; if not - call_succesful = False will be returned ,
        # indicating there was an error resolving the IP - exit with an error status
        call_successful = lookup_gdpr_region(in_progress_event)
        if not call_successful:
            in_progress_event['STATUS'] = 'ERROR_EXIT'
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "ERROR_EXIT"
            return in_progress_event

        # set EU_ALLOWED = False if this IP/Region is in the GDPR exclusion list
        # set EU_ALLOWED = True otherwise - OR if the exclusion is overridden for this CID
        in_progress_event = set_eu_allowed(in_progress_event, cached_configs.get_gdpr_exclude_cids())

        # if there are any errors looking up config lists in DBs - EXIt with status ERROR_EXIT
        # we cannot store/process any GDPR-excluded events -
        # set the status to MATCHED_GDPR_NOT_ALLOWED and exit
        if not in_progress_event.get('EU_ALLOWED'):
            # this event matched regions excluded due to GDPR rules
            logger.debug("This event matched regions excluded due to GDPR rules: {} ".format(json.dumps(input_event)))
            in_progress_event['STATUS'] = "MATCHED_GDPR_NOT_ALLOWED"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "MATCHED_GDPR_NOT_ALLOWED"
            return in_progress_event

        in_progress_event = resolve_domain_and_isp(in_progress_event)

        if in_progress_event.get('DOMAIN') == get_default_dict()['STRING']:
            # we have an event with empty DOMAIN - set corresponding status and send to BQ
            # this can happen if this IP range / IPs exist and resolve to a correct region in the maxmind dataset
            # but there is no entry for this IP range in the kickfire dataset OR
            # if there are no entries in either datasets
            logger.debug("This event's IP resolved to an empty DOMAIN: {}".format(json.dumps(in_progress_event)))
            in_progress_event['STATUS'] = "EMPTY_DOMAIN"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "EMPTY_DOMAIN"
            return in_progress_event

        if in_progress_event.get('IS_ISP'):
            # we have an event with IS_ISP = True: - set corresponding status and send to BQ
            logger.debug("this event resolved to IS_ISP = True: {}".format(json.dumps(in_progress_event)))
            in_progress_event['STATUS'] = "MATCHED_IS_ISP"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "MATCHED_IS_ISP"
            return in_progress_event

        # check for visiting domain - only proceed to web_scraping if it does not match
        matched_visiting_domain = filter_visiting_domain(in_progress_event,
                                                         cached_configs.get_exclude_visiting_domains())
        if matched_visiting_domain:
            # this event's DOMAIN matched a visiting domain - set corresponding status and send to BQ
            logger.info("this event's DOMAIN matched a visiting domain: {} ".format(json.dumps(input_event)))
            in_progress_event['STATUS'] = "MATCHED_VISITING_DOMAIN"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "MATCHED_VISITING_DOMAIN"
            return in_progress_event

        # TODO validate if this is still needed - we should not get here if there were any errors:
        # if there were any errors - do not webscrape this event -
        # but send it to the completed_events topic to be stored as is in BQ
        #if in_progress_event.get('ERRORS', []):
        #    logger.error("There were errors processing event:{} - sending it to the {} topic: ".format(
        #        json.dumps(in_progress_event), completed_events_topic
        #    ))
        #    publish_event_to_topic(in_progress_event, completed_events_topic)

        # check if we have scraped info for this URL and CID in the MySQL already;
        # if yes - add the "PAGE_TEXT_COUNT" and "PAGE_TITLE" fields to the event
        # and send it to the "completed_activities" topic
        edc_rest_endpoint = EDC_REST_URL + config["edc_service_lookup_method_path"]
        call_successful = lookup_scraping_info_from_db(in_progress_event, edc_rest_endpoint)
        if not call_successful:
            # there were errors from the lookup call to REST API - exit
            in_progress_event['STATUS'] = 'ERROR_EXIT'
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "ERROR_EXIT"
            return in_progress_event

        page_text_count = in_progress_event.get("PAGE_TEXT_COUNT", 0)
        page_title = in_progress_event.get("PAGE_TITLE", "")

        # if we have page title and text_count not empty - EXIT and send event to BQ
        if page_text_count > 0 and len(page_title) > 0:
            logger.debug(("Successfully looked up page info for URL={} and CID={}: " +
                          "page_text_count={}, page_title={}; sending event to {} topic").format(
                in_progress_event.get("REF_PARAM"), in_progress_event.get("CID"), page_text_count, page_title,
                completed_events_topic))
            in_progress_event['STATUS'] = 'WEBSCRAPING_CACHE_HIT'
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with STATUS = "WEBSCRAPING_CACHE_HIT"
            return in_progress_event

        # we did not get page info from the REST API call - proceed with webscraping if allowed
        # if GLOBAL_WEBSCRAPING_ENABLED == False --> skip webscraping for all events
        if not GLOBAL_WEBSCRAPING_ENABLED:
            logger.debug("GLOBAL_WEBSCRAPING_ENABLED is False - skipping webscraping for event: {}".format(
                json.dumps(in_progress_event)))
            in_progress_event['STATUS'] = 'GLOBAL_WEBSCRAPING_OFF'
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "GLOBAL_WEBSCRAPING_OFF"
            return in_progress_event

        # GLOBAL_WEBSCRAPING_ENABLED is True;
        # now check if this event's CID is on the "do_not_webscrape" list
        matched_do_not_webscrape_cids = filter_event_by_cid_in_set(in_progress_event,
                                                                   cached_configs.get_do_not_webscrape_cids(),
                                                                   "Do Not Webscrape")
        if matched_do_not_webscrape_cids:
            # this event's CID matched some do_not_webscrape CID - set corresponding status and send to BQ
            logger.info("This event's CID matched some do_not_webscrape CIDs: {} ".format(json.dumps(input_event)))
            in_progress_event['STATUS'] = "MATCHED_DO_NOT_WEBSCRAPE_CID"
            publish_event_to_topic(in_progress_event, completed_events_topic)
            # EXIT with status "MATCHED_DO_NOT_WEBSCRAPE_CID"
            return in_progress_event

        # Finally, we are at the point when we need to send this event for webscraping
        events_to_webscrape_topic = config['events_to_webscrape_topic']
        logger.debug(("No cached page info found for URL={} and CID={}: " +
                      "sending event for webscraping to {} topic").format(
            in_progress_event.get("REF_PARAM"), in_progress_event.get("CID"), events_to_webscrape_topic))
        publish_event_to_topic(in_progress_event, events_to_webscrape_topic)
        # EXIT with no status set - it will be set in the webscraper CF
        return in_progress_event

    # if anything else fails - send event to the failed_events topic
    # - as we may not have an event  with required fields (like LOGSTASH_ID) to store into the normal BQ table
    except Exception as e:
        err_str = "activity_details_resolution CF: Exception in process_event(): event={}; ERROR: {}".format(
            json.dumps(in_progress_event), str(e))
        logger.exception(err_str)
        append_error(in_progress_event, 'fatal_error', err_str)
        in_progress_event['STATUS'] = "ERROR_EXIT"
        publish_event_to_topic(in_progress_event, failed_events_topic)


def lookup_scraping_info_from_db(event, edc_service_url):
    """
    function to call EDC microservice via a REST API to get cached page info for URL+CID, if available
    example REST API: https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls?clientId.equals=100&clientUrl.equals=www.techtarget.com%2FhomepageURL_1

    """
    call_successful = True
    url_to_scrape = event.get("REF_PARAM")
    cid = event.get("CID")
    query_params = {'clientId.equals': cid, 'clientUrl.equals': url_to_scrape, 'page': 0, 'size': 1,
                    'sort': 'dateCreated,desc'}
    try:
        response = requests.get(edc_service_url, params=query_params)
        json_response = response.json()
        if not json_response:
            logger.info(("Completed call to {} for url {} " +
                         "OK - but no data was retrieved").format(edc_service_url, url_to_scrape))
        else:
            event['PAGE_TEXT_COUNT'] = json_response[0]["pageTextCount"]
            event['PAGE_TITLE'] = json_response[0]["pageTitle"]
            logger.debug("Completed call to {} for url {}: response: {}; event: {}".format(
                edc_service_url, url_to_scrape, json_response, event))
    except Exception as e:
        err_str = "activity_details_resolution CF: Error calling REST API {} for CID={} and url={}: error={}".format(
            edc_service_url, cid, url_to_scrape, str(e))
        logger.error(err_str)
        append_error(event, "Failed call to REST API", err_str)
        call_successful = False

    return call_successful


def get_callback(logstash_id):
    def callback(future):
        try:
            logger.debug("Future completed for {} with result {}.".format(logstash_id, future.result()))
            if futures.get(logstash_id):
                futures.pop(logstash_id)
        except Exception as e:
            err_str = str(e)
            logger.exception("Fatal exception while publishing {} for {}.".format(err_str, logstash_id))
            append_error(None, 'fatal_error', err_str)
    return callback


def publish_event_to_topic(event, topic):
    publisher = pubsub_v1.PublisherClient()
    logstash_id = event.get("LOGSTASH_ID")
    data = json.dumps(event).encode("utf-8")
    futures.update({logstash_id: None})
    # When you publish a message, the client returns a future.
    topic_path = publisher.topic_path(project, topic)
    future = publisher.publish(topic_path, data)
    futures[logstash_id] = future
    # Publish failures shall be handled in the callback function.
    future.add_done_callback(get_callback(logstash_id))

    # Wait for the future to resolve before exiting.
    while len(futures) != 0:
        time.sleep(0.05)
