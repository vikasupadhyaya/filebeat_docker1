"""
Provide cache layer to configurations stored in firestore.

The configured time that we refresh cache should be close to the time that we
perform any checks for new configurations.  Eg. If kickfire active collection is
changed at 2:30am by the kickfire_complete_check_job, new cloud function instances
created between time active collection is switched and cache refresh will be using
a different collection than existing cloud function instances until the time of
cache refresh.
"""
import logging
from config import get_config
from datetime import datetime
from google.cloud import firestore

logger = logging.getLogger(__name__)

global_firestore_config_last_updated = None
global_firestore_config_kickfire_collection = None
global_firestore_config_maxmind_collection = None

# global instance to re-use details and refresh as needed
firestore_client = None


def ensure_initialized():
    """
        If global firestore client instance is not defined, then perform Client
        creation and set to global.

        :return: None
        """
    global firestore_client

    if firestore_client is None:
        logger.info("Creating firestore client with default scope project")
        firestore_client = firestore.Client()


def __private_get_kickfire_config_from_firestore():
    """
    Obtain the collection name from firestore that is to be used to obtain
    kickfire details.

    :return: (str) - kickfire collection name to use
    """
    # TODO remove this default after release and automated load process
    #  has run to create and update configuration document
    default_original_collection = "kickfire"

    kickfire_config_doc_id = get_config().get("kickfire_config_doc_id")
    kickfire_config_doc = firestore_client.document(kickfire_config_doc_id).get()

    if kickfire_config_doc.exists:
        kickfire_config = kickfire_config_doc.to_dict()
        # use bracket access because we want to fail if these properties are not present
        active_collection = kickfire_config["active_collection"]
        active_last_updated = kickfire_config["active_collection_last_updated"]

        # NOTE: It was determined to be acceptable that the instances of this cloud function
        # may be using different collections.  The time of switching to a new collection is done
        # nightly at 2am and instances will refresh cache at 3am.  The purpose of previous_active_collection
        # was to keep all instances in sync but was deemed low need vs additional time for logic and testing
        # needed to implement - see config file for time

        # use get access because previous_active could be empty until next quarter load is completed
        previous_active_collection = kickfire_config.get("previous_active_collection", default_original_collection)
        # combine before logger call since split lines was only formatting against second line string
        info_message = "Kickfire configuration: Returning kickfire active collection {}; active updated: {}; " + \
                        "previous active collection: {}"
        logger.info(info_message.format(active_collection, active_last_updated, previous_active_collection))
        return active_collection

    logger.info("Kickfire configuration: kickfire-config document does not exist. Using original collection: {}".format(
        default_original_collection))
    return default_original_collection


def __private_get_maxmind_config_from_firestore():
    """
    Obtain the collection name from firestore that is to be used to obtain
    maxmind details.

    :return: (str): maxmind collection name to use
    """
    # TODO remove this default after release and automated load process
    #  has run to create and update configuration document
    default_original_collection = "maxmind"

    maxmind_config_doc_id = get_config().get("maxmind_config_doc_id")
    maxmind_config_doc = firestore_client.document(maxmind_config_doc_id).get()

    if maxmind_config_doc.exists:
        maxmind_config = maxmind_config_doc.to_dict()
        # use bracket access because we want to fail if these properties are not present
        active_collection = maxmind_config["active_collection"]
        active_last_updated = maxmind_config["active_collection_last_updated"]

        # NOTE: It was determined to be acceptable that the instances of this cloud function
        # may be using different collections.  The time of switching to a new collection is done
        # nightly at 2am and instances will refresh cache at 3am.  The purpose of previous_active_collection
        # was to keep all instances in sync but was deemed low need vs additional time for logic and testing
        # needed to implement - see config file for time

        # use get access because previous_active could be empty until next load is completed
        previous_active_collection = maxmind_config.get("previous_active_collection", default_original_collection)
        # combine before logger call since split lines was only formatting against second line string
        info_message = "Maxmind configuration: Returning maxmind active collection {}; active updated: {}; " + \
                        "previous active collection: {}"
        logger.info(info_message.format(active_collection, active_last_updated, previous_active_collection))
        return active_collection

    logger.info("Maxmind configuration: maxmind-config document does not exist. Using original collection: {}".format(
        default_original_collection))
    return default_original_collection


def __private_set_cache_values(now_datetime):
    global global_firestore_config_last_updated
    global global_firestore_config_kickfire_collection
    global global_firestore_config_maxmind_collection

    ensure_initialized()
    global_firestore_config_kickfire_collection = __private_get_kickfire_config_from_firestore()
    global_firestore_config_maxmind_collection = __private_get_maxmind_config_from_firestore()

    # set last refresh time only after successfully setting all fresh retrieval methods
    global_firestore_config_last_updated = now_datetime


def __private_initialize_or_refresh_cache_values(now_datetime, cache_refresh_hour_of_day):
    """
    An internally used method to determine if cache is still valid based on other properties
    stored when cache is set.  A separate method to consolidate the cache logic for a unit test
    to confirm cache usage by various input values.  If cache is in need of setting, delegate
    to __provate_set_cache_values() to have all cached values updated to latest results from their
    corresponding sources of truth method.

    :param (datetime) now_datetime: current time
    :param (int) cache_refresh_hour_of_day: hour of day that cache is to be refreshed from source of truth
    :return: (boolean): True if cache is value; False if cache needs to be set from sources of truth
    """
    cache_used = False
    now_datetime_refresh_time = now_datetime.replace(hour=cache_refresh_hour_of_day, minute=0, second=0, microsecond=0)

    if global_firestore_config_last_updated is None:
        __private_set_cache_values(now_datetime)
    elif now_datetime < now_datetime_refresh_time:
        cache_used = True
    elif global_firestore_config_last_updated < now_datetime_refresh_time:
        __private_set_cache_values(now_datetime)
    else:
        cache_used = True
    return cache_used


def __private_ensure_cache_accurate():
    """
    Wrapper to get current time and configuration for hour of day to refresh the cache
    and delegate to __private_initialize_or_refresh_cache_values() which is exposed to
    allow testing cache usage with different time of day and refresh hour.

    :return: (boolean): True if cache used; False if retrieved latest values from source of truth
    """
    now_datetime = datetime.now()
    cache_refresh_hour_of_day = get_config().get("cache_cleanup_hour_of_day")
    return __private_initialize_or_refresh_cache_values(now_datetime, cache_refresh_hour_of_day)


def get_kickfire_collection():
    """
    Hide cache details from locations that need access to the kickfire collection
    that is to be used.

    :return: (str) - firestore collection name containing kickfire data to use
    """
    __private_ensure_cache_accurate()
    return global_firestore_config_kickfire_collection


def get_maxmind_collection():
    """
    Hide cache details from locations that need access to the maxmind collection
    that is to be used.

    :return: (str) - firestore collection name containing maxmind data to use
    """
    __private_ensure_cache_accurate()
    return global_firestore_config_maxmind_collection
