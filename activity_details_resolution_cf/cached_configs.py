"""
Provide a cache layer to the configurations used by cloud function that
are obtained from micro-service or other resource that we do not want
to call on each invocation, but re-use the retrieved values until
a nightly cache refresh occurs.
"""
import logging
import helpers.micro_service_config_retrieval as micro_service_config_retrieval
import helpers.firestore_retrieve_expired_cids as firestore_retrieve_expired_cids
from config import get_config
from datetime import datetime

logger = logging.getLogger(__name__)

global_config_last_updated_datetime = None

# The client ids which are excluded from GDPR rule
global_config_gdpr_exclude_cids = None

# The client ids which are expired and should not be processed
global_config_expired_cids = None

# The urls or pattern rules that will prevent further processing
global_config_blacklist_urls = None

# The client ids which are excluded from web scraping process
global_config_do_not_webscrape_cids = None

# The domain values to match and prevent further processing
global_config_exclude_visiting_domains = None


def __private_verify_initialized_or_raise_error():
    """
    Safeguard on all cache get methods to ensure proper
    initialization.

    :return: None; raises Exception if cache not initialized
    """
    if global_config_last_updated_datetime is None:
        raise Exception("Cache not initialized. Need to call initialize_or_refresh() prior to use.")


def __private_ensure_global_connections_initialized():
    """
    If any global re-usable objects, such as google client library connections
    are used by cache, ensure they are initialized.

    :return: None
    """
    # see firestore_config for example of actual initialization steps
    logger.debug("No global clients to initialize.")


def __private_set_cache_values_only_once():
    """
    The configurations defined from files which are deployed with the cloud function only
    need to be read in once since they would not change until a new version of the cloud function
    is deployed and would result in a clean cache initialization run, reading the new files.

    :return: None; may raise Exception from raw retrieval processes
    """
    if global_config_last_updated_datetime is not None:
        return

    logger.info("Performing load of configurations from deployed files:")
    logger.info("--no file based configurations")


def __private_set_cache_values(now_datetime):
    """
    Retrieve from sources of truth the configuration values and set into our
    local cache values.  When all complete, update the cache last updated datetime
    to value provided.

    :param (datetime) now_datetime: a datetime that is usually 'now' to determine if cache is in need of refresh
    :return: None; may raise Exception from raw retrieval processes
    """
    global global_config_last_updated_datetime
    global global_config_gdpr_exclude_cids
    global global_config_expired_cids
    global global_config_blacklist_urls
    global global_config_do_not_webscrape_cids
    global global_config_exclude_visiting_domains

    __private_ensure_global_connections_initialized()

    # This call for 'only once' will only do retrievals if global_config_last_updated_datetime is not set
    __private_set_cache_values_only_once()

    global_config_gdpr_exclude_cids = micro_service_config_retrieval.get_gdpr_exclude_cids()

    # Expired Client IDs not retrieved from micro-service since we can obtain directly
    # from BigQuery query.  Firestore retrieval was a historical implementation that
    # had retrieved from BigQuery (same as in this CF now), but placed results into Firestore
    # and then we read from Firestore.  Removing that extra layer and processing since we
    # are caching and can obtain directly from source of truth rather than an intermediate
    # location.
    # Update - continue to use Firestore process since concurrent BigQuery queries cause read limit
    # to be reached.
    global_config_expired_cids = firestore_retrieve_expired_cids.get_do_not_process_cids()

    global_config_blacklist_urls = micro_service_config_retrieval.get_blacklist_urls()
    global_config_do_not_webscrape_cids = micro_service_config_retrieval.get_do_not_webscrape_cids()
    global_config_exclude_visiting_domains = micro_service_config_retrieval.get_exclude_visiting_domains()

    # set last refresh time only after successfully setting all fresh retrieval methods
    global_config_last_updated_datetime = now_datetime


def __private_initialize_or_refresh_cache_values(now_datetime, cache_refresh_hour_of_day):
    """
    An internally used method to determine if cache is still valid based on other properties
    stored when cache is set.  A separate method to consolidate the cache logic for a unit test
    to confirm cache usage by various input values.  If cache is in need of setting, delegate
    to __private_set_cache_values() to have all cached values updated to latest results from their
    corresponding sources of truth method.

    :param (datetime) now_datetime: current time
    :param (int) cache_refresh_hour_of_day: hour of day that cache is to be refreshed from source of truth
    :return: (boolean): True if cache is value; False if cache needs to be set from sources of truth
    """
    global global_config_last_updated_datetime
    cache_used = False
    now_datetime_refresh_time = now_datetime.replace(hour=cache_refresh_hour_of_day, minute=0, second=0, microsecond=0)

    if global_config_last_updated_datetime is None:
        __private_set_cache_values(now_datetime)
    elif now_datetime < now_datetime_refresh_time:
        cache_used = True
    elif global_config_last_updated_datetime < now_datetime_refresh_time:
        __private_set_cache_values(now_datetime)
    else:
        cache_used = True

    logger.debug("Initialization or refresh of cache check resulted in cache use of '{}'".format(cache_used))

    return cache_used


def __private_ensure_cache_accurate():
    """
    Wrapper to get current time and configuration for hour of day to refresh the cache
    and delegate to __private_initialize_or_refresh_cache_values() which is exposed to
    allow testing cache usage with different time of day and refresh hour.

    :return: (boolean): True if cache used; False if retrieved latest values from source of truth
    """
    now_datetime = datetime.now()
    cache_refresh_hour_of_day = get_config().get("cache_cleanup_hour_of_day")
    return __private_initialize_or_refresh_cache_values(now_datetime, cache_refresh_hour_of_day)


def initialize_or_refresh():
    """
    The routine to ensure that cache values are up to date.  Should
    be called a single time at the beginning of handling an event rather than
    have multiple cache checks performed on each cached value request.

    :return: (str) - error reason during initialization or empty for successful initialization
    """
    error_str = ""
    try:
        __private_ensure_cache_accurate()
    except Exception as e:
        error_str = "Failed setting cache values: {}".format(str(e))
        logger.exception(error_str)

    return error_str


def get_gdpr_exclude_cids():
    """
    Hide cache details and logic for obtaining values, just returning the
    requested configuration value.

    Load a list of CIDs that override GDPR exclusion - if a CID is on this list it means they allow
    usage of their region/IP info

    Originally defined in gdpr_exclude_override_cids.txt file.

    :return: (set) - of client ids that are excluded from gdpr restrictions
    """
    __private_verify_initialized_or_raise_error()
    return global_config_gdpr_exclude_cids


def get_expired_cids():
    """
    Hide cache details and logic for obtaining values, just returning the
    requested configuration value.

    The client ids which are expired or configured to no longer have processing performed.

    Originally defined in do_not_process_cids.txt file.

    :return: (set) - of client ids that have expired
    """
    __private_verify_initialized_or_raise_error()
    return global_config_expired_cids


def get_blacklist_urls():
    """
    Hide cache details and logic for obtaining values, just returning the
    requested configuration value.

    The urls or pattern rules that will prevent further processing

    Originally defined in blacklist_urls.txt file

    :return: (set) - containing defined rule format for determining matches to urls to filter processing
    """
    __private_verify_initialized_or_raise_error()
    return global_config_blacklist_urls


def get_do_not_webscrape_cids():
    """
    Hide cache details and logic for obtaining values, just returning the
    requested configuration value.

    Originally defined in do_not_webscrape_cids.txt file

    :return: (set) - of client ids that should not have web scraping performed
    """
    __private_verify_initialized_or_raise_error()
    return global_config_do_not_webscrape_cids


def get_exclude_visiting_domains():
    """
    Hide cache details and logic for obtaining values, just returning the
    requested configuration value.

    Originally defined in visiting_domains.txt file

    :return: (set) - of domains that will cause event to be filtered from processing
    """
    __private_verify_initialized_or_raise_error()
    return global_config_exclude_visiting_domains
