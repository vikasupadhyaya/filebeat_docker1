"""
Centralized location for configuration options specific to the create classification
events process.
"""
import os

cloud_function_execution_config = {}


def set_gcp_project_config(gcp_project):
    """
    Example of overriding and setting explicit configuration values
    such as when in development mode

    :return: no return value
    """
    global cloud_function_execution_config
    cloud_function_execution_config["GCP_PROJECT"] = gcp_project


def set_ibc_host_config(ibc_host):
    """
    Example of overriding and setting explicit configuration values
    such as when in development mode

    :return: no return value
    """
    global cloud_function_execution_config
    cloud_function_execution_config["IBC_HOST"] = ibc_host.rstrip("/")


def init_configs():
    """
    The default implementation of configurations for this cloud function
    """
    global cloud_function_execution_config
    cloud_function_execution_config = {
        "GCP_PROJECT": os.getenv("PROJECT"),

        # The host of IBC microservice used for obtaining
        # page records.  Stored value will never contain the
        # trailing slash - '/'
        "IBC_HOST": os.getenv("EDC_REST_URL", "not-provided").rstrip("/"),

        # Configuration endpoints
        "CONFIG_GDPR_EXCLUDE_OVERRIDE_CIDS_API_PATH": "/api/client-exclusions",
        "CONFIG_BLACKLIST_URLS_API_PATH": "/api/config-url-exclusions",
        "CONFIG_DO_NOT_WEBSCRAPE_CIDS_API_PATH": "/api/config-client-do-not-webscrapes",
        "CONFIG_EXCLUDE_VISITING_DOMAINS_API_PATH": "/api/config-visiting-domain-exclusions",

        # The original values via get_config that re-created and returned these hard
        # coded values
        "edc_service_lookup_method_path": "/api/inbound-converter-urls",
        "completed_events_topic": "ibc.completed-activities",
        "failed_events_topic": "ibc.failed-events",
        "events_to_webscrape_topic": "ibc.events-to-webscrape",
        "kickfire_config_doc_id": "ibc-cf-configurations/kickfire-config",
        "maxmind_config_doc_id": "ibc-cf-configurations/maxmind-config",

        # NOTE: It was determined to be acceptable that the instances of this cloud function
        # may be using different collections.  The time of switching to a new collection is done
        # nightly at 2am and instances will refresh cache at 3am.  The purpose of previous_active_collection
        # was to keep all instances in sync but was deemed low need vs additional time for logic and testing
        # needed to implement - see firestore_config for kickfire and maxmind active collection
        # The cloud schedule job - kickfire_complete_check_job - time of execution to be hour before this value
        "cache_cleanup_hour_of_day": 3
    }


def get_config():
    """
    WARNING - this method when used as it was originally as top level single access
        to set a global value within a particular file will NOT reflect subsequent
        changes to the stored values - such as for testing.  A call should be used
        each time a particular configuration is needed - eg:
        get_config_value(key)

    :return: (dict) - of configuration keys and values
    """
    return cloud_function_execution_config


def get_config_value(key):
    """
    Retrieve configuration value with specified key.
    Use a method instead of referring to variable directly in other
    modules since changes to entries are not reflected after initial inclusion
    in other files.  eg. init, change, lookup uses value from init not change

    :param key: to obtain value of
    :return: type varies by key
    """
    return cloud_function_execution_config.get(key)


init_configs()
