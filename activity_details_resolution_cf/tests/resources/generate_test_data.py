import json
from os import listdir
from os.path import isfile, join

mypath = 'raw_logs'
input_path = 'input_data'
my_key_map = {
    "LOGSTASH_ID": "LOGSTASH_ID",
    "referrer": "referrer",
    "clientip": "ACTIVITY_IP",
    "ap-cid": "CID",
    "ap-version": "VERSION",
    "ap-refurl": "REF_PARAM",
    "agent": "USER_AGENT"

}

for f in listdir(mypath):
    if isfile(mypath + '/' + f):
        with open(mypath + '/' + f) as o:
            data = json.loads(o.read())['_source']

        dict_you_want = {value: data[key] for key, value in my_key_map.items()}
        with open(input_path + '/' + f, "w") as out:
            out.write(json.dumps(dict_you_want, indent=2))
