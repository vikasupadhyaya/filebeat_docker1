"""
Unit tests for micro_service_config_retrieval
"""
import pytest
import helpers.micro_service_config_retrieval as micro_service_config_retrieval
import logging
import config as cf_configs

logger = logging.getLogger(__name__)


def test_retrieve_gdpr_exclude_cids():
    cf_configs.set_gcp_project_config("tt-pd-eng")
    cf_configs.set_ibc_host_config("http://primary-edc-inboundconverter.eng.techtarget.com")

    results = micro_service_config_retrieval.get_gdpr_exclude_cids()
    assert len(results) > 0

    logger.info("{} - {}".format(len(results), results))
    for item in results:
        logger.info("{}".format(type(item)))
