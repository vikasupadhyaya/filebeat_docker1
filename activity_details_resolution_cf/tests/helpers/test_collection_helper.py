import pytest
import logging
import helpers.collection_helper as collection_helper

logger = logging.getLogger(__name__)

test_collection = ["1", 2, "", "abc"]
empty_collection = []


@pytest.mark.parametrize(
    "str_or_int_value, the_collection, expected", [
        (1, test_collection, True),
        ("1", test_collection, True),
        (2, test_collection, True),
        ("3", test_collection, False),
        ("", test_collection, True),
        ({}, test_collection, False),
        (None, test_collection, False),
        ("abc", test_collection, True),
        ("def", test_collection, False),
        (1, empty_collection, False)
    ]
)
def test_collection_contains(str_or_int_value, the_collection, expected):
    """
    Unit test for various inputs to collection_contains.
    Use env property to disable emulators if manually testing without those started.
    See conftest.py:pytest_sessionstart()

    :param str_or_int_value:
    :param the_collection:
    :param expected:
    :return: None
    """
    result = collection_helper.collection_contains(str_or_int_value, the_collection)
    assert result == expected
