import pytest
import helpers.bigquery_retrieve_expired_cids as bigquery_retrieve_expired_cids
import logging

logger = logging.getLogger(__name__)


def test_get_do_not_process_cids():
    results = bigquery_retrieve_expired_cids.get_do_not_process_cids()
    assert len(results) > 0

    logger.info("{} - {}".format(len(results), results))
    """
    for item in results:
        logger.info("{}".format(type(item)))
    """
