import pytest
import helpers.retrieve_config_values_from_file as retrieve_config_values_from_file
import logging

logger = logging.getLogger(__name__)


def test_retrieve_file():
    """
    See conftest.py:pytest_sessionstart() for environment variable to set
    to disable Emulators when running manually without Emulators running.

    :return: None
    """
    expected_line_count = 16
    test_file_name = "blacklist_urls.txt"
    content = retrieve_config_values_from_file.get_resource_file_content(test_file_name)
    assert len(content) == expected_line_count
    logger.debug("type: {}".format(type(content)))
    logger.debug("content ({}): {}".format(len(content), content))
