"""
Unit tests for cached_configs
"""
import pytest
import logging
from unittest.mock import patch
import cached_configs
from datetime import datetime, timezone, timedelta
import config as cf_configs

logger = logging.getLogger(__name__)


def test_cache_used_or_initialized():
    cf_configs.set_gcp_project_config("tt-pd-eng")
    cf_configs.set_ibc_host_config("http://primary-edc-inboundconverter.eng.techtarget.com")

    now_datetime = datetime.now()
    cache_cleanup_hour_of_day = 3
    now_datetime_3am = now_datetime.replace(hour=cache_cleanup_hour_of_day, minute=0, second=0, microsecond=0)

    # mock now_datetime to be 1 hour before the today's 3AM
    mocked_now_datetime = now_datetime_3am - timedelta(hours=1)

    # first call - no cache present
    cache_used = cached_configs.__private_initialize_or_refresh_cache_values(mocked_now_datetime, cache_cleanup_hour_of_day)
    assert not cache_used

    # at this point, the global_config_last_updated_datetime will be set to today's 2AM,
    # the current time is still less than 3AM, so
    # calling the function again should result in using the cache
    cache_used = cached_configs.__private_initialize_or_refresh_cache_values(mocked_now_datetime, cache_cleanup_hour_of_day)
    assert cache_used

    # now mock now_datetime to be 1 hour AFTER the today's 3am -
    # since the global_config_last_updated_datetime is still  today's 2AM  -
    # the cache should be treated as expired and should be refreshed
    mocked_now_datetime = now_datetime_3am + timedelta(hours=1)
    cache_used = cached_configs.__private_initialize_or_refresh_cache_values(mocked_now_datetime, cache_cleanup_hour_of_day)
    assert not cache_used


def test_configurations_have_values():
    cf_configs.set_gcp_project_config("tt-pd-eng")
    cf_configs.set_ibc_host_config("http://primary-edc-inboundconverter.eng.techtarget.com")

    cached_configs.initialize_or_refresh()
    gdpr_exclude_cids = cached_configs.get_gdpr_exclude_cids()
    expired_cids = cached_configs.get_expired_cids()
    blacklist_urls = cached_configs.get_blacklist_urls()
    do_not_webscrape_cids = cached_configs.get_do_not_webscrape_cids()
    exclude_visiting_domains = cached_configs.get_exclude_visiting_domains()

    assert len(gdpr_exclude_cids) > 0
    assert len(expired_cids) > 0
    assert len(blacklist_urls) > 0
    assert len(do_not_webscrape_cids) > 0
    assert len(exclude_visiting_domains) > 0


def test_configurations_have_values_from_test_service():
    cf_configs.set_gcp_project_config("tt-pd-eng")
    cf_configs.set_ibc_host_config("http://192.168.30.115:8080")

    cached_configs.initialize_or_refresh()
    gdpr_exclude_cids = cached_configs.get_gdpr_exclude_cids()
    expired_cids = cached_configs.get_expired_cids()
    blacklist_urls = cached_configs.get_blacklist_urls()
    do_not_webscrape_cids = cached_configs.get_do_not_webscrape_cids()
    exclude_visiting_domains = cached_configs.get_exclude_visiting_domains()

    logger.info("gdpr_exclude_cids: {}".format(gdpr_exclude_cids))
    logger.info("expired_cids: {}".format(expired_cids))
    logger.info("blacklist_urls: {}".format(blacklist_urls))
    logger.info("do_not_webscrape_cids: {}".format(do_not_webscrape_cids))
    logger.info("exclude_visiting_domains: {}".format(exclude_visiting_domains))

    assert len(gdpr_exclude_cids) > 0
    assert len(expired_cids) > 0
    assert len(blacklist_urls) > 0
    assert len(do_not_webscrape_cids) > 0
    assert len(exclude_visiting_domains) > 0
