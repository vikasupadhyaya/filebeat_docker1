"""
Manual tests for firestore_config

Tests may require special setup and tester verification
"""
import pytest
import pytest_cov
import logging
from firestore_config import get_kickfire_collection
from firestore_config import __private_get_maxmind_config_from_firestore
from firestore_config import ensure_initialized
from tests.lib.gcp import firestore
from datetime import datetime, timezone, timedelta

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test with no assertions")
def test_get_kickfire_collection():
    logger.info("result...{}".format(get_kickfire_collection()))


@pytest.mark.skip(reason="manual test with no assertions")
def test_get_missing_kickfire_doc():
    fs_client = firestore.Client("tt-temp-2021030444")
    kickfire_config_doc = fs_client.document("ibc-cf-configurations/kickfire-config").get()

    if kickfire_config_doc.exists:
        kickfire_config = kickfire_config_doc.to_dict()
        # use bracket access because we want to fail if these properties are not present
        active_collection = kickfire_config["active_collection"]
        staging_collection = kickfire_config["staging_collection"]

        # adjusts to be in UTC time +00:00 - eg if shown as -5 in firestore, add 5 hours
        active_last_updated = kickfire_config["active_collection_last_updated"]
        logger.info("last updated: {}".format(active_last_updated))
        logger.info("active: '{}'; staging: '{}'".format(active_collection, staging_collection))
    else:
        logger.info("doc does not exist")


@pytest.mark.skip(reason="manual test with no assertions")
def test_get_missing_maxmind_doc():
    """
    This CF does not use gcp project environment to initialize firestore Client, so set
    environment value GOOGLE_CLOUD_PROJECT to project test is to run against.

    :return: None
    """
    ensure_initialized()
    maxmind_collection = __private_get_maxmind_config_from_firestore()
    logger.info("maxmind configuration returned from firestore: {}".format(maxmind_collection))


@pytest.mark.skip(reason="manual test with no assertions")
def test_time_stuff():
    now_datetime = datetime.now()
    now_datetime_3am = now_datetime.replace(hour=3, minute=0)
    now_datetime_3am = now_datetime.replace(hour=3, minute=0, second=0, microsecond=0)
    logger.info("'{}' - '{}'".format(now_datetime, now_datetime_3am))
    logger.info("to utc: {}".format(now_datetime_3am.utcnow()))

    seq = int(datetime.now().strftime("%Y%m%d%H%M%S"))
    utc_now = datetime.now(timezone.utc)
    utc_now.utcnow()
    logger.info("{} - {}".format(seq, utc_now))
    logger.info("now: {}".format(now_datetime))
    logger.info("now to utc: {}".format(now_datetime.utcnow()))
