import json
import pytest
import main
import requests_mock

from unittest.mock import patch

from tests.conftest import read_resource_file, create_event_from_json
from datetime import datetime, timezone

# TODO fix Firestore mock
@pytest.mark.parametrize(
    "input_filename, expected_filename", [
        ("1_input_good.json", "1_expected_webscrape_cache_hit.json"),
        ("2_input_bad_ip.json", "2_expected_bad_ip.json")
    ]
)
def test_process_event(
        input_filename, generate_context, expected_filename,
        monkeypatch, get_clients
):
    directory = "process_event"
    edc_service_url = "https://mocked_url"
    edc_service_endpoint = 'https://mocked_url/api/inbound-converter-urls'
    gdpr_rest_url = 'https://mocked_url/api/client-exclusions'
    response_json = json.loads(read_resource_file(directory, "1_rest_json_response.json"))
    # TODO fix mock return from config
    gdpr_response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    input_event = create_event_from_json(directory, input_filename, "attributes")
    expected_result_event = json.loads(read_resource_file(directory, expected_filename))

    with requests_mock.Mocker() as request_mocker:
        with patch('main.futures') as mock_futures:
            request_mocker.register_uri('GET', edc_service_endpoint, status_code=200, json=response_json)
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=gdpr_response_json)
            monkeypatch.setattr("main.project", "tt-pd-eng")
            monkeypatch.setattr("main.EDC_REST_URL", edc_service_url)
            monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")

            mock_futures.return_value = None
            # set all dates in the input and result events to the current date and time
            now = datetime.utcnow()
            now_millis = int(now.timestamp() * 1000)

            input_event['attributes']['ACTIVITY_DATE'] = now_millis
            # ACTIVITY_DATE is converted into year-day-month format in the main code
            # have to specify the timezone explicitly
            expected_result_event['ACTIVITY_DATE'] = now.astimezone(timezone.utc).strftime('%Y-%m-%d')
            expected_result_event['ACTIVITY_MILLIS'] = now_millis

            result_event = main.process_event(input_event, generate_context)

            # verify audit columns are greater or equal than test start, with seconds granularity
            date_created = datetime.strptime(result_event['DATE_CREATED'], '%Y-%m-%d %H:%M:%S')
            # get rid of microseconds part
            now_without_microseconds = now.replace(microsecond=0)
            assert date_created >= now_without_microseconds
            assert datetime.strptime(result_event['DATE_MODIFIED'], '%Y-%m-%d %H:%M:%S') >= now_without_microseconds

            # replace expected values with actual for full event assertion
            expected_result_event['DATE_CREATED'] = result_event['DATE_CREATED']
            expected_result_event['DATE_MODIFIED'] = result_event['DATE_MODIFIED']

            assert result_event == expected_result_event


# TODO fix Firestore mock
def test_process_event_global_webscrape_off(
        generate_context, monkeypatch, get_clients
):
    directory = "process_event"
    edc_service_url = "https://mocked_url"
    edc_service_endpoint = 'https://mocked_url/api/inbound-converter-urls'
    gdpr_rest_url = 'https://mocked_url/api/client-exclusions'
    # TODO fix mock return from config
    gdpr_response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    input_event = create_event_from_json(directory, "1_input_good.json", "attributes")
    expected_result_event = json.loads(read_resource_file(directory, "3_expected_webscraping_off.json"))
    # simulate no response from the page content REST service - but not an error
    response_json = {}

    with requests_mock.Mocker() as request_mocker:
        with patch('main.futures') as mock_futures:
            request_mocker.register_uri('GET', edc_service_endpoint, status_code=200, json=response_json)
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=gdpr_response_json)
            monkeypatch.setattr("main.EDC_REST_URL", edc_service_url)
            # set GLOBAL_WEBSCRAPING_ENABLED to FALSE  - turn off webscraping
            monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "false")
            monkeypatch.setattr("main.project", "tt-pd-eng")
            mock_futures.return_value = None
            # do not bother with dates - we will only validate STATUS and other relevant fields

            result_event = main.process_event(input_event, generate_context)

            # verify only fields that matter for this test case
            event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'CID', 'REF_PARAM',
                                      'EU_ALLOWED', 'IS_ISP', 'REGION_CD', 'PAGE_TYPE',
                                      'ERRORS', 'STATUS', 'DOMAIN', 'PAGE_TITLE', 'PAGE_TEXT_COUNT', 'PAGE_CONTENT']
            for field in event_fields_to_verify:
                print("Comparing field: {} ...".format(field))
                assert result_event.get(field) == expected_result_event.get(field)


def test_process_event_rest_call_error(
        generate_context, monkeypatch, get_clients
):
    directory = "process_event"
    # simulate bad formatted URL - no https:// - this will cause an Exception
    # to be thrown from the REST call
    edc_service_url = "BAD_REST_URL"
    gdpr_rest_url = 'BAD_REST_URL/api/client-exclusions'
    # TODO fix mock return from config
    gdpr_response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    input_event = create_event_from_json(directory, "1_input_good.json", "attributes")
    expected_result_event = json.loads(read_resource_file(directory, "4_expected_restcall_error.json"))
    # simulate no response from the REST service - but not an error
    response_json = {}

    with requests_mock.Mocker() as request_mocker:
        with patch('main.futures') as mock_futures:
            request_mocker.register_uri('GET', edc_service_url, status_code=200, json=response_json)
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=gdpr_response_json)
            monkeypatch.setattr("main.EDC_REST_URL", edc_service_url)
            monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")
            monkeypatch.setattr("main.project", "tt-pd-eng")
            mock_futures.return_value = None
            # do not bother with dates - we will only validate STATUS and other relevant fields

            result_event = main.process_event(input_event, generate_context)

            # verify only fields that matter for this test case
            event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'CID', 'REF_PARAM',
                                      'EU_ALLOWED', 'IS_ISP', 'REGION_CD', 'PAGE_TYPE',
                                      'STATUS', 'DOMAIN', 'PAGE_TITLE', 'PAGE_TEXT_COUNT', 'PAGE_CONTENT']
            for field in event_fields_to_verify:
                assert result_event.get(field) == expected_result_event.get(field)
            # verify we have correct ERROR
            errors_values = result_event.get("ERRORS")
            assert len(errors_values) > 0
            first_error = errors_values[0]
            error_message = first_error.get("ERROR")
            # the first failure will come from the update to call the GDPR config service URL -
            # thus, the logged error will be from there
            assert error_message == 'Failed call to REST API'


def test_process_event_missing_required_fields(
        generate_context, monkeypatch, get_clients
):
    directory = "test_flow"
    # input event is missing one of the required fields: CID
    input_event = create_event_from_json(directory, "input_missing_required_field.json", "attributes")
    expected_result_event = json.loads(read_resource_file(directory, "expected_missing_required_field.json"))

    with patch('main.futures') as mock_futures:
        monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")
        monkeypatch.setattr("main.project", "tt-pd-eng")
        mock_futures.return_value = None
        # do not bother with dates - we will only validate STATUS and other relevant fields
        result_event = main.process_event(input_event, generate_context)

        # verify only fields that matter for this test case
        event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'REF_PARAM', 'STATUS']
        for field in event_fields_to_verify:
            print("verifying field: {}".format(field))
            assert result_event.get(field) == expected_result_event.get(field)
        # verify we have correct ERROR
        errors_values = result_event.get("ERRORS")
        assert len(errors_values) > 0
        first_error = errors_values[0]
        error_message = first_error.get("ERROR")
        assert error_message == 'MISSING_REQUIRED_FIELDS'


def test_data_tag_as_data(
        generate_context, monkeypatch, get_clients
):
    directory = "test_flow"
    data_format = "data"
    # input event is missing one of the required fields: CID
    input_event = create_event_from_json(directory, "input_missing_required_field.json", data_format)
    print("test_data_tag_as_data(): input_event = {}".format(input_event))
    expected_result_event = json.loads(read_resource_file(directory, "expected_missing_required_field.json"))

    with patch('main.futures') as mock_futures:
        monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")
        monkeypatch.setattr("main.project", "tt-pd-eng")
        mock_futures.return_value = None
        # do not bother with dates - we will only validate STATUS and other relevant fields
        result_event = main.process_event(input_event, generate_context)

        # verify only fields that matter for this test case
        event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'REF_PARAM', 'STATUS']
        for field in event_fields_to_verify:
            print("verifying field: {}".format(field))
            assert result_event.get(field) == expected_result_event.get(field)
        # verify we have correct ERROR
        errors_values = result_event.get("ERRORS")
        assert len(errors_values) > 0
        first_error = errors_values[0]
        error_message = first_error.get("ERROR")
        assert error_message == 'MISSING_REQUIRED_FIELDS'


def test_event_from_dataflow(
        generate_context, monkeypatch, get_clients
):
    directory = "test_flow"
    data_format = "data"
    # input event is missing one of the required fields: CID
    input_event = create_event_from_json(directory, "input_missing_required_field.json", data_format)
    # dataflow events have both 'data' with enccrypted event bode, and an empty 'attributes' element
    # example:
    # "{'@type': 'type.googleapis.com/google.pubsub.v1.PubsubMessage', 'attributes': None, 'data': 'eyJMT0dTVEF......'}"
    input_event['attributes'] = None
    print("test_data_tag_as_data(): input_event = {}".format(input_event))
    expected_result_event = json.loads(read_resource_file(directory, "expected_missing_required_field.json"))

    with patch('main.futures') as mock_futures:
        monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")
        monkeypatch.setattr("main.project", "tt-pd-eng")
        mock_futures.return_value = None
        # do not bother with dates - we will only validate STATUS and other relevant fields
        result_event = main.process_event(input_event, generate_context)

        # verify only fields that matter for this test case
        event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'REF_PARAM', 'STATUS']
        for field in event_fields_to_verify:
            print("verifying field: {}".format(field))
            assert result_event.get(field) == expected_result_event.get(field)
        # verify we have correct ERROR
        errors_values = result_event.get("ERRORS")
        assert len(errors_values) > 0
        first_error = errors_values[0]
        error_message = first_error.get("ERROR")
        assert error_message == 'MISSING_REQUIRED_FIELDS'


"""
NOTE on the data in the test datasets maxmind.jsp and kickfire.jsp used in all unit tests:
    kickfire.jsp is a subset of data that has IP ranges from 16777216 to 86742114 only
    maxmind.jsp is a subset of data covers IP ranges from 16777216 to 68123007 only
    
Test exit statuses that could happen before webscraping steps:
-- MATCHED_DO_NOT_PROCESS_CID
-- MATCHED_BLKLIST_URL
-- ERROR EXIT due to bad IP
-- MATCHED_GDPR_NOT_ALLOWED 
-- EMPTY_DOMAIN (for this test, the input IP does not resolve to any of the entries in either of the datasets explicitly:
        ip=103.246.41.53, int_ip=1744185653 
        instead it resolves to the highest available IP range in both datasets, 
        which fails check on the start and end range conditions and default values are used for DOMAIN and IS_ISP
        )
-- MATCHED_IS_ISP (for this test, added an entry to the kickfire.json for IP: 99.30.124.235 , int_ip = 1662942443;
    this IP is not matched in the maxmind dataset (for GDPR region)
-- MATCHED_VISITING_DOMAIN (for this test, added new entry to kickfire.jsp for ACTIVITY_IP=170.76.156.249, int_ip = 2857147641,
    with "Website": "test.visiting.domain1.com" - that matches a configured test visiting domain;
    it has no matching entry in the maxmind dataset)
"""

# TODO fix Firestore mock
@pytest.mark.parametrize(
    'input_filename, expected_filename', [
        ('input_donotprocess_cid.json', 'expected_donotprocess_cid.json'),
        ('input_blacklisted_url.json', 'expected_blacklisted_url.json'),
        ("input_bad_ip_exit_error.json", "expected_bad_ip_exit_error.json"),
        ("input_gdpr_not_allowed.json", "expected_gdpr_not_allowed.json"),
        ("input_empty_domain.json", "expected_empty_domain.json"),
        ("input_matched_is_isp.json", "expected_matched_is_isp.json"),
        ("input_matched_visiting_domain.json", "expected_matched_visiting_domain.json")
    ]
)
def test_flow_before_webscraping(
        input_filename, expected_filename, generate_context, monkeypatch, get_clients
):
    directory = "test_flow"
    edc_service_url = "https://mocked_url"
    gdpr_rest_url = 'https://mocked_url/api/client-exclusions'
    # TODO fix mock return from config
    gdpr_response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    input_event = create_event_from_json(directory, input_filename, "attributes")
    expected_result_event = json.loads(read_resource_file(directory, expected_filename))

    with patch('main.futures') as mock_futures:
        with requests_mock.Mocker() as request_mocker:
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=gdpr_response_json)
            monkeypatch.setattr("main.EDC_REST_URL", edc_service_url)
            monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")
            monkeypatch.setattr("main.project", "tt-pd-eng")
            mock_futures.return_value = None
            # do not bother with dates - we will only validate STATUS and other relevant fields
            result_event = main.process_event(input_event, generate_context)
            assert result_event is not None
            # verify only fields that matter for this test case
            event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'REF_PARAM', 'EU_ALLOWED',
                                      'IS_ISP', 'REGION_CD', 'DOMAIN', 'STATUS', 'ERRORS']
            for field in event_fields_to_verify:
                print("verifying field: {}".format(field))
                assert result_event.get(field) == expected_result_event.get(field)


def test_lookup_scraping_info_from_db_good_match():
    directory = "test_rest_lookup"
    input_event = json.loads(read_resource_file(directory, "1_input_good.json"))
    expected_event = json.loads(read_resource_file(directory, "1_expected_good.json"))
    edc_service_url = "https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls"
    response_json = json.loads(read_resource_file(directory, "1_rest_json_response.json"))
    with requests_mock.Mocker() as request_mocker:
        request_mocker.register_uri('GET', edc_service_url, status_code=200, json=response_json)
        # after this call - input_event will be updated with new info
        result = main.lookup_scraping_info_from_db(input_event, edc_service_url)
        assert result is True
        assert input_event == expected_event


def test_lookup_scraping_info_from_db_no_match():
    directory = "test_rest_lookup"
    with requests_mock.Mocker() as request_mocker:
        input_event = json.loads(read_resource_file(directory, "2_input_no_match.json"))
        expected_event = json.loads(read_resource_file(directory, "2_expected_no_match.json"))
        edc_service_url = "https://primary-edc-inboundconverter.eng.techtarget.com/api/inbound-converter-urls"
        # simulate no response from the REST service - but not an error
        response_json = {}
        request_mocker.register_uri('GET', edc_service_url, status_code=200, json=response_json)
        # after this call - input_event will be updated with new info
        result = main.lookup_scraping_info_from_db(input_event, edc_service_url)
        # call to the REST service will be considered successful (returns True) even though
        # it returned empty response
        assert result is True
        assert input_event == expected_event



# This test verified the full happy path flow when there is no webscraped info found in the DB
# and GLOBAL_WEBSCRAPING_ENABLED is True
# TODO fix Firestore mock
def test_happy_path_webscraping_cache_miss(generate_context, monkeypatch, get_clients):
    directory = "process_event"
    edc_service_url = "https://mocked_url"
    edc_service_endpoint = 'https://mocked_url/api/inbound-converter-urls'
    gdpr_rest_url = 'https://mocked_url/api/client-exclusions'
    # TODO fix mock return from config
    gdpr_response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    input_event = create_event_from_json(directory, "1_input_good.json", "attributes")
    expected_result_event = json.loads(read_resource_file(directory, "6_expected_happy_path_cache_miss.json"))
    # simulate no response from the REST service - but not an error
    response_json = {}

    with requests_mock.Mocker() as request_mocker:
        with patch('main.futures') as mock_futures:
            request_mocker.register_uri('GET', edc_service_endpoint, status_code=200, json=response_json)
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=gdpr_response_json)
            monkeypatch.setattr("main.EDC_REST_URL", edc_service_url)
            # set GLOBAL_WEBSCRAPING_ENABLED to TRUE
            monkeypatch.setattr("main.GLOBAL_WEBSCRAPING_ENABLED_STR", "true")
            monkeypatch.setattr("main.project", "tt-pd-eng")
            mock_futures.return_value = None
            # do not bother with dates - we will only validate STATUS and other relevant fields
            result_event = main.process_event(input_event, generate_context)

            # verify only fields that matter for this test case
            event_fields_to_verify = ['LOGSTASH_ID', 'ACTIVITY_IP', 'CID', 'REF_PARAM',
                                      'EU_ALLOWED', 'IS_ISP', 'REGION_CD', 'PAGE_TYPE',
                                      'ERRORS', 'STATUS', 'DOMAIN', 'PAGE_TITLE', 'PAGE_TEXT_COUNT', 'PAGE_CONTENT']
            for field in event_fields_to_verify:
                print("Comparing field: {} ...".format(field))
                assert result_event.get(field) == expected_result_event.get(field)

            # STATUS field should be empty - double check
            assert result_event.get("STATUS") is None

