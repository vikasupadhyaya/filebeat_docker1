import json

import pytest
from lib.rules.page_type import *
from tests.conftest import read_resource_file


@pytest.mark.parametrize(
    'input_filename, expected_filename', [
        ('1_process_data.json', '1_expected.json'),
        ('2_process_data.json', '2_expected.json'),
        ('3_process_data.json', '3_expected.json'),
        ('4_process_data.json', '4_expected.json')
    ]
)
def test_classify_url(
        input_filename, expected_filename
):
    directory = 'classify_url'
    input_event = json.loads(read_resource_file(directory, input_filename))
    expected_result_event = json.loads(read_resource_file(directory, expected_filename))
    result_event = classify_url(input_event)
    assert result_event == expected_result_event
