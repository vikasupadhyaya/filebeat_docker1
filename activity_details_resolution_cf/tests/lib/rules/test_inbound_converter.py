import pytest
from unittest.mock import patch
import copy

import requests_mock

from lib.rules.inbound_converter import *
from tests.conftest import read_resource_file
from tests.conftest import read_resource_file_as_set
from datetime import datetime


# TODO add more unit tests for:
# -- empty response from the REST urL for GDPR
# -- failure from the Firestore call


@pytest.mark.parametrize(
    'input_filename, expected_missing_fields_list', [
        ("1_input_nothing_missing.json", []),
        ("2_input_missing_cid.json", ["CID"])
    ]
)
def test_missing_required_fields(input_filename, expected_missing_fields_list):
    directory = "missing_required_fields"
    input_event = json.loads(read_resource_file(directory, input_filename))
    result = get_missing_required_fields(input_event)
    assert result == expected_missing_fields_list


def test_add_partition_columns():
    input_event = {}
    now_est = datetime.now()
    now_utc = datetime.utcnow()
    now_millis = int(now_est.timestamp() * 1000)
    input_event['ACTIVITY_DATE'] = now_millis
    expected_output = copy.deepcopy(input_event)
    # ACTIVITY_DATE is converted into year-day-month format in the main code
    # verify date is correctly set using UTC value
    expected_output['ACTIVITY_DATE'] = now_utc.strftime('%Y-%m-%d')
    expected_output['ACTIVITY_MILLIS'] = now_millis

    result_event = add_partition_columns(input_event)
    assert result_event == expected_output


def test_add_partition_columns_with_explicit_activity_date():
    # GMT: Thursday,  May 27, 2021 1: 58:40.060AM
    # EST: Wednesday, May 26, 2021 9: 58:40.060PM
    input_event = {'ACTIVITY_DATE': 1622080720060}
    expected_output = copy.deepcopy(input_event)
    expected_output['ACTIVITY_DATE'] = '2021-05-27'
    expected_output['ACTIVITY_MILLIS'] = 1622080720060

    result_event = add_partition_columns(input_event)
    assert result_event == expected_output


def test_add_audit_columns():
    with patch('lib.rules.inbound_converter.datetime') as dt_time:
        input_event = {}
        now = datetime.utcnow().strftime('%Y-%m-%d %X')
        dt_time.utcnow.return_value = dt_time
        dt_time.strftime.return_value = now
        expected_output = copy.deepcopy(input_event)
        expected_output['DATE_CREATED'] = now
        expected_output['DATE_MODIFIED'] = now

        result_event = add_audit_columns(input_event)
        assert result_event == expected_output


def test_decode_url():
    directory = "decode_url"
    input_event = json.loads(read_resource_file(directory, "1_encoded_input.json"))
    expected_result_event = json.loads(read_resource_file(directory, "1_expected_decoded.json"))
    result_event = decode_url(input_event)
    assert result_event == expected_result_event


@pytest.mark.parametrize(
    'input_filename, expected_filename, expected_call_status', [
        ('1_input_goodip_DK.json', '1_expected.json', 'True'),
        ('2_input_goodip_PL.json', '2_expected.json', 'True'),
        ('3_input_empty_domain.json', '3_expected.json', 'True'),
        ('4_input_bad_ip.json', '4_expected_bad_ip.json', 'False')
    ]
)
def test_lookup_gdpr_region(
        input_filename, expected_filename, expected_call_status, get_clients
):
    directory = 'get_gdpr_region'
    if expected_call_status == "True":
        expected_call_status_bool = True
    else:
        expected_call_status_bool = False

    input_event = json.loads(read_resource_file(directory, input_filename))
    expected_result_event = json.loads(read_resource_file(directory, expected_filename))
    result_call_status = lookup_gdpr_region(input_event)
    assert result_call_status == expected_call_status_bool
    assert input_event == expected_result_event


@pytest.mark.parametrize(
    'input_filename, expected_match_result', [
        ("1_input_no_match.json", False),
        ("2_input_exact_match.json", True),
        ("3_input_match_encoded.json", True),
        ("3_input_match_not_encoded.json", True),
        ("4_input_match_2_elements.json", True),
        ("5_input_match_3_elements.json", True),
        ("6_input_no_match_1_of_2_elements.json", False)
    ]
)
def test_match_blacklisted_urls(input_filename, expected_match_result):
    directory = 'match_blacklisted_urls'
    exclusion_rules = read_resource_file_as_set("blacklist_urls", "blacklist_urls.txt")
    input_event = json.loads(read_resource_file(directory, input_filename))
    match_result = match_blacklisted_urls(input_event, exclusion_rules)
    no_rules_result = match_blacklisted_urls(input_event, None)
    assert no_rules_result is False
    assert match_result == expected_match_result


@pytest.mark.parametrize(
    'input_filename, expected_match_result', [
        ("1_input_not_match.json", False),
        ("2_input_match.json", True)
    ]
)
def test_filter_visiting_domain(input_filename, expected_match_result):
    directory = "filter_visiting_domain"
    exclusion_rules = read_resource_file_as_set(directory, "visiting_domains.txt")
    input_event = json.loads(read_resource_file(directory, input_filename))
    match_result = filter_visiting_domain(input_event, exclusion_rules)
    assert match_result == expected_match_result


def test_filter_blacklist_all_urls():
    # list of URLs that should be blacklisted - but are still in the BQ tables:
    # testing getting encoded and decoded URLs
    urls_to_check = [
        "https://slack.com/signin?redir=%2Fmessages%2FCCJQLCNQZ",
        "https://slack.com/signin?redir=/messages/CCJQLCNQZ",
        "https://slack.com/signin?redir=%2Fhelp%2Ftest",
        "https://slack.com/signin?redir=/help/test",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=%2Fsupport%2Ftickets%2F",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=/support/tickets/",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=%2Fproducts-and-downloads%2F",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=/products-and-downloads/",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=/",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=%2F"
    ]
    # create input_event - with encoded URL as REF_PARAM
    input_event_1 = {
        "LOGSTASH_ID": "marina_id1",
        "REFERER": "https%3A%2F%2Fslack.com%2Fsignin?redir=%2Fhelp%2Ftest",
        "ACTIVITY_IP": "1.0.0.0",
        "ACTIVITY_DATE": "1607557877000",
        "ACTIVITY_TYPE_ID": "31",
        "CID": "123456789",
        "VERSION": "2.0",
        "REF_PARAM": "https://slack.com/signin?redir=%2Fmessages%2FCCJQLCNQZ",
        "USER_AGENT": "Mozilla/5.0 (X11; CrOS aarch64 13421.102.0) blah blah"
    }
    # this URL should be blacklisted - so expected result should be "True"
    expected_result = True
    exclusion_rules = read_resource_file_as_set("blacklist_urls", "blacklist_urls.txt")
    # test all urls from the list
    for url_to_check in urls_to_check:
        input_event_1['REF_PARAM'] = url_to_check
        input_event_data = json.loads(json.dumps(input_event_1))
        # TODO add collection to check
        result = match_blacklisted_urls(input_event_data, exclusion_rules)
        assert result == expected_result


@pytest.mark.parametrize(
    'directory, input_filename, expected_match_result', [
        ('filter_cids', "1_input_no_match.json", False),
        ('filter_cids', "2_input_match.json", True)
    ]
)
def test_filter_event_by_cid_in_set(
        directory, input_filename, expected_match_result):
    # TODO change filter_cids call to be the generic one - filter_event_by_cid_in_set
    input_event = json.loads(read_resource_file(directory, input_filename))
    exclusion_rules = read_resource_file_as_set("do_not_webscrape", "do_not_webscrape_cids.txt")
    match_result = filter_event_by_cid_in_set(input_event, exclusion_rules, "Do not webscrape test")
    assert match_result == expected_match_result


# This test uses real data from the config_donotprocess_cids collection in Firestore
# and calls real REST URL for microservice that returns other config (gdpr)
# run this test manually if you have your GCP auth setup locally and a correct REST URL for ENG
@pytest.mark.parametrize(
    'directory, input_filename, expected_match_result', [
        ('filter_cids', "1_input_no_match.json", False),
        ('filter_cids', "3_input_match_in_firestore.json", True)
    ]
)
def test_filter_expired_cids_from_gcp(
        directory, input_filename, expected_match_result):
    input_event = json.loads(read_resource_file(directory, input_filename))
    exclusion_rules = read_resource_file_as_set("do_not_process", "do_not_process_cids.txt")
    match_result = filter_event_by_cid_in_set(input_event, exclusion_rules, "expired cids test")
    assert match_result == expected_match_result


# This test uses mocked data from the lib/gcp/firestore/config_donotprocess_cids.json
# and mocked REST requests - with none of the test CIDs matching the returned ones
@pytest.mark.parametrize(
    'directory, input_filename, expected_match_result', [
        ('filter_cids', "1_input_no_match.json", False),
        ('filter_cids', "2_input_match.json", True)
    ]
)
def test_filter_expired_cids_happy_path(directory, input_filename, expected_match_result, monkeypatch):
    input_event = json.loads(read_resource_file(directory, input_filename))
    # TODO fix mock return from config
    response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    # mock REST service to return correct results for gdpr config
    gdpr_rest_url = 'https://mocked_url/api/client-exclusions'
    exclusion_rules = set()
    with patch('lib.rules.inbound_converter.firestore.Client') as mock_firestore:
        with requests_mock.Mocker() as request_mocker:
            mock_firestore.return_value = firestore.Client()
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=response_json)
            match_result = filter_event_by_cid_in_set(input_event, exclusion_rules, "expired cids happy path test")
            assert match_result == expected_match_result


# This test uses mocked data from the lib/gcp/firestore/config_donotprocess_cids.json
# This test verifies that if a call to the REST service to update GDPR CIDs cache fails -
#   the call to get expired CIDs also fails as those caches are now loaded / refreshed at the same time
# and failure to get one of them will cause an early error exit
# NOTE : this test relies on clean cache of the configs - and thus has to be called manually
#   as a standalone test; otherwise, other tests may have already loaded valid configs and cahce will be used
@pytest.mark.parametrize(
    'directory, input_filename, expected_match_result', [
        ('filter_cids', "1_input_no_match.json", False),
        ('filter_cids', "2_input_match.json", False)
    ]
)
@pytest.mark.skip(reason="this test has to be run manually as a standalone test")
def test_filter_expired_cids_rest_failure(
        directory, input_filename, expected_match_result):
    input_event = json.loads(read_resource_file(directory, input_filename))
    # define configuration to return no data
    exclusion_rules = set()
    with patch('lib.rules.inbound_converter.firestore.Client') as mock_firestore:
        mock_firestore.return_value = firestore.Client()
        match_result = filter_event_by_cid_in_set(input_event, exclusion_rules, "expired cids rest failure test")
        assert match_result == expected_match_result


@pytest.mark.parametrize(
    'input_filename, expected_filename', [
        ("1_input_eu_allowed.json", "1_expected_eu_allowed.json"),
        ("2_input_eu_not_allowed.json", "2_expected_eu_not_allowed.json"),
        ("3_input_eu_not_allowed_with_override.json", "3_expected_eu_not_allowed_with_override.json")
    ]
)
def test_set_eu_allowed(input_filename, expected_filename, monkeypatch):
    directory = 'eu_allowed'
    # mock REST service to return correct results for gdpr config
    gdpr_rest_url = 'https://mocked_url/api/client-exclusions'
    # TODO fix mock return from config
    response_json = json.loads(read_resource_file('filter_cids', "rest_response_gdpr_cids.json"))
    input_event = json.loads(read_resource_file(directory, input_filename))
    expected_result_event = json.loads(read_resource_file(directory, expected_filename))
    # TODO fix for exclusion_rules to load
    exclusion_rules = set()
    with patch('lib.rules.inbound_converter.firestore.Client') as mock_firestore:
        with requests_mock.Mocker() as request_mocker:
            request_mocker.register_uri('GET', gdpr_rest_url, status_code=200, json=response_json)
            mock_firestore.return_value = firestore.Client()
            # TODO update cached_config to return expected values
            result_event = set_eu_allowed(input_event, exclusion_rules)
            assert result_event == expected_result_event


@pytest.mark.parametrize(
    'input_filename, expected_filename', [
        ('1_input_isp_false.json', '1_expected.json'),
        ('2_input_hetzner_isp_false.json', '2_expected.json'),
        ('3_input_isp_true.json', '3_expected.json'),
        ('4_input_bad_ip.json', '4_expected_bad_ip.json')
    ]
)
def test_resolve_domain_and_isp(
        input_filename, expected_filename
):
    directory = 'resolve_domain_and_isp'
    input_event = json.loads(read_resource_file(directory, input_filename))
    expected_result_event = json.loads(read_resource_file(directory, expected_filename))
    result_event = resolve_domain_and_isp(input_event)
    assert result_event == expected_result_event
