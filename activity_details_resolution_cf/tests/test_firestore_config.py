"""
Unit tests for firestore_config
"""
import pytest
import logging
from unittest.mock import patch
from firestore_config import __private_initialize_or_refresh_cache_values
from datetime import datetime, timezone, timedelta
from google.cloud import firestore

logger = logging.getLogger(__name__)


def test_cache_usage():
    with patch('firestore_config.__private_get_kickfire_config_from_firestore') as mock_firestore:
        mock_firestore.return_value = "kickfire-collection"
        now_datetime = datetime.now()
        cache_cleanup_hour_of_day = 3
        now_datetime_3am = now_datetime.replace(hour=cache_cleanup_hour_of_day, minute=0, second=0, microsecond=0)

        # mock now_datetime to be 1 hour before the today's 3AM
        mocked_now_datetime = now_datetime_3am - timedelta(hours=1)

        # first call - no cache present
        cache_used = __private_initialize_or_refresh_cache_values(mocked_now_datetime, cache_cleanup_hour_of_day)
        assert not cache_used

        # at this point, the global_config_last_updated_datetime will be set to today's 2AM,
        # the current time is still less than 3AM, so
        # calling the function again should result in using the cache
        cache_used = __private_initialize_or_refresh_cache_values(mocked_now_datetime, cache_cleanup_hour_of_day)
        assert cache_used

        # now mock now_datetime to be 1 hour AFTER the today's 3am -
        # since the global_config_last_updated_datetime is still  today's 2AM  -
        # the cache should be treated as expired and should be refreshed
        mocked_now_datetime = now_datetime_3am + timedelta(hours=1)
        cache_used = __private_initialize_or_refresh_cache_values(mocked_now_datetime, cache_cleanup_hour_of_day)
        assert not cache_used
