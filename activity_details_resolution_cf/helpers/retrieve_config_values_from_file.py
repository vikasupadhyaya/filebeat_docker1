import os
import logging

logger = logging.getLogger(__name__)

# The folder under location of this file where files are located to load
RESOURCES_DIR = "resources"


def get_resource_file_content(file_name):
    """
    Obtain the values from file_name, storing results in a set.
    Each unique line of the file is an entry in returned set.
    We opt to use a set vs a list since sets are much more performant for large
    collections.

    :param (str) file_name: to load content from
    :return: (set) - representing contents of file_name; each entry is a str type
    """
    return_value = set()
    my_path = os.path.abspath(os.path.dirname(__file__))
    with open(os.path.join(my_path, RESOURCES_DIR, file_name)) as f:
        file_content = f.read().splitlines(keepends=False)

    if file_content:
        return_value = set(file_content)

    return return_value
