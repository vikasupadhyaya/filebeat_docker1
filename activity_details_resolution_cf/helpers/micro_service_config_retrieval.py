"""
Routines to retrieve all values from an endpoint using batch retrieval with
page specifier since api is limited to returning 2000 max records at a time and
the endpoints for configurations could exceed 2000 entries.
"""
import requests
import logging
import config as cf_configs

logger = logging.getLogger(__name__)

# The micro-service limit of number of records returned in a single request
max_page_size = 2000


def __retrieve_page_of_configurations(base_request_url, page_num):
    """
    Use base_request_url and add page and size to url as additional
    criteria for retrieval.

    This implementation is expected to be used for configurations where the returned
    results from api contain a 'value' attribute and that is value used in constructing
    the returned list.

    :param (str) base_request_url: base request
    :param (int) page_num: page number of request
    :return: (list) of obtained results
    """
    logger.debug("retrieve_page() - {} - page {}".format(base_request_url, page_num))

    """
    ensure we only request microservice max

    we always request full page size if we are on subsequent pages since the retrieval
    of records uses size * page as offset for which records to retrieve and why
    we must retrieve max_page_size if after first page (page 0) and then only
    use the needed number of records in returned list
    """
    request_url = base_request_url + "&page=" + str(page_num) + "&size=" + str(max_page_size)
    page_results = []

    try:
        response = requests.get(request_url)
        json_response = response.json()

        # no results not logged here since zero results for arbitrary page may
        # be due to last record on previous page - log zero configurations at top level
        if json_response:
            # json_response is a list of objects with an attribute 'value' - where this value is the
            # configuration value applicable to request
            list_of_ids = map(lambda item: item.get("value"), json_response)
            page_results = list(list_of_ids)

        log_message = "Completed call to API: returning: ({}) - {}; obtained: ({}) - {}; {}"
        logger.debug(log_message.format(len(page_results), page_results,
                                        len(json_response), json_response,
                                        request_url))
    except Exception as e:
        logger.error("Problem retrieving page records: {}".format(str(e)))
        raise e

    return page_results


def __retrieve_pages_of_configurations(request_url):
    """
    Iterate pages of the request_url until all configurations are retrieved,
    combining into a single result set.

    :param (str) request_url - configuration records to retrieve
    :return: (set) - of all configurations from request_url
    """
    page_num = 0
    accumulated_records = []

    while True:
        page_results = __retrieve_page_of_configurations(request_url, page_num)
        accumulated_records += page_results
        page_num += 1
        if len(page_results) < max_page_size:
            break

    if len(accumulated_records) == 0:
        logger.info("No configurations present: {}".format(request_url))

    logger.debug("Retrieved {} configurations for: {}".format(len(accumulated_records), request_url))
    return set(accumulated_records)


def __retrieve_all_configurations(configuration_endpoint):
    """
    Wrapper to construct the url used in obtaining all configurations.

    :param (str) configuration_endpoint: api endpoint for retrieving all records (supplemental details are
        added internally to sort and make page requests)
    :return: (set) - of configurations retrieved
    """
    # use base sort to ensure records are returned in consistent order across pages
    base_url = str(cf_configs.get_config_value("IBC_HOST")).rstrip("/") + str(configuration_endpoint) + "?sort=id%2Casc"
    return __retrieve_pages_of_configurations(base_url)


def get_gdpr_exclude_cids():
    """
    Obtain the client ids which can override the GDPR restrictions.

    Original implementation stored values in 'gdpr_exclude_override_cids.txt' file.

    :return: (set) - of client ids that override the GDPR rules for client region
    """
    api_endpoint_for_configuration = cf_configs.get_config_value("CONFIG_GDPR_EXCLUDE_OVERRIDE_CIDS_API_PATH")
    results = __retrieve_all_configurations(api_endpoint_for_configuration)
    logger.info("Configurations retrieved for GDPR Exclusions: {}".format(results))
    return results


def get_blacklist_urls():
    """
    Obtain the blacklist url rules configurations.

    Original implementation stored values in 'blacklist_urls.txt' file.

    :return: (set) - of url patterns/rules for determining if event should be processed
    """
    api_endpoint_for_configuration = cf_configs.get_config_value("CONFIG_BLACKLIST_URLS_API_PATH")
    results = __retrieve_all_configurations(api_endpoint_for_configuration)
    logger.info("Configurations retrieved for Blacklist Url Rules: {}".format(results))
    return results


def get_do_not_webscrape_cids():
    """
    Obtain the client ids which are configured to not be web scraped.

    Original implementation stored values in 'do_not_webscrape_cids.txt' file.

    :return: (set) - of client ids that should be excluded from web scraping
    """
    api_endpoint_for_configuration = cf_configs.get_config_value("CONFIG_DO_NOT_WEBSCRAPE_CIDS_API_PATH")
    results = __retrieve_all_configurations(api_endpoint_for_configuration)
    logger.info("Configurations retrieved for Do Not Webscrape Client Ids: {}".format(results))
    return results


def get_exclude_visiting_domains():
    """
    Obtain the configured values of visiting domains that should cause event to be filtered and not processed.

    Original implementation stored values in 'visiting_domains.txt' file.

    :return: (set) - of domains which cause event to be filtered and not processed
    """
    api_endpoint_for_configuration = cf_configs.get_config_value("CONFIG_EXCLUDE_VISITING_DOMAINS_API_PATH")
    results = __retrieve_all_configurations(api_endpoint_for_configuration)
    logger.info("Configurations retrieved for Exclude Visiting Domains: {}".format(results))
    return results
