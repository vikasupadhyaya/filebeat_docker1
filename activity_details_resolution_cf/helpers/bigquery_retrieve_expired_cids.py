"""
Helper file to retrieve expired client ids from BigQuery
"""
import logging
from google.cloud import bigquery

logger = logging.getLogger(__name__)


def get_do_not_process_cids():
    """
    Refactored out of ibc_config_load_cf to go directly to source of truth - BigQuery view - rather
    than a BQ->firestore process that added unnecessary complexity and processes.

    :return: (set) - of client ids which are not to be processed (expired clients)
    """
    bq_client = bigquery.Client()
    original_query_sql = \
        """
        select distinct
            client_access_id,
            client_access_key,
            expired_client_company.aiv_company_id as client_id,
            company.company_domain as domain,
            date_start,
            date_end
        from
            analytics_pe.client_subscription expired,
            analytics_activity.company,
            unnest(client_company) expired_client_company
        where
            expired_client_company.product_type='WEBSITE_TRACKING'
            and expired_client_company.aiv_company_id = company.aiv_company_id
            and not exists (
                select 1 from analytics_pe.client_subscription valid, unnest(client_company) valid_client_company
                where
                    expired_client_company.aiv_company_id = valid_client_company.aiv_company_id
                    and valid_client_company.product_type='WEBSITE_TRACKING'
                    and date_start <= current_timestamp()
                    and date_end >= timestamp_sub(current_timestamp(), interval 90 day))
        """

    query_view_sql = \
        """
        select distinct client_id
        from
            analytics_inboundconverter.expired_cids
        """
    logger.debug("Running the query to BQ: {}".format(query_view_sql))
    query_job = bq_client.query(query_view_sql)
    results = query_job.result()  # Waits for job to complete
    expired_cids_list = []
    for row in results:
        expired_cids_list.append(row.client_id)
    logger.debug("Retrieved from BQ: expired_cids_list={}".format(expired_cids_list))
    return set(expired_cids_list)
