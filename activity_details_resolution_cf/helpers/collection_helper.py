"""
A helper for collections - list or set - related logic common to CF
"""
import logging

logger = logging.getLogger(__name__)


def collection_contains(str_or_int_value, the_collection):
    """
    Collections of configurations for client ids are mixed between int and str values
    depending on source of loading.  To consolidate logic and not care if the list
    has str values or int values of client ids, this will check both.  Use regular 'in' condition
    if we need to be specific on type.

    :param (str or int) str_or_int_value: to determine if exists in the_collection
    :param (set or list) the_collection: containing values that str_or_int_value may appear in
    :return: (boolean) - True if str_or_int_value as int or str appears in the_collection; False otherwise
    """
    matched_value_from_collection = False
    if isinstance(str_or_int_value, (int, str)):
        str_value = str(str_or_int_value)
        if str_value.isnumeric():
            int_value = int(str_or_int_value)
        else:
            int_value = None

        # only do check if defined and has values
        if the_collection:
            if str_value in the_collection or int_value in the_collection:
                matched_value_from_collection = True
        else:
            logger.debug("Collection to check is empty - return False")

    return matched_value_from_collection
