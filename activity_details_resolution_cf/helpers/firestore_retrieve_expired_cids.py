import logging
from google.cloud import firestore

logger = logging.getLogger(__name__)


def get_do_not_process_cids():
    """
    Refactored out of large inbound_converter flow to isolate retrieval from
    firestore of these configurations until we move the configurations into
    microservice.

    :return: (set) - of client ids which are not to be processed (expired clients)
    """
    firestore_collection = "config_donotprocess_cids"
    # load donotprocess client ids list
    fs_client = firestore.Client()
    site = fs_client.collection(firestore_collection)

    # the list to use in constructing the return set
    do_not_process_cids = []

    # find the latest document with donotprocess CIDs - based on the created_at timestamp field
    try:
        query = site.order_by('created_at_timestamp_ms', direction=firestore.Query.DESCENDING) \
            .limit(1)
        results = query.stream()
        # stream() returns a list of results, even though in our case there will be only one result
        for document in results:
            doc_as_dict = document.to_dict()
            # TODO: getting document ID does not work in Unit tests - because we do not mock Firestore properly
            # doc_id = document.id
            created_at_datetime_utc_str = doc_as_dict.get('created_at_datetime_utc_str')
            # TODO add doc_id when unit tests are fixed
            #  logger.debug("got donotprocess_cids from Firestore: doc_id={}, doc={}".format(doc_id, doc_as_dict))
            logger.info("got donotprocess_cids from Firestore: created_at_datetime_utc_str={}, doc={}".format(
                created_at_datetime_utc_str, doc_as_dict))
            do_not_process_cids = doc_as_dict.get('cids')
    except Exception as e:
        error_str = "activity_details_resolution CF: Error getting expired CIDs from Firestore: error={}". \
            format(str(e))
        logger.error(error_str)
        raise e

    return set(do_not_process_cids)
