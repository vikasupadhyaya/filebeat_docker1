# Build Filebeat Docker image and register in GCR

cd <project_dir>

```
gcloud builds submit --tag "gcr.io/tt-temp-2021030444/ibc-filebeat-1203:7.15.0" .
```
verify output:
should be similar to:
```
...
7.15.0: digest: sha256:34e664428cdd98accfc83e5be54c4e29911ab07d15dd1286448b380f5002143b size: 3240
DONE
-----------------------------------------------------------------------------------------------------------------------------------------------------------
ID                                    CREATE_TIME                DURATION  SOURCE                                                                                            IMAGES                                              STATUS
bbf6f356-5d9a-4cee-bd01-70eec60613d9  2021-12-04T00:59:13+00:00  40S       gs://tt-temp-2021030444_cloudbuild/source/1638579552.231878-9a8b914ec797428ea42d2da0e12ac11f.tgz  gcr.io/tt-temp-2021030444/ibc-filebeat-1203:7.15.0  SUCCESS
...
```

# Create and run a VM from this image in GCP

determine which Service Account to use:
using SI (tt-temp-cicd-dataflow-worker@tt-temp-2021030444.iam.gserviceaccount.com)
with additional role:  roles/monitoring.metricWriter 
VPC:  marina-vpc
ElasticSearch credentials: provide your own:
CLOUD_AUTH=xxx

create VM:
```gcloud compute instances create-with-container filebeat-vm-1203 --project=tt-temp-2021030444 --zone=us-central1-a --machine-type=e2-micro --network-interface=network-tier=PREMIUM,subnet=marina-vpc --maintenance-policy=MIGRATE --service-account=tt-temp-cicd-dataflow-worker@tt-temp-2021030444.iam.gserviceaccount.com --scopes=https://www.googleapis.com/auth/cloud-platform,https://www.googleapis.com/auth/pubsub,https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --container-image=gcr.io/tt-temp-2021030444/ibc-filebeat-1203:7.15.0 --container-restart-policy=never --container-tty --container-command=filebeat --container-arg="-e" --container-env=PROJECT_ID=tt-temp-2021030444,PUBSUB_INPUT_TOPIC=logs-for-es-marina,SUBSCRIPTION_NAME=logs-for-es-marina-sub,CLOUD_ID=tt-ibc-eng-elastic:dXMtZWFzdDQuZ2NwLmVsYXN0aWMtY2xvdWQuY29tJDkzNWE2M2NiZDlmZDQ0OTU5NzMwMTkzNzRhMjY1NWI4JGUxYjliNGU0MDBjNDRmNDY4MmEyOTNhYzU1Y2U1MmI3,CLOUD_AUTH=mpopova:xxx --create-disk=auto-delete=yes,device-name=instance-no-creds6,image=projects/cos-cloud/global/images/cos-stable-89-16108-534-9,mode=rw,size=10 --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --labels=container-vm=cos-stable-89-16108-534-9
```

SSH to the VM and verify 'filebeat' process is running, and no errors in the logs

# Vaidate pipeline

Send a test event to the input PubSub topic (logs-for-es-marina in this example)
```
{
   "event_uuid":"m_id_1203_1",
   "logstash_id":"m_id_1203_1",
   "cid":"12345",
   "event_timestamp_millis":"1638565171000",
   "activity_date":"2021-12-03",
   "remote_ip":"165.155.130.139",
   "user_agent":"Mozilla/5.0 (X11; CrOS aarch64 13421.102.0) AppleWebKit/537.36 (KHTML, like Gecko)Chrome/86.0.4240.199 Safari/537.36",
   "referer":"https://www.my.site2.com/",
   "ref_param":"https://www.nyt.com",
   "request_status":"200",
   "request_method":"POST",
   "request_size":"52",
   "response_size":"124",
   "latency":"1.3"
}
```


verify events are showing in the ES Dashboard:
ENG ES and Kibana:
https://tt-pd-eng-elastic-1.kb.us-east4.gcp.elastic-cloud.com:9243/app/home#/

Dashboard name: "IBC Parsed Logs"
