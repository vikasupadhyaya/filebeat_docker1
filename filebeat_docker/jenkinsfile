#!groovy

//@Library('jenkins-shared') _
def loadValuesYaml(){
  def VERSION = readYaml (file: './Version.yaml')
  return VERSION;
}

def gcloudLogin() {
    def SA_NAME="jenkins-image@jenkins-image.iam.gserviceaccount.com"
    def KEYFILE="/home/ec2-user/gcloud/jenkins-image-6172f8f3b2ba.json"

    sh 'gcloud auth list'
    sh "gcloud auth activate-service-account ${SA_NAME} --key-file=${KEYFILE}"
    sh "gcloud config set account ${SA_NAME}"
}

def IMAGE_ID = " "

pipeline {
    agent any
    tools {
        jdk '11.0.13'
    }
    environment {
        GCP_PREFIX="us-east4-docker.pkg.dev"
        STAGE_PRENAME="Filebeat Docker"
        PROJECT_NAME="ibc-filebeat-docker"
        VERSION = "${loadValuesYaml().docker_image}"
        GCP_PROJECT="tt-pr-infraservices-prod"
        GCP_REGISTRY="tt-reg-infraservices-prod-docker"
        GCP_REGION="us-east4"
        GIT_SHORT="${env.GIT_COMMIT[0..7]}"
        JENKINS_BUILD_NUMBER="${env.BUILD_NUMBER}"
        DIR_NAME="filebeat_docker/"
    }
    stages {
        stage ('Build Docker Image') {
            steps {
                dir ("${WORKSPACE}") {
                    sh "pwd"
                    echo "Building ${STAGE_PRENAME} Docker Image"
                    sh "docker build --no-cache -t ${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:${VERSION}-${GIT_SHORT}-${JENKINS_BUILD_NUMBER} -f ${DIR_NAME}/Dockerfile ."
                }
            }
        }
        stage ('Use "latest" tag if a SNAPSHOT version') {
            when {
                expression { return VERSION =~ /SNAPSHOT/ }
            }
            steps {
                echo "Tagging SNAPSHOT build with 'latest'"
                sh "docker tag ${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:${VERSION}-${GIT_SHORT}-${JENKINS_BUILD_NUMBER} \
                ${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:latest"
            }
        }
        stage ('Upload Docker Image if Release or RC') {
            when {
                not {
                    expression { return VERSION =~ /SNAPSHOT/ }
                }
            }
            steps {
                echo "Building ${STAGE_PRENAME} Docker Image"
                lock("${env.NODE_NAME}") {
                    gcloudLogin()
                    sh "docker push ${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:${VERSION}-${GIT_SHORT}-${JENKINS_BUILD_NUMBER}"
                }
            }
        }
                stage ('Upload Docker Image if SNAPSHOT') {
            when {
                expression { return VERSION =~ /SNAPSHOT/ }
            }
            steps {
                echo "Building ${STAGE_PRENAME} Docker Image"
                lock("${env.NODE_NAME}") {
                    gcloudLogin()
                    sh "docker push ${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:${VERSION}-${GIT_SHORT}-${JENKINS_BUILD_NUMBER}"
                    sh "docker push ${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:latest"
                }
            }
        }
        stage ('Capture Image ID') {
            steps {
                sh "docker images --filter=reference=${GCP_PREFIX}/${GCP_PROJECT}/${GCP_REGISTRY}/${PROJECT_NAME}:${VERSION}-${GIT_SHORT}-${JENKINS_BUILD_NUMBER} --quiet > commandResult"
                script {
                    IMAGE_ID = readFile('commandResult').trim()
                }
                echo "Image ID = ${IMAGE_ID}"
            }
        }
    }
    post {
        success {
            echo "Purging local image"
            sh "docker rmi -f ${IMAGE_ID}"
        }
    }
}