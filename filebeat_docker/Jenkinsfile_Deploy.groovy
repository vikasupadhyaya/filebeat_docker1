#!groovy

// def loadValuesYaml(){
//   def VERSION = readYaml (file: './Version.yaml')
//   return VERSION;
//  }

pipeline {
	agent any

    tools {
        jdk 'JDK11'
    }

	environment {
        PRODUCT_NAME="inboundconverter"
    }
	parameters {
        choice(
            name: 'SERVICE',
            choices: ["filebeat"],
            description: 'Choose Product to deploy'
        )
		choice(
			name: 'ENV', 
			choices: ["eng","qa","prod"], 
			description: 'Pick environment to deploy to'
        )
        string(
            name: 'VERSION',
            description: 'Artifact version to deploy (ex: 0.1-6116c4b7-1)',
            defaultValue: '1.0-ea3b6cda-25'
        )
	}

	stages {
        stage ("Upload Innovation Environment Configs") {
            environment {
                GITLAB_TOKEN = credentials('GITLAB_DEPLOY_DOCKER')
            }
            steps {
                lock("${env.NODE_NAME}") {
                    sh "curl -X POST \
                    -F token=${GITLAB_TOKEN}\
                    -F ref=main \
                    -F \"variables[ENV]=${params.ENV}\" \
                    -F \"variables[PRODUCT]=inboundconverter\" \
                    -F \"variables[SERVICE]=${params.SERVICE}\" \
                    -F \"variables[TAG]=${params.VERSION}\" \
                    https://gitlab.com/api/v4/projects/31547153/trigger/pipeline"
                }
            }
            post {
                failure {
                    echo "Trigger step failed, please check logs"
                }
            }
        }
	}
}