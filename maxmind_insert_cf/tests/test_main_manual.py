"""
Manual tests used during development
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from main import add_to_firestore

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)

@pytest.mark.skip(reason="manual test")
def test_add_records():
    """
    Add the records to specified firestore collection - maxmind-<revision>

    Requires the 'shards' records to be created:
    "ibc-cf-configurations/maxmind-config/data-loads/" + str(data_revision) + "/shards/" + str(shard)

    Can run from the maxmind_load_cf/tests/helpers/test_shards_helper_manual:test_initialize_shards()

    :return: None
    """
    config.innovation_configs()

    insert_event = {
        "attributes": {
                "REVISION": "Nov-2019-manual-test",
                "SHARD": 0,
                "SHARD_BATCH_ID": 1111,
                "RECORDS": [
                    {
                        "geoname_id": "manual-test-1",
                        "start_ip_int": 628669783,
                        "end_ip_int": 628669783
                    },
                    {
                        "geoname_id": "manual-test-2",
                        "start_ip_int": 728669783,
                        "end_ip_int": 728669783
                    },
                    {
                        "geoname_id": "manual-test-3",
                        "start_ip_int": 828669783,
                        "end_ip_int": 828669783
                    }
                ]
          }
        }

    add_to_firestore(insert_event, None)
    logger.info("done")
