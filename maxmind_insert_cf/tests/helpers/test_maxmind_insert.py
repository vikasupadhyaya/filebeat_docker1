"""
Unit tests for maxmind_insert
"""
import pytest
import logging
import helpers.maxmind_insert as maxmind_insert
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "data_revision, expected", [
        ("Q2-2020-T1", "maxmind-Q2-2020-T1"),
        ("Q1-2121", "maxmind-Q1-2121")
    ]
)
def test_get_collection_name_from_revision(data_revision, expected):
    result = maxmind_insert.get_collection_name_from_revision(data_revision)
    assert result == expected


@pytest.mark.parametrize(
    "data_revision, shard, expected", [
        ("Q2-2020-T1", 1, "ibc-cf-configurations/maxmind-config/data-loads/Q2-2020-T1/shards/1"),
        ("Q1-2121", 2, "ibc-cf-configurations/maxmind-config/data-loads/Q1-2121/shards/2")
    ]
)
def test_get_shard_document_id(data_revision, shard, expected):
    result = maxmind_insert.get_shard_document_id(data_revision, shard)
    assert result == expected


@pytest.mark.parametrize(
    "geoname_id, start_ip, end_ip, expected", [
        # no forward slashes in doc keys
        ("1234567890/98", 15934222, 15934300, "123456789098_15934222_15934300"),
        # various other tests of concat sequence
        ("some-other-id", "4567", "89555", "some-other-id_4567_89555")
    ]
)
def test_get_document_id(geoname_id, start_ip, end_ip, expected):
    record = {
        "geoname_id": geoname_id,
        "start_ip_int": int(start_ip),
        "end_ip_int": int(end_ip)
    }
    result = maxmind_insert.get_document_id(record)
    assert result == expected
