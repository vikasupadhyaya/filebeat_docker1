"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
import helpers.maxmind_insert as maxmind_insert
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test requiring special setup")
def test_records_exist():
    """
    Find a record that exists in innovation maxmind collection and confirm
    1) return value is expected for indicating exists
    2) ensure both a correct true and false result is confirmed

    :return: None
    """
    config.innovation_configs()
    maxmind_insert.ensure_initialized()

    data_revision = "Nov-2019-manual-test"
    records = []
    # only partial record for testing including the properties we need for creating document id
    record = {
        "geoname_id": "manual-test-insert-1",
        "start_ip_int": 628669783,
        "end_ip_int": 628669783
    }
    records.append(record)
    already_exists = maxmind_insert.records_exist(data_revision, records)
    logger.info("exists: {} - record: {}".format(already_exists, record))


@pytest.mark.skip(reason="manual test requiring special setup")
def test_insert_records_to_collection():
    config.innovation_configs()
    # maxmind_insert.ensure_initialized()
    data_revision = "Nov-2019-manual-test"
    records = []
    # only partial record for testing including the properties we need for creating document id
    record = {
        "geoname_id": "manual-test-insert-1",
        "start_ip_int": 628669783,
        "end_ip_int": 628669783
    }
    record2 = {
        "geoname_id": "manual-test-insert-2",
        "start_ip_int": 628669785,
        "end_ip_int": 628669785
    }
    records.append(record)
    records.append(record2)
    shard = 0
    shard_batch_id = 12345
    maxmind_insert.insert_records_to_collection(data_revision, shard, shard_batch_id, records)
