"""
Unit tests for config
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from config import get_config_value

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


def test_config_timeout():
    """
    Verify age of event is expected value

    :return: None
    """
    config.innovation_configs()
    timeout = get_config_value("MAX_SECONDS_EVENT_AGE")
    assert timeout == 21600


def test_collection_components():
    """
    The path components in firestore collections need to match what
    is expected and defined in other cloud functions.  Verify what
    we expect to be values and any change to these would need to be
    checked in the corresponding load scripts and completed process
    because all refer back to the same configuration document.

    :return: None
    """
    config.innovation_configs()
    assert get_config_value("CONFIG_COLLECTION_NAME") == "ibc-cf-configurations"
    assert get_config_value("DATA_LOADS_COLLECTION_NAME") == "data-loads"
    assert get_config_value("SHARDS_COLLECTION_NAME") == "shards"
    assert get_config_value("MAXMIND_DOC_ID") == "maxmind-config"
    assert get_config_value("COLLECTION_PREFIX") == "maxmind-"
