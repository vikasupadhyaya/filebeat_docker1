# Test Message Structure
To support direct testing, an unencoded message structure can be found in example-message.json

The size of array of RECORDS must not be larger than 499 since all elements of array will be submitted to firestore as a single batch write.  500 is the limit, but we require an additional document to track the batches inserted into a 'shard' document to allow for more efficient inserts since a single document can not be updated more often than once a second.

The REVISION property is used to construct the target firestore collection that data will be added to - maxmind-<revision>

