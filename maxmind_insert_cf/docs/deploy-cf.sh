#
# command to deploy in innovation project for testing
#
gcloud functions deploy maxmind_insert_cf \
--project tt-temp-2021030444 \
--region=us-east4 \
--runtime python37 \
--service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com \
--source=. \
--trigger-topic ibc.maxmind-insert \
--entry-point add_to_firestore \
--stage-bucket ibc-client-activity-input-innovation \
--memory=128MB \
--timeout=120 \
--max-instances=30 \
--retry \
--set-env-vars ENV=tt-temp-2021030444,PROJECT=tt-temp-2021030444,CF_ENV=True \
--update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi

