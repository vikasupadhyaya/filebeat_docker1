import json
import base64
import logging
from datetime import datetime, timezone
from tt_common.tt_logging import Logging
from dateutil import parser
from helpers.maxmind_insert import insert_records_to_collection
from config import get_config_value

# Initialize logging
Logging().init_logging()
logger = logging.getLogger(__name__)

# Informational detail to determine re-use of instance
execution_count = 0


def ignore_aged_event(event, context):
    """
    Determine if the age of the event is older than the configured value to prevent
    continuous processing since we retry failures.

    :param (dict) event: containing details of event
    :param (google.cloud.functions.Context) context: of invocation containing event time
    :return: (boolean) - True if event is to be discarded; False to process
    """
    ignore_event = False
    if not context:
        logger.info("No context provided - no age check performed during direct tests.")
        return ignore_event

    try:
        timestamp = context.timestamp
        event_time = parser.parse(timestamp)
        event_age = (datetime.now(timezone.utc) - event_time).total_seconds()
        if event_age > int(get_config_value("MAX_SECONDS_EVENT_AGE")):
            logging.info(
                "Event is being dropped due to age. (age of event: {} seconds) - Event: {}".format(
                    event_age, event))
            ignore_event = True
    except Exception as e:
        logging.error("Problem in age of event check - aborting and not retrying. {}".format(str(e)))
        ignore_event = True

    return ignore_event


def add_to_firestore(received_event_param, context):
    """
    Entry point of cloud function.  See example message for expected structure.

    :param (dict) received_event_param: message containing records to insert to a firestore collection
    :param (google.cloud.functions.Context) context: of invocation containing event time
    :return: None
    """
    global execution_count
    execution_count += 1
    logger.info("Function instance execution count: {}".format(execution_count))

    try:
        received_event = dict() if not received_event_param else received_event_param
        if "data" in received_event:
            event = json.loads(base64.b64decode(received_event['data']).decode('utf-8'))
        elif "attributes" in received_event:
            event = json.loads(json.dumps(received_event['attributes']))
        else:
            event = dict()

        if ignore_aged_event(event, context):
            return

        """
        Use 'get' style access in case we do not receive an expected value from message.
        If required values are not present, then log a message and exit gracefully.
        """
        logger.debug("Processing event: {}".format(event))
        data_revision = str(event.get("REVISION", "")).strip()
        records = event.get("RECORDS")
        shard = event.get("SHARD")
        shard_batch_id = event.get("SHARD_BATCH_ID")

        # if not data_revision or not shard or not shard_batch_id:
        # if (data_revision is None) or (not data_revision) or (shard is None) or (shard_batch_id is None):
        if (not data_revision) or (shard is None) or (shard_batch_id is None):
            logger.info("Missing required properties: data_revision: '{}'; shard: '{}'; shard_batch_id: '{}'".format(
                data_revision, shard, shard_batch_id))
            logger.info("Records are not loaded from event: {}".format(event))
            return

        insert_records_to_collection(data_revision, shard, shard_batch_id, records)

    except Exception as e:
        logger.error("Error inserting maxmind data - {}".format(str(e)))
        # If error is due to missing document (a shard), then an error in logic or flow
        # is present and we should not continue retries
        if type(e).__name__ == "NotFound":
            logger.error("Not retrying event due to error in data structure.")
            return

        # retry on failures by CF deployment configuration
        # age check performed on entry to prevent continuous retries
        raise e
