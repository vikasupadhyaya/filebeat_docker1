"""
Centralized location for configuration options specific to the maxmind processing.

Startup Parameters
PROJECT - stored as GCP_PROJECT in cf_config dictionary
STORAGE_BUCKET - stored as STORAGE_BUCKET in cf_config dictionary
CF_ENV - Necessary when deployed as cloud function with value True
       - to control whether cloud storage or local file access is used


Common configuration options are expected to be present in config based dictionary definitions
for passing to common routines that take in a config object for operation.
GCP_PROJECT - the GCP Project name
STORAGE_BUCKET - the GCP Storage bucket name used by cloud function
LOCAL_DEV - boolean - false will retrieve data files from cloud storage

Additional Configuration properties:
PUBLISH_TO_TOPIC_BATCH - the topic this cloud function is triggered by
PUBLISH_TO_TOPIC_INSERT - the topic which cloud function to add maxmind firestore entries is triggered by
LOCAL_PATH - path to data files when in local mode (not retrieved from cloud storage)
TEMP_STORAGE - path to storage location when files retrieved from gcp
CLOUD_STORAGE_PATH - location in bucket to look for maxmind files
"""
import os

cf_config = {}


def innovation_configs():
    """
    Example of overriding and setting explicit configuration values
    such as when in development mode and accessing the innovation
    GCP environment and a local microservice

    :return: no return value
    """
    global cf_config

    cf_config["GCP_PROJECT"] = "tt-temp-2021030444"
    # bucket used in innovation space for this test purpose not
    # the bucket which will trigger cloud function invocations
    # ibc-configurations-<env> is bucket that has triggers defined
    # against bucket for invoking automatic processing
    cf_config["STORAGE_BUCKET"] = "ibc-client-activity-input-innovation"
    cf_config["LOCAL_DEV"] = True


def innovation_configs_helpers_tests():
    """
    Override the 'local' path to maxmind data to account for direct helpers
    tests that require additional parent location to access files.

    :return: None
    """
    innovation_configs()
    global cf_config

    cf_config["LOCAL_PATH"] = "../../maxmind"


def init_configs():
    """
    The default implementation of configuration process to initialize
    the configurations
    """
    global cf_config
    cf_config = {
        "GCP_PROJECT": os.getenv("PROJECT"),
        "STORAGE_BUCKET": os.getenv("STORAGE_BUCKET"),
        "LOCAL_DEV": True,
        "CONFIG_COLLECTION_NAME": "ibc-cf-configurations",
        "DATA_LOADS_COLLECTION_NAME": "data-loads",
        "SHARDS_COLLECTION_NAME": "shards",
        "MAXMIND_DOC_ID": "maxmind-config",
        "COLLECTION_NAME_PREFIX": "maxmind-",
        "DEFAULT_ORIGINAL_COLLECTION": "maxmind",
        "MAX_RECORDS_PER_INSERT": 499,
        # 500 * MAX_RECORDS_PER_INSERT (or multiple of config for best performance)
        "PROCESS_RECORDS_PER_BATCH": 249500,
        "PUBLISH_TO_TOPIC_BATCH": "ibc.maxmind-load",
        "PUBLISH_TO_TOPIC_INSERT": "ibc.maxmind-insert",

        # When local dev, we use this as location to check
        # for maxmind data files
        "LOCAL_PATH": "../maxmind",

        # Location used to store retrieved maxmind files
        # when pulling from cloud storage
        "TEMP_STORAGE": "/tmp/cf-maxmind",

        # The path in storage bucket to check for maxmind data files
        # to load
        "CLOUD_STORAGE_PATH": "maxmind",

        # Number of seconds an event must be handled within before discarding
        # Since this cloud function uses retries, we want to impose a limit
        # to fall back to rather than retries for 7 days
        # 4 hours is initial setting - 60s * 60m * 4h = seconds
        "MAX_SECONDS_EVENT_AGE": 60 * 60 * 4
    }

    in_cloud_env = os.getenv("CF_ENV", "False").lower() == "true"
    cf_config["LOCAL_DEV"] = not in_cloud_env


def get_config_value(key):
    """
    Retrieve configuration value with specified key.
    Use a method instead of referring to variable directly in other
    modules since changes to entries are not reflected after initial inclusion
    in other files.  eg. init, change, lookup uses value from init not change

    :param key: to obtain value of
    :return: type varies by key
    """
    return cf_config.get(key)


init_configs()
