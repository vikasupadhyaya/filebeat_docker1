# Local Data Files

The files located in this folder are for local development.
The following files are expected to be present:  
  `GeoIP2-City-Blocks-IPv4-<revision>.csv`
  `GeoIP2-City-Locations-en-<revision>.csv`

The main data file (`GeoIP2-City-Blocks-IPv4` prefix) is a small portion of entries for 
commit reference.
Pull full files as needed from cloud storage:
* tt-pd-eng/ibc-configurations-eng/maxmind
