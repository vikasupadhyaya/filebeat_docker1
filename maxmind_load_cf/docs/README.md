# Overview
Maxmind data loading is not currently automated to point of placing files into monitored
cloud storage location.  This CF along with others are automation steps for loading and
promoting to become point of reference in other cloud functions once the reference
data files are placed in cloud storage bucket that is monitored.


## Cloud Storage Bucket monitored
See storage_configs_cf for implementation.

* ibc-configurations-{environment} - /maxmind folder  
  GeoIP2-City-Blocks-IPv4-{revision}.csv - IP and geoname_id with some meta information
  GeoIP2-City-Locations-en-{revision}.csv - Location details for a geoname_id
  
  
Where {revision} is some identifier added to obtained files to easily associate the two as working together
to provide overall data content to be loaded.  The revision is used in tracking and
naming of the firestore collection created (maxmind-{revision})

## Process Data File - Create Batch insert records
Given size of the main data file, we perform two stages of batching to distribute the processing to create the insert records.
1. Cloud Storage trigger will provide the {revision} that is to be loaded
2. The data file will be read to separate out into a reasonable processing batch size such as 'process lines starting at line 1 thru {batch size}'
3. The same pub/sub topic is used for initial revision start and subsequent batch handling from a starting line
4. The batch request with a starting line will create insert messages of final firestore records to insert and publish to ibc.maxmind-insert (see maxmind_insert_cf)


# Test message structure
To support direct testing, an unencoded message structure is one of the following:

* Initial starting point to process a new quarterly file: (if REVISION is empty, then a single data file is looked for in cloud storage)
* An override property "ALLOW_RELOAD" may be specified with value "YES" to skip the prevention of reprocessing a file already marked as loaded in event we want to recreate insert messages.
```
{
    "attributes": {
        "REVISION": "Nov-2019-local-v1"
    }
}
```

* To process a batch, which is a range of lines from a quarterly data file:
```
{
    "attributes": {
        "REVISION": "Nov-2019-local-v1",
        "STARTING_RECORD_NUMBER": 1
    }
}
```

Batch processing only specifies a starting record number and will process the configured number of lines that represent a batch.
The creation of batch messages is performed from initial message to start the load process.