# Loading Maxmind Data

The Maxmind data set is comprised of multiple data files and formats, which we use
two of to create our maxmind firestore collection:
  * `GeoIP2-City-Blocks-IPv4-{revision}.csv` - IP and geoname_id with some meta information
  * `GeoIP2-City-Locations-en-{revision}.csv` - Location details for a geoname_id
  
Note: The files received from maxmind do not include a revision, so the source files that become revision
files are named:
  * `GeoIP2-City-Blocks-IPv4.csv`
  * `GeoIP2-City-Locations-en.csv`


This cloud function serves two purposes:
* Create batch messages to process the large 'Blocks' file in to chunks
* Process to handle batch messages to create insert messages for firestore insertion that includes up to 499 records

The initial starting step will be triggered by a cloud storage addition of the Maxmind data
which will begin the process.  When loading is completed, the configuration of which maxmind collection
to use in other IBC flow processes will be updated to new collection (revision from filename).
