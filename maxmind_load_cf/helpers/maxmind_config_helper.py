"""
Provide routines for managing the maxmind configurations.

The overview of collection and document structure is:
ibc-cf-configurations/maxmind-config/data-loads/<data_revision>/shards/<0-99>

ibc-cf-configurations - the root collection of inbound converter configurations
maxmind-config - the document containing:
                  active_collection - the collection which cloud functions should be using
                  staging_collection - a collection being populated that will eventually become the active_collection
data-loads - collection of quarterly files so we can track when and what was loaded
<data_revision> - document for each quarterly file loaded containing:
                  staging_status - progress of state of data load
                  record_count - number of configurations in main data file, and expected to match load count in shards
                  last_updated - timestamp of last change
                  created_timestamp - timestamp of when record was initially created
shards - collection to contain documents to allow for more efficient tracking of written records
         based on firestore distributed counters strategy, with additional detail for determining load failures
<0-99> - one hundred documents, each containing a portion of the beginning record ip addresses and tracking details:
         handled_batch_ids - array - starting ip address of first record in batch that has been written
         expected_batch_ids - array - starting ip address of first record in batch from all records in data file that
                                      would is expected to be associated and tracked in the specific shard instance
        records_written - int - total number of written documents (if same data set is manually fed into system for
                                loading, the documents will be overwritten and this count indicating a higher value
                                than the actual number of documents present in collection when summed
        last_updated - timestamp of last change to the shard instance
"""
import logging
from helpers.shards_helper import initialize_shards
from google.cloud import firestore
from config import get_config_value

# Initialize logging
logger = logging.getLogger(__name__)


@firestore.transactional
def update_maxmind_staging_in_tx(transaction, data_revision, config_ref, revision_load_ref, data_file_record_count):
    """
    Determine if the requested data_revision has already been staged for future use, preventing
    a subsequent run that would perform all inserts again and lead to increased costs.

    If the data_revision has not been created, structure will be initialized within a transaction to
    prevent future loads of the same revision.

    :param transaction: firestore transaction
    :param (str) data_revision: checking for ability to begin load
    :param config_ref: firestore document reference to maxmind configuration
    :param revision_load_ref: firestore document reference corresponding to data_revision
    :param (int) data_file_record_count: number of configuration entries expected to be written
    :return: (boolean) : True to create insert records; False to skip re-loading
    """
    allow_load = False

    snapshot_maxmind_config_doc = config_ref.get(transaction=transaction)
    snapshot_revision_doc = revision_load_ref.get(transaction=transaction)

    if snapshot_revision_doc.exists:
        logger.info(("Maxmind revision {} has already been loaded, or is in progress.  " +
                     "Not creating insert events again.").format(data_revision))
    else:
        logger.info("Maxmind revision {} not previously loaded - adding now.".format(data_revision))
        new_staging_version = str(get_config_value("COLLECTION_NAME_PREFIX")) + str(data_revision)

        staging_record = {
            "record_count": int(data_file_record_count),
            "staging_status": "loading",
            "created_timestamp": firestore.SERVER_TIMESTAMP,
            "last_updated": firestore.SERVER_TIMESTAMP
        }
        if snapshot_maxmind_config_doc.exists:
            # snapshot not present, so add it and update config
            current_staging_version = snapshot_maxmind_config_doc.get("staging_collection")
            logger.info("Updating maxmind configuration stage_collection from {} to {}".format(
                current_staging_version, new_staging_version))
            transaction.update(reference=config_ref, field_updates={
                "staging_collection": new_staging_version,
                "staging_collection_last_updated": firestore.SERVER_TIMESTAMP,
                "last_updated": firestore.SERVER_TIMESTAMP
            })
        else:
            # one time execution to initialize structure
            default_original_collection = str(get_config_value("DEFAULT_ORIGINAL_COLLECTION"))
            initial_maxmind_config = {
                "active_collection": default_original_collection,
                "active_collection_last_updated": firestore.SERVER_TIMESTAMP,
                "staging_collection": new_staging_version,
                "staging_collection_last_updated": firestore.SERVER_TIMESTAMP,
                "last_updated": firestore.SERVER_TIMESTAMP,
                "created_timestamp": firestore.SERVER_TIMESTAMP,
            }
            logger.info("Maxmind configuration record not present - adding initial entry: {}".format(initial_maxmind_config))
            transaction.set(reference=config_ref, document_data=initial_maxmind_config, merge=False)

        # in else block we always want to create the record and this is our condition for
        # allowing load to begin
        transaction.set(reference=revision_load_ref, document_data=staging_record, merge=False)
        allow_load = True

    # Note: testing of tx flow, added a sleep here and executed in two separate invocations
    #       to verify the replay of tx by logging entries. eg log/sleep/log, and log at entry point
    #       of tx method, then verifying instance started first allowed for load, and second did not
    #       allow load
    return allow_load


def allow_load_and_initialize(data_revision, data_file_record_count):
    """
    Wrap process of updating the maxmind staging collection and history of staging events
    within a transaction to ensure that only a single load start is performed for any one revision.

    :param (str) data_revision: requested to load
    :param (int) data_file_record_count: number of configuration entries expected to be written
    :return: (boolean): True to allow for load events to be constructed and published; False
                        to prevent since we have already loaded the specified revision
    """
    # Firestore Collection containing cloud function configurations for inbound converter
    project = get_config_value("GCP_PROJECT")
    config_collection_name = get_config_value("CONFIG_COLLECTION_NAME")
    data_loads_collection_name = get_config_value("DATA_LOADS_COLLECTION_NAME")

    # Firestore document identifier for maxmind configurations
    maxmind_doc_id = get_config_value("MAXMIND_DOC_ID")

    fs_client = firestore.Client(project)
    transaction = fs_client.transaction()

    config_collection = fs_client.collection(config_collection_name)
    maxmind_config_ref = config_collection.document(maxmind_doc_id)
    revision_load_ref = maxmind_config_ref.collection(data_loads_collection_name).document(str(data_revision))

    allow_load = update_maxmind_staging_in_tx(transaction, data_revision, maxmind_config_ref, revision_load_ref,
                                               data_file_record_count)
    if allow_load:
        initialize_shards(revision_load_ref)
    return allow_load
