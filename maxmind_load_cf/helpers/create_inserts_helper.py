"""
Routines that are used to read from data file and create the insert messages
containing a batch size of records that will be passed on to the ibc.maxmind-insert
topic for performing the inserts.

Prior to returning the messages, the collected batch ids and expected counts are merged
into shards of the data_revision being processed so that we have a reference point of what
is expected to be loaded, and can subsequently be used in determining failures if excessive time
has passed and not complete, or that all inserts are complete and counts match that we can promote
the staged collection to be the new active collection.
"""
import csv
import logging
import helpers.data_loader as data_loader
import helpers.shards_helper as shards_helper
from config import get_config_value
from netaddr import IPNetwork

# Initialize logging
logger = logging.getLogger(__name__)


def create_firestore_insert_message(data_revision, shard, shard_batch_id, records):
    """
    Create a single insert message with information from parameters.
    Individual messages are used to group a single batch of records together for
    tracking of inserts to accurately determine the records loaded.

    :param (str) data_revision: that created messages will specify as source file to process
    :param (str) shard: identifier of the shard instance we are to track batch write with when message is processed
    :param (int) shard_batch_id: the starting_ip_int value of first message which we use as batch id
    :param (list) records: to include in batch insert by handling of created message
    :return: (dict) - message to add to pubsub
    """
    message = {
        "REVISION": str(data_revision),
        "SHARD": str(shard),
        "SHARD_BATCH_ID": int(shard_batch_id),
        "RECORDS": list(records)
    }

    return message


def create_firestore_insert_record(headers, line):
    """
    Create the individual firestore record for a maxmind data "line" from large data file.
    The constructed object represents what we need in firestore for IBC workflow usage.

    :param (list) headers: first line of GeoIP2-City-Blocks-IPv4- file to specify column keys
    :param (list) line: from GeoIP2-City-Blocks-IPv4- file
    :return: (dict) - record to add to firestore maxmind-<revision> collection
    """
    temp_record = {key: value for key, value in zip(headers, line)}
    geoname_id = temp_record["geoname_id"]
    network = IPNetwork(temp_record["network"])
    start_ip_int = int(network[0])
    end_ip_int = int(network[-1])
    location_detail = data_loader.location_details.get(geoname_id, {})

    record = {
        # from Blocks file
        "geoname_id": geoname_id,
        "start_ip_int": start_ip_int,
        "end_ip_int": end_ip_int,
        "registered_country_geoname_id": str(temp_record.get("registered_country_geoname_id", "")),
        "represented_country_geoname_id": str(temp_record.get("represented_country_geoname_id", "")),
        "is_anonymous_proxy": str(temp_record.get("is_anonymous_proxy", "")),
        "is_satellite_provider": str(temp_record.get("is_satellite_provider", "")),
        "postal_code": str(temp_record.get("postal_code", "")),

        # from Locations object
        "locale_code": str(location_detail.get("locale_code", "")),
        "continent_code": str(location_detail.get("continent_code", "")),
        "continent_name": str(location_detail.get("continent_name", "")),
        "country_iso_code": str(location_detail.get("country_iso_code", "")),
        "country_name": str(location_detail.get("country_name", "")),
        "subdivision_1_iso_code": str(location_detail.get("subdivision_1_iso_code", "")),
        "subdivision_1_name": str(location_detail.get("subdivision_1_name", "")),
        "subdivision_2_iso_code": str(location_detail.get("subdivision_2_iso_code", "")),
        "subdivision_2_name": str(location_detail.get("subdivision_2_name", "")),
        "city_name": str(location_detail.get("city_name", "")),
        "metro_code": str(location_detail.get("metro_code", "")),
        "time_zone": str(location_detail.get("time_zone", "")),
        "is_in_european_union": str(location_detail.get("is_in_european_union", ""))
    }
    return record


def get_firestore_insert_messages(data_file, data_revision, starting_record_number):
    """
    Process a section of the data_file, starting at record starting_record_number and process up
    to the configured batch size of rows.  Allows for different cloud function instances
    to process the same data file concurrently for a more efficient loading of data.

    :param (str) data_file: path and name of GeoIP2-City-Blocks-IPv4- file containing source data
    :param (str) data_revision: that created messages will specify as source file to process
    :param (int) starting_record_number: of data file to begin processing records
    :return: (list) - of messages used to insert firestore records
    """
    process_records_per_batch = get_config_value("PROCESS_RECORDS_PER_BATCH")
    max_records_per_insert = get_config_value("MAX_RECORDS_PER_INSERT")

    # records are individual firestore entries, which are grouped into messages
    records = []

    # messages are individual batch inserts of a list of records
    messages = []

    file_record_count = 0
    current_message_record_count = 0
    processed_record_count = 0
    shards_expected_batch_ids = shards_helper.get_shards_for_tracking()
    shards_expected_record_counts = shards_helper.get_shards_for_tracking()
    shard = 0
    shard_batch_id = 0

    with open(data_file, encoding="utf-8", errors='ignore') as infile:
        reader = csv.reader(infile)
        headers = next(reader)
        for line in reader:
            file_record_count += 1
            if file_record_count >= starting_record_number:
                record = create_firestore_insert_record(headers, line)
                current_message_record_count += 1
                if current_message_record_count == 1:
                    shard_batch_id = record.get("start_ip_int")
                    shard = shards_helper.get_shard_number_for_record(file_record_count, max_records_per_insert)
                    shards_expected_batch_ids[int(shard)].append(int(shard_batch_id))

                records.append(record)
                processed_record_count += 1

                if processed_record_count == process_records_per_batch:
                    break

                if current_message_record_count == max_records_per_insert:
                    messages.append(create_firestore_insert_message(data_revision, shard, shard_batch_id, records))
                    shards_expected_record_counts[int(shard)].append(len(records))
                    current_message_record_count = 0
                    records = []

    if current_message_record_count > 0:
        messages.append(create_firestore_insert_message(data_revision, shard, shard_batch_id, records))
        shards_expected_record_counts[int(shard)].append(len(records))

    shards_helper.merge_expected_batch_ids_to_shards(data_revision, shards_expected_batch_ids,
                                                     shards_expected_record_counts)
    return messages
