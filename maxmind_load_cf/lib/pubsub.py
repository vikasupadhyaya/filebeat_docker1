"""
Routines for managing interaction with google pub/sub.
"""
import json
import uuid
import logging

from google.cloud import pubsub_v1
from .manage_futures import add_future

logger = logging.getLogger(__name__)


def publish_events_to_topic(events, topic_project, topic_name):
    """
    Batch processes the events, creating a json object for each
    record and publishes to specified project and topic.

    :param events: to publish
    :param topic_project: gcp project containing target topic
    :param topic_name: to receive event
    :return: no return value
    """
    # Configure publisher for more efficient publishing of messages
    publisher = pubsub_v1.PublisherClient(
        publisher_options=pubsub_v1.types.PublisherOptions(
            flow_control=pubsub_v1.types.PublishFlowControl(
                message_limit=100,
                byte_limit=10 * 1024 * 1024,
                limit_exceeded_behavior=pubsub_v1.types.LimitExceededBehavior.BLOCK,
            ),
        ),
    )

    topic_path = publisher.topic_path(topic_project, topic_name)

    cnt = 0
    for current_event in events:
        cnt += 1
        data = json.dumps(current_event).encode("utf-8")
        future_id = str(uuid.uuid4())
        future = publisher.publish(topic_path, data)
        add_future(future_id, future, "publishing event")
        if cnt % 10000 == 0:
            logging.debug("published count at: {}".format(cnt))
