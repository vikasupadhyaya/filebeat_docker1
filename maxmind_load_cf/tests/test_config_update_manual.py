"""
Manual tests used during development

Note: field names in documents are using underscores since dashes are problematic in update calls.
      Documentation is not clear on how or why this is a limitation, other than stating that 'simple' field
      names are alphanumeric with underscores and can be updated in a different format than other field names.
"""
import pytest
import time
import config
import logging
from tt_common.tt_logging import Logging
from google.cloud import firestore
from config import get_config_value

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)

# Firestore Collection containing configurations
config_collection_name = "ibc-cf-configurations-tests"
# Firestore document identifier for maxmind configurations
maxmind_doc_id = "maxmind-config-tests"


@firestore.transactional
def update_maxmind_staging_in_tx(transaction, data_revision, config_ref, revision_load_ref):
    """
    Determine if the requested data_revision has already been staged for future use, preventing
    a subsequent run that would perform all inserts again and lead to increased costs.

    :param transaction: firestore transaction
    :param (str) data_revision: checking for ability to begin load
    :param config_ref: firestore document reference to maxmind configuration
    :param revision_load_ref: firestore document reference corresponding to data_revision
    :return: (boolean) : True to create insert records; False to skip re-loading
    """
    logger.info("...1) begin TX - confirm tx test")
    allow_load = False

    snapshot_maxmind_config_doc = config_ref.get(transaction=transaction)
    snapshot_revision_doc = revision_load_ref.get(transaction=transaction)

    if snapshot_revision_doc.exists:
        logger.info("Maxmind revision {} has already been loaded, or is in progress.  Not creating insert events again.".format(data_revision))
    else:
        logger.info("Maxmind revision {} not previously loaded - adding now.".format(data_revision))
        staging_record = {
            "staging_status": "loading",
            "created_timestamp": firestore.SERVER_TIMESTAMP
        }
        if snapshot_maxmind_config_doc.exists:
            # snapshot not present, so add it and update config
            staged_version = snapshot_maxmind_config_doc.get("staging_collection")
            logger.info("current staged version: {}".format(staged_version))
            logger.info("config doc EXISTS - need to update staging value")
            transaction.update(reference=config_ref, field_updates={
                "staging_collection": str(data_revision),
                "last_updated": firestore.SERVER_TIMESTAMP
            })
        else:
            # one time execution to initialize structure
            initial_maxmind_config = {
                "active_collection": "maxmind",
                "staging_collection": data_revision,
                "last_updated": firestore.SERVER_TIMESTAMP
            }
            logger.info("Maxmind configuration record not present - adding initial entry: {}".format(initial_maxmind_config))
            transaction.set(reference=config_ref, document_data=initial_maxmind_config, merge=False)

        # in else block we always want to create the record and this is our condition for
        # allowing load to begin
        transaction.set(reference=revision_load_ref, document_data=staging_record, merge=False)
        allow_load = True

    # Note: testing of tx flow, added a sleep here and executed in two separate invocations
    #       to verify the replay of tx by logging entries. eg log/sleep/log, and log at entry point
    #       of tx method, then verifying instance started first allowed for load, and second did not
    #       allow load
    logger.info("...2) before sleep in TX - start other instance with same revision")
    time.sleep(60)
    logger.info("...3) after sleep in TX - record should now be created or TX restarted and stopped")
    return allow_load


def update_maxmind_staging_details(data_revision):
    """
    Wrap process of updating the maxmind staging collection and history of staging events
    within a transaction to ensure that only a single load start is performed for any one revision.

    :param (str) data_revision: requested to load
    :return: (boolean): True to allow for load events to be constructed and published; False
                        to prevent since we have already loaded the specified revision
    """
    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    transaction = fs_client.transaction()

    config_collection = fs_client.collection(config_collection_name)
    maxmind_config_ref = config_collection.document(maxmind_doc_id)
    revision_load_ref = maxmind_config_ref.collection("data-loads").document(data_revision)

    allow_load = update_maxmind_staging_in_tx(transaction, data_revision, maxmind_config_ref, revision_load_ref)

    if allow_load:
        logger.info("Start LOAD")
    else:
        logger.info("Already loaded or in progress")

    return allow_load


# @pytest.mark.skip(reason="manual test to run together with another instance for tx confirmation")
def test_load():
    config.innovation_configs()
    data_revision = "Q2-2021-test3"
    update_maxmind_staging_details(data_revision)
