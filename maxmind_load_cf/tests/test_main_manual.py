"""
Manual tests used during development
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from main import load_maxmind

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_none():
    """
    If no specified revision, look to file system for which file is present
    and then create batch entries.

    The associated firestore collection for the revision present must not exist:
    ibc-cf-configurations/maxmind-config/data-loads/<revision>

    :return: None
    """
    config.innovation_configs()

    load_maxmind(None, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_batches():
    """
    Create batch records using specified file.

    The associated firestore collection for the revision must not exist:
    ibc-cf-configurations/maxmind-config/data-loads/<revision>

    :return: None
    """
    config.innovation_configs()

    batch_event = {
        "attributes": {
            "REVISION": "Nov-2019-10k"
        }
    }

    load_maxmind(batch_event, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_batches_missing_revision():
    """
    Requested revision is not present, then fail.

    :return: None
    """
    config.innovation_configs()

    batch_event = {
        "attributes": {
            "REVISION": "Nov-2019-local-missing"
        }
    }

    load_maxmind(batch_event, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_insert_messages():
    """
    Manual test to create insert records for a portion of the data file, starting
    at the specified record number from the revision data file, creating insert
    messages published to the ibc.maxmind-insert topic and entries added to
    the load details at:
    firestore:ibc-cf-configurations/maxmind-config/data-loads/<revision>/shards/<0-99>:expected_batch_ids array

    :return: None
    """
    config.innovation_configs()

    start_event = {
        "attributes": {
            "REVISION": "Nov-2019-10k",
            "STARTING_RECORD_NUMBER": 9500
        }
    }

    load_maxmind(start_event, None)
