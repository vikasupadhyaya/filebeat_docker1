"""
Unit tests for the create_inserts_helper module.

Will use 'local' mode to reference the committed sample data files with a known data set
that can be used in verifying functionality of methods.
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
import helpers.create_inserts_helper as create_inserts_helper
import helpers.data_loader as data_loader

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


def test_create_firestore_insert_message():
    data_revision = "Q1-2020"
    shard = 1
    shard_batch_id = 123
    records = list()
    records.append(1)

    message = create_inserts_helper.create_firestore_insert_message(data_revision, shard, shard_batch_id, records)
    assert message.get("REVISION") == data_revision
    assert message.get("SHARD") == str(shard)
    assert message.get("SHARD_BATCH_ID") == shard_batch_id
    assert message.get("RECORDS") == records


@pytest.mark.parametrize(
    "geoname_id, network, registered_country_geoname_id, represented_country_geoname_id, is_anonymous_proxy, " + \
    "is_satellite_provider, postal_code, start_ip_int, end_ip_int",
    [
        ("5672477", "1.128.0.0/18", "reg-country", "rep-country", "anony", "sat-prov", "postal111", 25165824, 25182207),
        ("999999999", "1.129.98.0/23", "country2", "rep2", "anony2", "sat-prov2", "postal222", 25256448, 25256959)
    ]
)
def test_create_firestore_insert_record(geoname_id, network, registered_country_geoname_id,
                                        represented_country_geoname_id,
                                        is_anonymous_proxy,
                                        is_satellite_provider,
                                        postal_code, start_ip_int, end_ip_int):
    """
    Use information from local committed maxmind data files which are a sample of full data.

    :return: None
    """
    config.innovation_configs_helpers_tests()
    local_data_revision = "Nov-2019-10k"
    data_loader.initialize_data_files(local_data_revision)
    headers = ["geoname_id",
               "network",
               "registered_country_geoname_id",
               "represented_country_geoname_id",
               "is_anonymous_proxy",
               "is_satellite_provider",
               "postal_code"]
    line = [geoname_id, network, registered_country_geoname_id,
            represented_country_geoname_id,
            is_anonymous_proxy,
            is_satellite_provider,
            postal_code]
    location_detail = data_loader.location_details.get(geoname_id, {})
    record = create_inserts_helper.create_firestore_insert_record(headers, line)
    logger.info("record: {}".format(record))

    assert record.get("geoname_id") == geoname_id
    assert record.get("start_ip_int") == start_ip_int
    assert record.get("end_ip_int") == end_ip_int
    assert record.get("locale_code") == str(location_detail.get("locale_code", ""))
    assert record.get("continent_code") == str(location_detail.get("continent_code", ""))
    assert record.get("continent_name") == str(location_detail.get("continent_name", ""))
    assert record.get("country_iso_code") == str(location_detail.get("country_iso_code", ""))
    assert record.get("country_name") == str(location_detail.get("country_name", ""))
    assert record.get("subdivision_1_iso_code") == str(location_detail.get("subdivision_1_iso_code", ""))
    assert record.get("subdivision_1_name") == str(location_detail.get("subdivision_1_name", ""))
    assert record.get("subdivision_2_iso_code") == str(location_detail.get("subdivision_2_iso_code", ""))
    assert record.get("subdivision_2_name") == str(location_detail.get("subdivision_2_name", ""))
    assert record.get("city_name") == str(location_detail.get("city_name", ""))
    assert record.get("metro_code") == str(location_detail.get("metro_code", ""))
    assert record.get("time_zone") == str(location_detail.get("time_zone", ""))
    assert record.get("is_in_european_union") == str(location_detail.get("is_in_european_union", ""))
