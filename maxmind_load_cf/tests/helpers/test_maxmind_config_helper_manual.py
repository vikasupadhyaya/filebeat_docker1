"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
import helpers.maxmind_config_helper as maxmind_config_helper
from google.cloud import firestore
from tt_common.tt_logging import Logging
from config import get_config_value

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


"""
For testing proper transaction handling and retry on external edit once a transaction is started, the following
was done:
1) Copy from maxmind_config_helper the method with transactional annotation into this test file,
   replacing the signature below this comment block, and include the annotation of firestore transactional
2) Add time.sleep(30) and a logging statement output before sleep - before the return allow_load
3) Make a copy of this test file in another project that has firestore requirement loaded along with other test file
   imports satisfied
4) Start manual test in one project and within sleep time after logging statement of start of sleep is started
   in first project, then run the same test in separate project.  Verify the test started first indicates
   load is allowed and in second test will ultimately return false that load is not allowed due to tx
   behavior of only allowing single revision to load.
"""
def update_maxmind_staging_in_tx(transaction, data_revision, maxmind_config_ref, revision_load_ref, data_file_record_count):
    logger.info("see above instructions for testing tx behavior")


def update_maxmind_staging_details(data_revision):
    """
    An abbreviated version of 'allow_load_and_initialize' from maxmind_config_helper that
    is used to test the tx functionality.

    :param data_revision:
    :return: (boolean) - indicating if tx call is indicating we could proceed with load
    """
    data_file_record_count = 2599
    project = get_config_value("GCP_PROJECT")
    config_collection_name = get_config_value("CONFIG_COLLECTION_NAME")
    data_loads_collection_name = get_config_value("DATA_LOADS_COLLECTION_NAME")

    # Firestore document identifier for maxmind configurations
    maxmind_doc_id = get_config_value("MAXMIND_DOC_ID")

    fs_client = firestore.Client(project)
    transaction = fs_client.transaction()

    config_collection = fs_client.collection(config_collection_name)
    maxmind_config_ref = config_collection.document(maxmind_doc_id)
    revision_load_ref = maxmind_config_ref.collection(data_loads_collection_name).document(str(data_revision))

    allow_load = update_maxmind_staging_in_tx(transaction, data_revision, maxmind_config_ref, revision_load_ref,
                                               data_file_record_count)

    if allow_load:
        logger.info("Yes - proceed with loading the revision {}".format(data_revision))
    else:
        logger.info("No - revision has already been added and is in progress. {}".format(data_revision))

    return allow_load


@pytest.mark.skip(reason="manual test requiring special setup")
def test_maxmind_revision_initialization_check():
    """
    See comments above for testing transaction behavior.

    :return: None
    """
    config.innovation_configs_helpers_tests()
    data_revision = "Nov-2019-manual-test5"
    update_maxmind_staging_details(data_revision)


@pytest.mark.skip(reason="manual test of a revision to load")
def test_allow_load_and_initialize():
    """
    Manual test to confirm structure is created.
    To fully test, all of the ibc-cf-configurations structure should be clean to
    confirm:
    1) the initial maxmind-config document is created
    2) the data-loads and revision and shards sub-collections are created
    3) above test for confirming tx behavior should not be needed again unless code changes are made to tx scope

    :return: None
    """
    config.innovation_configs_helpers_tests()

    data_revision = "Nov-2019-maxmind-config-manual-test"
    data_file_record_count = 4599
    allow_load = maxmind_config_helper.allow_load_and_initialize(data_revision, data_file_record_count)
    logger.info("Result of allow_load is {} for revision {}".format(allow_load, data_revision))
    logger.info("***Verify structure of created instances then run again and verify another load is prevented.")
