"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
import helpers.create_inserts_helper as create_inserts_helper
import helpers.data_loader as data_loader

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test to verify firestore insert messages are created as expected")
def test_get_firestore_insert_messages():
    """
    Requires innovation space to have the data-loads/manual-test-inserts-helper/shards initialized

    Manual verification of the expected batch id and expected record count added to shard
    and messages in expected structure.

    If the committed reference maxmind data files are modified, this will affect the verification
    steps below.

    :return: None
    """
    logger.info("start test")
    config.innovation_configs_helpers_tests()
    local_data_revision = "Nov-2019-10k"
    data_loader.initialize_data_files(local_data_revision)
    data_file = data_loader.data_file_reference
    # above revision is for loading the data file from legit file
    # use this for the firestore path instances to verify within
    data_revision = "manual-test-inserts-helper"
    starting_record_number = 9998
    messages = create_inserts_helper.get_firestore_insert_messages(data_file, data_revision, starting_record_number)

    # manually verify in data-loads/manual-test-inserts-helper/shards/1
    # expected_records_written incremented by 2
    # expected_batch_ids contains 27023240
    logger.info("{}".format(messages))
    assert len(messages) == 1
    assert messages[0].get("REVISION") == data_revision
    assert messages[0].get("SHARD") == "20"
    assert messages[0].get("SHARD_BATCH_ID") == int("27023240")
    assert len(messages[0].get("RECORDS")) == 2

