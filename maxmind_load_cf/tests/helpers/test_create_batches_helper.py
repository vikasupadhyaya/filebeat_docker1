"""
Unit tests for the create_batches_helper module.

Will use 'local' mode to reference the committed sample data files with a known data set
that can be used in verifying functionality of methods.
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
import helpers.create_batches_helper as create_batches_helper

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


def test_get_record_count_from_data_file():
    config.innovation_configs()
    # source control committed revision - subset of full for reference and tests
    data_revision = "Nov-2019-10k"
    # given location of test, need to go up two folders to reference maxmind sample data
    data_file = "../../maxmind/GeoIP2-City-Blocks-IPv4-Nov-2019-10k.csv"
    record_count = create_batches_helper.get_record_count_from_data_file(data_file, data_revision)
    assert record_count == 9999


@pytest.mark.parametrize(
    "data_file_record_count, expected_message_count", [
        (1, 1),
        (249500, 1),
        (249501, 2),
        (998, 1),
        (1247500, 5),
        (1247501, 6)
    ]
)
def test_get_batch_messages(data_file_record_count, expected_message_count):
    config.innovation_configs()
    # tests based on 249500 as configuration value of "PROCESS_RECORDS_PER_BATCH"
    data_revision = "Q1-2020"
    messages = create_batches_helper.get_batch_messages(data_revision, data_file_record_count)
    assert len(messages) == expected_message_count


def test_get_batch_messages_format():
    config.innovation_configs()
    # tests based on 249500 as configuration value of "PROCESS_RECORDS_PER_BATCH"
    data_revision = "Q1-2020"
    messages = create_batches_helper.get_batch_messages(data_revision, 249501)
    # logger.info("messages: {}".format(messages))

    assert messages[0].get("REVISION") == data_revision
    assert messages[0].get("STARTING_RECORD_NUMBER") == 1
    assert messages[1].get("STARTING_RECORD_NUMBER") == 249501


def test_create_batch_record():
    data_revision = "Q1-2020"
    starting_record_number = 5

    message = create_batches_helper.create_batch_record(data_revision, starting_record_number)
    assert message.get("REVISION") == data_revision
    assert message.get("STARTING_RECORD_NUMBER") == starting_record_number
