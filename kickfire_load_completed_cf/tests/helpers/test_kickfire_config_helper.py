"""
Unit tests for kickfire_config_helper

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
import helpers.kickfire_config_helper as kickfire_config_helper
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


def test_get_revision_from_collection_name():
    """
    Verify the revision is properly obtained from a collection name, with expectation
    that other parts of load process prefix 'kickfire-' to the revision.

    :return: None
    """
    expected_version = "Q2-2020"
    collection_name = "kickfire-" + expected_version
    data_version = kickfire_config_helper.get_revision_from_collection_name(collection_name)
    assert data_version == expected_version


def test_get_revision_from_collection_name_overlap():
    """
    Verify the rstrip and lstrip issue is not present in version determination.

    :return: None
    """
    expected_version = "kickfire-123"
    collection_name = "kickfire-" + expected_version
    data_version = kickfire_config_helper.get_revision_from_collection_name(collection_name)
    assert data_version == expected_version


def test_get_kickfire_config_doc_id():
    """
    Verify the path to kickfire configuration document.

    :return: None
    """
    config.innovation_configs()
    expected = "ibc-cf-configurations/kickfire-config"
    config_doc_id = kickfire_config_helper.get_kickfire_config_doc_id()
    assert config_doc_id == expected


def test_get_kickfire_revision_doc_id():
    """
    Verify the constructed path to a kickfire revision document is based on data_revision
    and configuration properties.

    :return: None
    """
    config.innovation_configs()
    data_revision = "Q2-2020"
    expected = "ibc-cf-configurations/kickfire-config/data-loads/" + data_revision
    doc_id = kickfire_config_helper.get_kickfire_revision_doc_id(data_revision)
    assert doc_id == expected
