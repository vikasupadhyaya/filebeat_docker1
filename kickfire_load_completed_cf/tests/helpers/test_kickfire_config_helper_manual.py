"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
import helpers.kickfire_config_helper as kickfire_config_helper
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test requiring special setup")
def test_get_staging_collection_for_completion_check():
    """
    Manual test - update ibc-cf-configurations/kickfire-config document
    1) to have same active and staging values - return None
    2) different values - return staging collection name
    confirm correct value returned

    :return: None
    """
    config.innovation_configs()
    pending_staging_collection = kickfire_config_helper.get_staging_collection_for_completion_check()
    logger.info("Pending collection: {}".format(pending_staging_collection))


@pytest.mark.skip(reason="manual test requiring special setup")
def test_manual_update_active_kickfire_collection():
    """
    Manual test - update ibc-cf-configurations/kickfire-config document
    1) to have same active and staging values - no action taken
    2) staging different than provided - no action taken
    2) provided is equal to staging and different from active - collection promoted

    :return: None
    """
    config.innovation_configs()
    new_active_collection = "kickfire-Q2-2021-2m-v2"
    kickfire_config_helper.update_active_kickfire_collection(new_active_collection)
    logger.info("Verify expected result now in kickfire-config document.  New active in test was '{}'".format(
        new_active_collection))


@pytest.mark.skip(reason="manual test requiring special setup")
def test_not_same_as_staging_update_active_kickfire_collection():
    """
    Manual test - update ibc-cf-configurations/kickfire-config document
    1) confirm configuration for staging_collection is not value used in this test

    :return: None
    """
    config.innovation_configs()
    new_active_collection = "kickfire-Q2-2022-not-same"
    kickfire_config_helper.update_active_kickfire_collection(new_active_collection)
    logger.info("Verify no action taken since new collection is not equal to current staging collection.")


@pytest.mark.skip(reason="manual test requiring special setup")
def test_get_kickfire_revision_expected_count():
    """
    Test record count retrieval from a revision document.

    :return: None
    """
    config.innovation_configs()
    data_revision = "Q2-2021-2m-v2"
    expected_count = 2000000
    doc_record_count = kickfire_config_helper.get_kickfire_revision_expected_count(data_revision)
    logger.info("Revision '{}' has record count of '{}' and expected count of '{}'".format(data_revision,
                                                                                           doc_record_count,
                                                                                           expected_count))
