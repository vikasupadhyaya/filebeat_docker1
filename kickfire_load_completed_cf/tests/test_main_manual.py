"""
Manual tests used during development
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from main import process_event

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test executed during dev")
def test_full_run():
    """
    Will check for completion and promote.

    :return: None
    """
    config.innovation_configs()

    process_event(None, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_do_not_promote():
    """
    Only log results, do not promote a complete collection.

    :return: None
    """
    config.innovation_configs()

    event = {
        "attributes": {
            "MODE": "DO_NOT_PROMOTE"
        }
    }

    process_event(event, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_check_specific_collection():
    """
    Skip determination of which collection to check and use revision specified in request

    :return: None
    """
    config.innovation_configs()

    event = {
        "attributes": {
            "MODE": "CHECK_COLLECTION",
            "COLLECTION": "kickfire-Q2-2021-2m-v2"
        }
    }

    process_event(event, None)
