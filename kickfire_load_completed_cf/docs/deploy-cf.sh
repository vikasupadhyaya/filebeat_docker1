#
# command to deploy in innovation project for testing
#
gcloud functions deploy kickfire_load_completed_cf \
--project tt-temp-2021030444 \
--region=us-east4 \
--runtime python37 \
--service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com \
--source=. \
--trigger-topic ibc.kickfire-load-completed \
--entry-point process_event \
--stage-bucket ibc-client-activity-input-innovation \
--memory=512MB \
--timeout=180 \
--max-instances=1 \
--set-env-vars ENV=tt-temp-2021030444,PROJECT=tt-temp-2021030444,LOG_LEVEL=INFO,CF_ENV=True \
--update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi

