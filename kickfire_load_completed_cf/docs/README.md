# Overview

This cloud function will check for a Kickfire data load being completed
by looking at the summary documents (shards) for any pending work.
When all expected batches are present, then load is complete and the staging
collection can be promoted to become the active collection.

# Test message structure
To support direct testing, an unencoded message structure is one of the following:
(full execution does not rely on any data elements - options provide override flags for testing)

* Perform checks, but do not promote
```
{
    "attributes": {
        "MODE": "DO_NOT_PROMOTE"
    }
}
```

* Check a specific collection - will not promote, only performs is complete check
```
{
    "attributes": {
        "MODE": "CHECK_COLLECTION",
        "COLLECTION": "kickfire-revision"
    }
}
```