# Completing Kickfire Data Load

Kickfire Data is loaded by an automated process and this implementation will perform 
checks on current state of that load to promote the latest collection to become active
and usable by others that rely on kickfire data for their operation.

## Structure within Firestore

The following contains the layout as initially developed:
ibc-cf-configurations/kickfire-config/data-loads/{revision}/shards/{0-99}

where {revision} is a placeholder for quarterly name, and {0-99} are
individual document ids to separate load statistics that this cloud function
will check to determine final status.

kickfire-config is document containing the active collection, and staging collection,
which is what needs to be checked for completion.  If active and staging are the
same collection, then no further checks are necessary.