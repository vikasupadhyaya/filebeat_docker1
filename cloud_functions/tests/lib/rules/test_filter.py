import pytest
from lib.rules.filter import *


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('filter_key_none', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('filter_key_none', '2_process_data.json', '2_rule.json', '2_expected.json'),
        ('filter_key_none', '3_process_data.json', '3_rule.json', '3_expected.json'),
    ]
)
def test_filter_key_none(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert filter_key_none(process_data=generate_input, config=generate_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('filter_key_default', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('filter_key_default', '2_process_data.json', '2_rule.json', '2_expected.json'),
        ('filter_key_default', '3_process_data.json', '3_rule.json', '3_expected.json'),
    ]
)
def test_filter_key_default(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert filter_key_default(process_data=generate_input, config=generate_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('filter_key_false', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('filter_key_false', '2_process_data.json', '2_rule.json', '2_expected.json'),
        ('filter_key_false', '3_process_data.json', '3_rule.json', '3_expected.json'),
    ]
)
def test_filter_key_false(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert filter_key_false(process_data=generate_input, config=generate_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('filter_key_true', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('filter_key_true', '2_process_data.json', '2_rule.json', '2_expected.json'),
        ('filter_key_true', '3_process_data.json', '3_rule.json', '3_expected.json'),
    ]
)
def test_filter_key_true(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert filter_key_true(process_data=generate_input, config=generate_config) == generate_expected
