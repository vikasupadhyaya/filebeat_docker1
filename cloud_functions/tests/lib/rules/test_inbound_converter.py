import pytest
from unittest.mock import patch
import copy

from lib.rules.config import get_inbound_converter_config
from lib.rules.inbound_converter import *
from tests.conftest import read_resource_file
from tests.lib.gcp import firestore
from datetime import datetime


@pytest.mark.parametrize(
    'directory, input_filename, config_filename', [
        ('add_audit_columns', '1_process_data.json', '1_rule.json')
    ]
)
def test_add_partition_columns(
        input_filename, generate_input,
        config_filename, generate_config,
        directory
):
    with patch('lib.rules.inbound_converter.datetime') as dt_time:
        now = datetime.utcnow()
        now_millis = int(str(int(now.timestamp() * 1000)))
        generate_input['ACTIVITY_DATE'] = now_millis
        dt_time.utcfromtimestamp.return_value = now
        dt_time.strftime.return_value = now.strftime('%Y-%m-%d')
        expected_output = copy.deepcopy(generate_input)
        expected_output['ACTIVITY_DATE'] = now.strftime('%Y-%m-%d')
        expected_output['ACTIVITY_MILLIS'] = now_millis

        assert add_partition_columns(process_data=generate_input, config=generate_config) == expected_output


@pytest.mark.parametrize(
    'directory, input_filename, config_filename', [
        ('add_audit_columns', '1_process_data.json', '1_rule.json')
    ]
)
def test_add_audit_columns(
        input_filename, generate_input,
        config_filename, generate_config,
        directory
):
    with patch('lib.rules.inbound_converter.datetime') as dt_time:
        now = datetime.utcnow().strftime('%Y-%m-%d %X')

        dt_time.utcnow.return_value = dt_time
        dt_time.strftime.return_value = now
        expected_output = copy.deepcopy(generate_input)
        expected_output['DATE_CREATED'] = now
        expected_output['DATE_MODIFIED'] = now

        assert add_audit_columns(process_data=generate_input, config=generate_config) == expected_output


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('decode_url', '1_process_data.json', '1_rule.json', '1_expected.json')
    ]
)
def test_decode_url(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert decode_url(process_data=generate_input, config=generate_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('is_eu_allowed', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('is_eu_allowed', '2_process_data.json', '2_rule.json', '2_expected.json')
    ]
)
def test_is_eu_allowed(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert is_eu_allowed(process_data=generate_input, config=generate_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('get_gdpr_region', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('get_gdpr_region', '2_process_data.json', '2_rule.json', '2_expected.json'),
        ('get_gdpr_region', '3_process_data.json', '3_rule.json', '3_expected.json')
    ]
)
def test_get_gdpr_region(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        get_clients, directory
):
    with patch('lib.rules.inbound_converter.firestore.Client') as mock_firestore:
        mock_firestore.return_value = firestore.Client()
        config = generate_config
        assert get_gdpr_region(process_data=generate_input, config=config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, expected_filename', [
        #('filter_blacklist_urls', "1_input_good.json", "1_expected_good.json"),
        #('filter_blacklist_urls', "2_input_bl_match.json", "expected_match_empty.json"),
        ('filter_blacklist_urls', "3_input_match_not_encoded.json", "expected_match_empty.json")
    ]
)
def test_filter_blacklist_urls(
        input_filename, generate_input,
        expected_filename, generate_expected,
        directory
):
    # use real config of this service
    config = get_inbound_converter_config()
    assert filter_blacklist_urls(process_data=generate_input, config=config) == generate_expected


def test_filter_blacklist_urls_encoding():
    # list of URLs that should be blacklisted - but are still in the BQ tables:
    # testing getting encoded and decoded URLs
    urls_to_check = [
        "https://slack.com/signin?redir=%2Fmessages%2FCCJQLCNQZ",
        "https://slack.com/signin?redir=/messages/CCJQLCNQZ",
        "https://slack.com/signin?redir=%2Fhelp%2Ftest",
        "https://slack.com/signin?redir=/help/test",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=%2Fsupport%2Ftickets%2F",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=/support/tickets/",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=%2Fproducts-and-downloads%2F",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=/products-and-downloads/",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=/",
        "https://community.helpsystems.com/login/SignInLocal?returnurl=%2F"
    ]
    # create input_event - with encoded URL as REF_PARAM
    input_event_1 = {
        "LOGSTASH_ID": "marina_id1",
        "REFERER": "https%3A%2F%2Fslack.com%2Fsignin?redir=%2Fhelp%2Ftest",
        "ACTIVITY_IP": "1.0.0.0",
        "ACTIVITY_DATE": "1607557877000",
        "ACTIVITY_TYPE_ID": "31",
        "CID": "123456789",
        "VERSION": "2.0",
        "REF_PARAM": "https://slack.com/signin?redir=%2Fmessages%2FCCJQLCNQZ",
        "USER_AGENT": "Mozilla/5.0 (X11; CrOS aarch64 13421.102.0) blah blah"
    }
    # this URL should be blacklisted - so expected result is an empty event
    expected_event = {}
    # test all urls from the list
    for url_to_check in urls_to_check:
        input_event_1['REF_PARAM'] = url_to_check
        input_event_data = json.loads(json.dumps(input_event_1))
        # use real config of this service
        config = get_inbound_converter_config()
        result_event = filter_blacklist_urls(process_data=input_event_data, config=config)
        assert result_event == expected_event


@pytest.mark.parametrize(
    'directory, input_filename, expected_filename', [
        ('filter_blacklist_client_ids', "1_input_good.json", "1_expected_good.json"),
        ('filter_blacklist_client_ids', "2_input_bad.json", "2_expected_bad.json")
    ]
)
def test_filter_blacklist_client_ids(
        input_filename, generate_input,
        expected_filename, generate_expected,
        directory
):
    # use real config of this service
    config = get_inbound_converter_config()
    assert filter_blacklist_client_ids(process_data=generate_input, config=config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('is_gdpr_allowed', "1_process_data.json", "1_rule.json", "1_expected.json"),
        ('is_gdpr_allowed', "2_process_data.json", "2_rule.json", "2_expected.json"),
        ('is_gdpr_allowed', "3_process_data.json", "3_rule.json", "3_expected.json")
    ]
)
def test_is_gdpr_allowed(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    assert is_gdpr_allowed(process_data=generate_input, config=generate_config) == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ('get_external_id', '1_process_data.json', '1_rule.json', '1_expected.json'),
        ('get_external_id', '2_process_data.json', '2_rule.json', '2_expected.json'),
        ('get_external_id', '3_process_data.json', '3_rule.json', '3_expected.json')
    ]
)
def test_get_external_id(
        input_filename, generate_input,
        config_filename, generate_config,
        expected_filename, generate_expected,
        directory
):
    with patch('lib.rules.inbound_converter.firestore.Client') as mock_firestore:
        mock_firestore.return_value = firestore.Client()
        config = generate_config
        assert get_external_id(process_data=generate_input, config=config) == generate_expected
