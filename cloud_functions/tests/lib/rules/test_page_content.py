import pytest
from unittest.mock import patch
from lib.rules.page_content import *

# TODO get rid of config param to page_contnet() method - use explicit timeout - fix the ignored tests
@pytest.mark.skip(reason="Incomplete Test")
def test_get_html(
        generate_input, input_filename,
        generate_config, config_filename,
        generate_expected, expected_filename,
        directory
):
    assert get_html(process_data=generate_input, config_filename=generate_config)


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, html_filename, expected_filename, status, error', [
        ('scrape_html', '1_process_data.json', 'general_rule.json', '1_test_html.html', '1_expected.json', 'OK', None),
        ('scrape_html', '2_process_data.json', 'general_rule.json', '2_test_html.html', '2_expected.json', 'BLAH', "Some Error"),
        ('scrape_html', '3_process_data.json', 'general_rule.json', '3_test_html.html', '3_expected.json', 'OK', None),
        ('scrape_html', '4_process_data.json', 'general_rule.json', '4_test_html.html', '4_expected.json', 'OK', None),
        ('scrape_html', '5_process_data.json', 'general_rule.json', '5_test_html.html', '5_expected.json', 'OK', None),

    ]
)
@pytest.mark.skip(reason="Jenkins doesnt like the HTML that we are passing to conftest.py.")
def test_scrape_html(
        generate_input, input_filename,
        generate_config, config_filename,
        generate_html, html_filename,
        generate_expected, expected_filename,
        directory, status, error
):
    assert scrape_html()

# TODO get rid of config param to page_contnet() method - use explicit timeout
@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename, status, get_error, title, text, scrape_error', [
        ('page_content', 'general_process_data.json', 'general_rule.json', '1_expected.json', 'OK', None, "Some Cool Title", "Some\nCool\nWords", None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '2_expected.json', 'BLAH', "Some Error", None, None, None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '3_expected.json', 'OK', None, None, "Some\nCool\nWords", None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '4_expected.json', 'OK', None, None, "", None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '5_expected.json', 'OK', None, "A really cool title", "", None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '6_expected.json', 'OK', None, "Another Cool Title", None, None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '7_expected.json', 'OK', None, "", None, None),
        ('page_content', 'general_process_data.json', 'general_rule.json', '8_expected.json', 'OK', None, None, None, None),

    ]
)
def test_page_content(
        generate_input, input_filename,
        generate_config, config_filename,
        generate_expected, expected_filename,
        directory, status, get_error, title, text, scrape_error
):
    with patch('lib.rules.page_content.get_html') as mock_get_html:
        with patch('lib.rules.page_content.scrape_html') as mock_scrape_html:

            mock_get_html.return_value = status, "", get_error
            mock_scrape_html.return_value = title, text, scrape_error
            assert page_content(process_data=generate_input, timeout=5) == generate_expected



