import pytest
from unittest.mock import patch
from tests.lib.gcp import bigquery, firestore
from lib.rules.general import *


@pytest.mark.parametrize(
    'directory, input_filename, config_filename, expected_filename', [
        ("process_rules", "1_log.json", "general_config.json", "1_expected.json"),
    ]
)
def test_process_rules(
        generate_input, input_filename,
        generate_config, config_filename,
        generate_expected, expected_filename,
        directory
):
    with patch('lib.rules.inbound_converter.firestore.Client') as mock_firestore:
        with patch('main.bigquery.Client') as mock_bigquery:
            mock_bigquery.return_value = bigquery.Client()
            mock_firestore.return_value = firestore.Client()

            assert process_rules(data=generate_input, config=generate_config) \
                   == generate_expected


@pytest.mark.parametrize(
    'directory, input_filename, columns, expected_filename', [
        ("filter_columns", "1_process_data.json", [], "1_expected.json"),
        ("filter_columns", "2_process_data.json", None, "2_expected.json"),
        ("filter_columns", "3_process_data.json", ["col1", "col2"], "3_expected.json"),
    ]
)
def test_filter_columns(
        generate_input, input_filename,
        generate_expected, expected_filename,
        columns
):
    assert filter_columns(data=generate_input, columns=columns) == generate_expected
