REGION=us-east4
RUNTIME=python37
SERVICE_ACCOUNT=tt-ibc@tt-pd-eng.iam.gserviceaccount.com
PROJECT_ID=tt-pd-eng
LABELS="tt-component=cloud-function,tt-environment=eng,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi"
IBC_STORE_UNMATCHED=0

# your-unix-command-here
gcloud functions deploy client_activity_input --entry-point process_ibc_event \
   --runtime=$RUNTIME --memory=128MB --trigger-topic ibc.client-activity-input \
   --set-env-vars=ENV=eng,IBC_STORE_UNMATCHED=$IBC_STORE_UNMATCHED --region=$REGION \
   --service-account=$SERVICE_ACCOUNT --update-labels=$LABELS --allow-unauthenticated


