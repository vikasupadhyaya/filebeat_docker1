"""
In the event we want to convert this to Cloud Run, an app file was created (along with a Dockerfile) to use on the
Compute instance for deployment
"""

import os

from flask import Flask
from flask import request

from main import process_event


app = Flask(__name__)


@app.route("/", methods=['POST'])
def proces_ibc():
    """
    Method to mimic the CF processing via Cloud Run
    :return:
    """
    data = request.json['message']
    data['event_id'] = request.json['message']['message_id']

    process_event(event=data, context=None)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
