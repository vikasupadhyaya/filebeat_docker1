REGION=us-east4
RUNTIME=python37
SERVICE_ACCOUNT=tt-ibc@tt-pd-eng.iam.gserviceaccount.com
PROJECT_ID=tt-pd-eng
LABELS="tt-component=cloud-function,tt-environment=eng,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi"

for i in {0..0}
do
    # your-unix-command-here
    gcloud functions deploy input_client_activity-partition_$i --entry-point process_kafka_avro \
       --runtime=$RUNTIME --memory=128MB --trigger-http --set-env-vars=PARTITION=$i --region=$REGION \
       --service-account=$SERVICE_ACCOUNT --update-labels=$LABELS --allow-unauthenticated
    #gcloud scheduler jobs create http input_client_activity-partition_$i \
    #    --schedule="* * * * *" \
     #   --uri=https://$REGION-$PROJECT_ID.cloudfunctions.net/input_client_activity-partition_$i/ \
     #   --http-method=POST \
     #   --project=$PROJECT_ID

done

