"""
Helper file to load the config to Firestore
"""
import json

from google.cloud import firestore

fs_client = firestore.Client()

with open('config/config.json') as f:
    config = json.loads(f.read())

for collection in config.keys():
    for document in config[collection].keys():
        fs_client.collection(collection).document(document).set(config[collection][document])
