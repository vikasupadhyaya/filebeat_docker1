# Testing Plan:
Test coverage is currently implemented using the pytest framework. This framework can be implemented from the command 
line or in any pipeline integration environment. 

The current plan implements a "test" file for each module implemented in the main path. Those files are housed under a 
"tests" folder and the subfolders are a direct mimic of the main folder path. The testing modules are all prefixed with 
"test_". Within those "test_" files, there are parallel functions prefixed by "test_" which are run with a variable set 
of parameters. 

Ex:
```
lib
    rules
        general.py
tests
    lib
        rules
            test_general.py
    test_main.py
main.py
```


### Unit Testing
The current strategy is to create a series of testing scenarios for each function created in each module. Those modules 
make use of the `unittest` framework for implementing the tests and mocking any services that may need to be used. 

For unit testing we implement a environment agnostic set of unit tests that process various scenarios for each function 
prior to release. These tests merely provide the framework of having the function produce the desired result. They use a
series of mocked services so that no access to the outside is required. The tests are implemented by paramaterizing the 
functions via `@mark.parameterize`. Each set of parameters is passed to the function to test the desired output. The 
consequence of setting up the testing this way is that may not be able to do positive and negative testing in the same 
test function. 

Resources are used to help standardize how test data is used in each test function. Inputs, outputs and configurations 
are stored in a `resources` folder under the `tests` directory. The `resources` folder is organized by function with a 
corresponding folder created for each one. This folder is referenced as a parameter in the test setup.
 
Fixtures are used to standardize how resources are read in as well as generate some standard variables that are used in 
a few of the test processes. These functions are are housed in the file `conftest.py`.

Tests can be run in one of two ways:  
1) Run the command `pytest` from the command line in the parent directory. This will run all tests found in all 
subdirectories
2) Run the command `pytest` followed by the path to the specific test module to run, ex: 
`pytest tests/lib/rules/test_inbound_converter.py`

### Linting
Code formatting and structure will adhere to the default guidelines provided by PyLint. A Lint will occur upon
deployment in the CI phase and if not passed, integration will stop. For reference, the following code is run during
that phase  
`find . -path '**/venv' -prune -false -o -name '*.py' | xargs pylint`

### Integration Testing
TODO

Useful links  
Unittest: https://docs.python.org/3/library/unittest.html#module-unittest  
Unittest Mock: https://docs.python.org/3/library/unittest.mock-examples.html  

Pytest: https://docs.pytest.org/en/stable/  
Pytest Parameterize: https://docs.pytest.org/en/stable/parametrize.html  
Pytest Fixture: https://docs.pytest.org/en/stable/fixture.html  

