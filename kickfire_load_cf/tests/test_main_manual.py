"""
Manual tests used during development
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from main import load_kickfire

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_none():
    """
    If no specified revision, look to file system for which file is present
    and then create batch entries.

    The associated firestore collection for the revision present must not exist:
    ibc-cf-configurations/kickfire-config/data-loads/<revision>

    :return: None
    """
    config.innovation_configs()

    load_kickfire(None, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_batches():
    """
    Create batch records using specified file.

    The associated firestore collection for the revision must not exist:
    ibc-cf-configurations/kickfire-config/data-loads/<revision>

    :return: None
    """
    config.innovation_configs()

    batch_event = {
        "attributes": {
            "REVISION": "Q1-2021"
        }
    }

    load_kickfire(batch_event, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_batches_missing_revision():
    """
    Requested revision is not present, then fail.

    :return: None
    """
    config.innovation_configs()

    batch_event = {
        "attributes": {
            "REVISION": "Q1-2019"
        }
    }

    load_kickfire(batch_event, None)


@pytest.mark.skip(reason="manual test executed during dev")
def test_create_insert_messages():
    """
    Manual test to create insert records for a portion of the data file, starting
    at the specified record number from the revision data file, creating insert
    messages published to the ibc.kickfire-insert topic and entries added to
    the load details at:
    firestore:ibc-cf-configurations/kickfire-config/data-loads/<revision>/shards/<0-99>:expected_batch_ids array

    :return: None
    """
    config.innovation_configs()

    start_event = {
        "attributes": {
            "REVISION": "Q1-2021",
            "STARTING_RECORD_NUMBER": 1
        }
    }

    load_kickfire(start_event, None)
