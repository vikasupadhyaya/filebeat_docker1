"""
Unit tests for the create_inserts_helper module.

Will use 'local' mode to reference the committed sample data files with a known data set
that can be used in verifying functionality of methods.
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
import helpers.create_inserts_helper as create_inserts_helper
import helpers.data_loader as data_loader

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


def test_create_firestore_insert_message():
    data_revision = "Q1-2020"
    shard = 1
    shard_batch_id = 123
    records = list()
    records.append(1)

    message = create_inserts_helper.create_firestore_insert_message(data_revision, shard, shard_batch_id, records)
    assert message.get("REVISION") == data_revision
    assert message.get("SHARD") == str(shard)
    assert message.get("SHARD_BATCH_ID") == shard_batch_id
    assert message.get("RECORDS") == records


@pytest.mark.parametrize(
    "line_website, line_start_ip, line_end_ip, expected_is_isp, expected_is_wifi", [
        ("bluestatecoffee.com", "1590", "1623", False, True),
        ("deltawebhosting.com", "290156", "290157", True, False),
        ("techtarget.com", "12345678", "12345679", False, False)
    ]
)
def test_create_firestore_insert_record(line_website, line_start_ip, line_end_ip, expected_is_isp, expected_is_wifi):
    """
    Use information from local committed kickfire data files which are a sample of full data.

    :param (str) line_website: domain
    :param (str) line_start_ip: starting ip as str but converted to int for message
    :param (str) line_end_ip: ending ip as str but converted to int for message
    :param (boolean) expected_is_isp: result of line_website being in isp config file
    :param (boolean) expected_is_wifi: result of line_website being in wifi config file
    :return: None
    """
    config.innovation_configs_helpers_tests()
    local_data_revision = "Q1-2021"
    data_loader.initialize_data_files(local_data_revision)
    headers = ["Start IP", "End IP", "Website"]
    line = [line_start_ip, line_end_ip, line_website]
    record = create_inserts_helper.create_firestore_insert_record(headers, line)
    assert record.get("Website") == line_website
    assert record.get("start_ip_int") == int(line_start_ip)
    assert record.get("end_ip_int") == int(line_end_ip)
    assert record.get("is_isp") == expected_is_isp
    assert record.get("is_wifi") == expected_is_wifi
