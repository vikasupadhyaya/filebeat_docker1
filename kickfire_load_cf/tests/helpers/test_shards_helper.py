import pytest
import logging
from tt_common.tt_logging import Logging
import helpers.shards_helper as shards_helper

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "file_record_number, expected_shard", [
        (1, 0),
        (499, 0),
        (500, 1),
        (998, 1),
        (999, 2),
        (49401, 98),
        (49402, 99),
        (49900, 99),
        (49901, 0)
    ]
)
def test_get_shard_number_for_record(file_record_number, expected_shard):
    """
    Invoke the method and verify correct shard id is returned.

    :param file_record_number:
    :param expected_shard:
    :return: None
    """
    record_batch_size = 499
    shard = shards_helper.get_shard_number_for_record(file_record_number, record_batch_size)
    assert shard == expected_shard


@pytest.mark.parametrize(
    "data_revision, shard, expected_id", [
        ("Q1-2020", 0, "ibc-cf-configurations/kickfire-config/data-loads/Q1-2020/shards/0"),
        ("Q2-2022", 99, "ibc-cf-configurations/kickfire-config/data-loads/Q2-2022/shards/99"),
        ("Q3-2021", 50, "ibc-cf-configurations/kickfire-config/data-loads/Q3-2021/shards/50"),
        ("Q4-2021-test", 150, "ibc-cf-configurations/kickfire-config/data-loads/Q4-2021-test/shards/150")
    ]
)
def test_get_shard_doc_id(data_revision, shard, expected_id):
    """
    Verify construction of shard document id, with expectation that the configuration values
    are as follows: (changes would have wider impact on other cloud functions since the base structure
    provides access to the kickfire-config document that is used to indicate active collection)
    "CONFIG_COLLECTION_NAME": "ibc-cf-configurations"
    "DATA_LOADS_COLLECTION_NAME": "data-loads"
    "SHARDS_COLLECTION_NAME": "shards"
    "KICKFIRE_DOC_ID": "kickfire-config"

    :param (str) data_revision: to include in path construction
    :param (int) shard: number to construct path to
    :param (str) expected_id: expected path to shard
    :return: None
    """
    doc_id = shards_helper.get_shard_doc_id(data_revision, shard)
    assert doc_id == expected_id


def test_get_shards_for_tracking():
    """
    The tracking shards are a list of lists so that during file processing we can track
    the expected batch ids and expected record counts encountered for a particular shard,
    and then prior to return messages for publishing, update firestore shard documents indicating
    what is to be expected when the insert records add the firestore entries and subsequently update
    the shard documents with what was actually written.

    :return: None
    """
    expected_num_shards = 100
    tracking_shards = shards_helper.get_shards_for_tracking()

    assert len(tracking_shards) == expected_num_shards
    # verify not empty and a list
    for num in range(expected_num_shards):
        assert len(tracking_shards[num]) == 0
        assert type(tracking_shards[num] is list)
