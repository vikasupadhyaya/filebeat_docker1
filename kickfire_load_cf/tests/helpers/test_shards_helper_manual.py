"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from google.cloud import firestore
from config import get_config_value
import helpers.shards_helper as shards_helper

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test to verify empty batch is acceptable")
def test_nothing_in_batch():
    """
    Scenario is possible in workflow that a batch commit can be empty, so confirm
    valid in api.

    :return: None
    """
    fs_client = firestore.Client("tt-temp-2021030444")
    logger.info("start batch")
    batch = fs_client.batch()
    batch.commit()
    logger.info("done with commit")


@pytest.mark.skip(reason="manual test to verify shards collection is properly initialized")
def test_initialize_shards():
    """
    Requires a clean document (no shards sub-collection) in
    ibc-cf-configurations/kickfire-config/data-loads/shards-manual-test

    :return: None
    """
    config.innovation_configs_helpers_tests()

    data_revision = "shards-manual-test"

    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    config_collection = fs_client.collection(get_config_value("CONFIG_COLLECTION_NAME"))
    kickfire_config_ref = config_collection.document(get_config_value("KICKFIRE_DOC_ID"))
    shards_parent_doc_ref = kickfire_config_ref.collection(get_config_value("DATA_LOADS_COLLECTION_NAME")).document(data_revision)
    shards_parent_doc_ref.set({"test_placeholder": "manual testing"})

    shards_helper.initialize_shards(shards_parent_doc_ref)
    logger.info("Verify shards initialized in ibc-cf-configurations/kickfire-config/data-loads/" + data_revision)


@pytest.mark.skip(reason="manual test to verify shards are merged with provided values")
def test_merge_expected_batch_ids_to_shards():
    """
    Requires manual setup with a clean shards collection.
    Run above test - test_initialize_shards - with pre-step of ensuring clean shards collection.

    After execution, shard 0 and 1 to be verified with test data for expected batch ids and expected
    records to be written.

    :return: None
    """
    config.innovation_configs_helpers_tests()

    data_revision = "shards-manual-test"
    shards_expected_batch_ids = shards_helper.get_shards_for_tracking()
    shards_expected_record_counts = shards_helper.get_shards_for_tracking()

    # shard 0 to have expected batch ids of 123 and 456
    shards_expected_batch_ids[0].append(123)
    shards_expected_batch_ids[0].append(456)

    # shard 0 to have expected written records value of 20 - from 12, 8
    shards_expected_record_counts[0].append(12)
    shards_expected_record_counts[0].append(8)

    # shard 1 to have expected batch ids of 789 and 222
    shards_expected_batch_ids[1].append(789)
    shards_expected_batch_ids[1].append(222)

    # shard 1 to have expected written records value of 50 - from 40, 10
    shards_expected_record_counts[1].append(40)
    shards_expected_record_counts[1].append(10)

    shards_helper.merge_expected_batch_ids_to_shards(data_revision, shards_expected_batch_ids,
                                                     shards_expected_record_counts)
    logger.info("Verify shards updated with above test values for shard 0 and shard 1")
