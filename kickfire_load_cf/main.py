"""
Entry point for starting the Kickfire data load.

When deployed as cloud function, will look for kickfire data files from cloud
storage, download when present, and begin process to load the data into a new
firestore collection.

Since firestore is storage location, there is no efficient way of updating or replacing
previously loaded values, so we load into a new collection and then change the configuration
to refer to the new collection when all data is loaded into new collection.

firestore:ibc-cf-configurations/kickfire-config:active_collection
- the value other cloud functions should use when looking up kickfire details

firestore:ibc-cf-configurations/kickfire-config:previous_active_collection
- to keep cloud function instances from using different collections for data retrieval, the cache mechanism
  in place should determine based on time which collection should be removed

firestore:ibc-cf-configurations/kickfire-config:staging_collection
-updated by this cloud function when new revision is being loaded)
"""
import json
import base64
import logging
import helpers.data_loader as data_loader
import helpers.create_inserts_helper as inserts_helper
import helpers.create_batches_helper as batches_helper
import helpers.kickfire_config_helper as kickfire_config_helper
from datetime import datetime, timezone
from dateutil import parser
from tt_common.tt_logging import Logging
from lib.pubsub import publish_events_to_topic
from lib.manage_futures import wait_for_futures_to_complete
from config import init_configs
from config import get_config_value

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize the configurations for process
init_configs()

# Initialize logging
Logging().init_logging()
logger = logging.getLogger(__name__)

# Informational detail to determine re-use of instance
execution_count = 0


def ignore_aged_event(event, context):
    """
    Determine if the age of the event is older than the configured value to prevent
    continuous processing since we retry failures.

    :param (dict) event: containing details of event
    :param (google.cloud.functions.Context) context: of invocation containing event time
    :return: (boolean) - True if event is to be discarded; False to process
    """
    ignore_event = False
    if not context:
        logger.info("No context provided - no age check performed during direct tests.")
        return ignore_event

    try:
        timestamp = context.timestamp
        event_time = parser.parse(timestamp)
        event_age = (datetime.now(timezone.utc) - event_time).total_seconds()
        if event_age > int(get_config_value("MAX_SECONDS_EVENT_AGE")):
            logging.info(
                "Event is being dropped due to age. (age of event: {} seconds) - Event: {}".format(
                    event_age, event))
            ignore_event = True
    except Exception as e:
        logging.error("Problem in age of event check - aborting and not retrying. {}".format(str(e)))
        ignore_event = True

    return ignore_event


def publish_messages(publish_to_topic, messages):
    """
    Publishes and waits for all futures to complete before returning.

    :param publish_to_topic: name of topic
    :param messages: list of messages to publish
    :return: None
    """
    if not messages:
        logging.debug("No messages to publish.")
        return

    """
    Log messages only available during local testing and development since we do
    not require pympler for determining object size other than for this purpose.
    
    logger.info("messages: {}".format(messages))
    logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
    logger.info("Publishing {} messages to topic {}".format(len(messages), publish_to_topic))
    """
    publish_events_to_topic(messages, get_config_value("GCP_PROJECT"), publish_to_topic)
    wait_for_futures_to_complete()


def load_kickfire(received_event_param, context):
    """
    Support two structures of messages:
    1) initial parsing and kickoff of loading process to create batch
       messages that will process a portion of the large data file
    2) a batch message for a revision and starting line to create
       the messages to populate the new firestore collection of
       the latest kickfire data

    :param received_event_param: message from pubsub or direct test
    :param context: cloud function context
    :return: None
    """
    global execution_count
    execution_count += 1
    logger.info("Function instance execution count: {}".format(execution_count))

    try:
        # received_event may be for initial triggering of load
        # or may be specifying a batch to process
        received_event = dict() if not received_event_param else received_event_param
        if "data" in received_event:
            event = json.loads(base64.b64decode(received_event['data']).decode('utf-8'))
        elif "attributes" in received_event:
            event = json.loads(json.dumps(received_event['attributes']))
        else:
            event = dict()

        if ignore_aged_event(event, context):
            return

        """
        Use 'get' style access in case we do not receive a value from legitimate message
        structure we can fall back to begin processing and create batch records
        with obtained revision from storage location.
        """
        # ensure the data is present for revision to process
        data_loader.initialize_data_files(event.get("REVISION"))

        data_revision = data_loader.loaded_data_revision
        starting_record_number = event.get("STARTING_RECORD_NUMBER")
        allow_reload = event.get("ALLOW_RELOAD", "NO")

        logging.info(("Processing event for revision: {}; " +
                     "starting record: {}; allow reload: {}").format(data_revision,
                                                                    starting_record_number, allow_reload))
        # STARTING_RECORD_NUMBER presence determines if we are doing initial start or processing lines
        # to create messages to insert into firestore
        messages = []
        # Initialize to something to prevent warning when we fall thru due to guard conditions
        publish_to_topic = ""
        if starting_record_number:
            logger.info("Received request to start processing at record {}".format(starting_record_number))
            publish_to_topic = get_config_value("PUBLISH_TO_TOPIC_INSERT")
            messages = inserts_helper.get_firestore_insert_messages(data_loader.data_file_reference, data_revision,
                                                                    starting_record_number)
        elif data_revision is not None:
            """
            Within a transaction, update the staging information to prevent multiple loads of
            the same kickfire data without some manual intervention.  Costs associated to firestore
            writes are expensive at large volumes such as the kickfire loads.
            """
            data_file_record_count = batches_helper.get_record_count_from_data_file(
                data_loader.data_file_reference, data_revision)
            if kickfire_config_helper.allow_load_and_initialize(data_revision, data_file_record_count) \
                    or allow_reload == "YES":
                logger.info("Create batch records for data revision {}".format(data_revision))
                publish_to_topic = get_config_value("PUBLISH_TO_TOPIC_BATCH")
                messages = batches_helper.get_batch_messages(data_revision, data_file_record_count)
            else:
                logger.info("ALERT - ignoring request to add revision {} since it has been loaded previously.".format(
                    data_revision))
        else:
            logger.info("Information not present to create batch records or to create insert records.")

        publish_messages(publish_to_topic, messages)

    except Exception as e:
        logger.error("Fatal error processing kickfire data to load - Error: {}".format(str(e)))

        # retry on failures by CF deployment configuration
        # age check performed on entry to prevent continuous retries
        # Retries added due to potential overloading of firestore inserts seen in production env:
        # Error: 409 Too much contention on these documents. Please try again.
        raise e
