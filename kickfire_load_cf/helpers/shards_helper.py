"""
Provide common routines for dealing with shards, which are individual documents used
to collect information for a batch of records to provide more efficient handling of
tracking and counts of inserts since a single document write has limited change rate of
once per second that is not suitable for the volume of data being loaded in most cases.
"""
import logging
from google.cloud import firestore
from config import get_config_value

# Initialize logging
logger = logging.getLogger(__name__)

# The number of separate documents to track batches of inserts so we can
# determine successful loading based on expected batches to be processed, those which
# are processed, and the number of records actually written to compare against the
# expected number of records from revision.
num_shards = 100


def get_shard_number_for_record(file_record_number, records_batch_size):
    """
    Obtain the target shard the record would be counted in.
    This is determined by the row of data file (file_record_number) and the number
    of records we batch together for a write.

    :param (int) file_record_number: NOT the line number of file - add 1 to account for header row
    :param (int) records_batch_size: number of records batched together for writting - not to exceed 499
    :return: (int) - using 100 shards, a value from 0-99
    """
    return ((file_record_number - 1) // records_batch_size) % num_shards


def get_shard_doc_id(data_revision, shard):
    """
    Use the values from the cloud function configuration instance to construct a firestore
    document id path.

    Uses the following configuration keys in construction of path value:
    CONFIG_COLLECTION_NAME
    KICKFIRE_DOC_ID
    DATA_LOADS_COLLECTION_NAME
    SHARDS_COLLECTION_NAME

    :param (str) data_revision: to include in path of shard document reference
    :param (int) shard: number of shard to create document id with
    :return: (str): a firestore document id, with full path information
    """
    return str(get_config_value("CONFIG_COLLECTION_NAME")) + "/" + str(get_config_value("KICKFIRE_DOC_ID")) + \
        "/" + str(get_config_value("DATA_LOADS_COLLECTION_NAME")) + "/" + str(data_revision) + "/" + \
        str(get_config_value("SHARDS_COLLECTION_NAME")) + "/" + str(shard)


def get_shards_for_tracking():
    """
    Obtain a list of same size as the number of available shards that can be used as a collector
    of details as a batch is being constructed, and then used to merge into the shard documents.

    :return: (list): of length num_shards that has a list at each position for adding details
    """
    shards = list()
    for num in range(num_shards):
        shards.append(list())
    return shards


def initialize_shards(revision_doc_ref):
    """
    Perform the writes to initialize document instances with expected fields that will be
    used during processing.

    :param (firestore document ref) revision_doc_ref: that the shards collection will be a child of
    :return: None
    """
    shards_col_ref = revision_doc_ref.collection(get_config_value("SHARDS_COLLECTION_NAME"))

    # Initialize each shard
    for num in range(num_shards):
        shard = {
            "expected_batch_ids": [],
            "handled_batch_ids": [],
            "expected_records_written": 0,
            "records_written": 0,
            "created_timestamp": firestore.SERVER_TIMESTAMP,
            "last_updated": firestore.SERVER_TIMESTAMP
        }
        shards_col_ref.document(str(num)).set(shard)


def merge_expected_batch_ids_to_shards(data_revision, shards_expected_batch_ids, shards_expected_record_counts):
    """
    Update firestore documents with expected batch ids and sum of expected record counts for
    a particular data_revision.

    Both of the 'shards_expected' parameters are lists equal to the number of shards we are using,
    with each position corresponding to a shard id, the list of values.  The list of 'values' are either
    the expected batch ids to add to shard, or a list of expected record counts that are summed and incremented
    into the expected_records_count document property.

    :param (str) data_revision: being updated and used in obtaining shards to update
    :param (list) shards_expected_batch_ids: list containing lists of expected batch ids
    :param (list) shards_expected_record_counts: list containing lists of expected records to sum
    :return: None
    """
    if len(shards_expected_batch_ids) != num_shards or len(shards_expected_record_counts) != num_shards:
        logger.error("Provided list of shards not equal to size of shards available")
        raise Exception("Incorrect number of shards provided to update expected batch ids.")

    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    batch = fs_client.batch()

    for num in range(num_shards):
        expected_batch_ids = shards_expected_batch_ids[num]
        if len(expected_batch_ids) > 0:
            increment_expected_records = 0
            expected_records_counts = shards_expected_record_counts[num]
            for expected_count in expected_records_counts:
                increment_expected_records += int(expected_count)
            shard_doc_id = get_shard_doc_id(data_revision, num)
            shard_ref = fs_client.document(shard_doc_id)
            batch.update(shard_ref, {"expected_batch_ids": firestore.ArrayUnion(expected_batch_ids),
                                     "expected_records_written": firestore.Increment(increment_expected_records),
                                     "last_updated": firestore.SERVER_TIMESTAMP})

    batch.commit()
