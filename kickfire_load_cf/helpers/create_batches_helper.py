"""
Methods to support the creation of batch requests to perform the kickfire load.

A batch for the loading process is a separation of the large kickfire data file
into smaller messages so that multiple cloud function instances can process a section
of the data file concurrently, which in turn creates the insert records to be handled
by another cloud function monitoring a pub/sub topic to insert a batch of records
to firestore.
"""
import csv
import logging
from config import get_config_value

# Initialize logging
logger = logging.getLogger(__name__)


def get_record_count_from_data_file(data_file, data_revision):
    """
    Obtain the total number of configuration records present in data_file so that
    batches can be created for separate processing.

    :param (str) data_file: path and name of twinranges file containing source data
    :param (str) data_revision: that created messages will specify as source file to process
    :return: (int) - number of records in data_file
    """
    record_count = 0

    with open(data_file, encoding="utf-8", errors='ignore') as infile:
        reader = csv.reader(infile)
        headers = next(reader)
        for line in reader:
            record_count += 1

    logging.info("Determined data file for revision {} has {} records".format(data_revision, record_count))
    return record_count


def create_batch_record(data_revision, starting_record_number):
    """
    Create messages to process sections of a data_file for concurrent processing
    of large files.

    :param (str) data_revision: that created messages will specify as source file to process
    :param (int) starting_record_number: of data file to begin batch
    :return: (dict) - pubsub message
    """
    record = {
        "REVISION": str(data_revision),
        "STARTING_RECORD_NUMBER": int(starting_record_number)
    }
    return record


def get_batch_messages(data_revision, data_file_record_count):
    """
    Determine the size of the data_file and create messages that will process
    sections of the file to allow for concurrent processing in smaller chunks of
    the large twinranges source file.

    :param (str) data_revision: that created messages will specify as source file to process
    :param (int) data_file_record_count: the number of records to be written
    :return: (list) - messages to process sections of data_file
    """
    process_records_per_batch = get_config_value("PROCESS_RECORDS_PER_BATCH")
    batches_needed = data_file_record_count // process_records_per_batch
    partial_batch_count = data_file_record_count % process_records_per_batch
    if partial_batch_count > 0:
        batches_needed += 1

    messages = []
    for current_batch in range(batches_needed):
        starting_record_number = (current_batch * process_records_per_batch) + 1
        messages.append(create_batch_record(data_revision, starting_record_number))

    logging.info("Created {} batch messages to process data revision {}".format(len(messages), data_revision))
    return messages
