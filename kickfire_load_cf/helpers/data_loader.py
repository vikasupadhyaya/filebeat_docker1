import os
import re
import logging
from google.cloud import storage
from config import get_config_value


logger = logging.getLogger(__name__)

data_file_prefix = "twinranges-"
isp_file_prefix = "ispfilter-"
wifi_file_prefix = "wififilter-"

# always local file reference to data file
# path and file name
data_file_reference = None
isp_domains = []
wifi_domains = []
loaded_data_revision = None


def parse_reference_to_list(input_path):
    """
    Small function to read in reference file to use in processing
    Return a set for better performance.  Difference in using a list
    vs a set for 300k records was over 8 minutes compared to 6 seconds
    using a set.

    :param (str) input_path: Path to reference file
    :return (set): reference list
    """
    ref_list = []
    with open(input_path, encoding="utf-8") as infile:
        for line in infile:
            line = re.sub("\"", "", line)
            line = re.sub("\n", "", line)
            ref_list.append(line)

    return set(ref_list)


def get_revision_from_filename(file_name):
    """
     Obtain the revision from file_name, which is the quarter name of data to load.

    :param (str) file_name: example twinranges-Q1-2021.csv; must be twinranges- prefixed filename
    :return (str) revision: extracted from file_name
    """
    suffix = ".csv"
    extracted_revision = file_name
    if extracted_revision.endswith(suffix):
        extracted_revision = extracted_revision[:-len(suffix)]
    if extracted_revision.startswith(data_file_prefix):
        extracted_revision = extracted_revision[len(data_file_prefix):]

    clean_revision = extracted_revision.strip()

    if not clean_revision or clean_revision != extracted_revision:
        logger.error(("Error - revision must be specified and may not contain leading or trailing " +
                     "whitespace: '{}'").format(extracted_revision))
        raise Exception("Data revision must be specified and may not contain leading or trailing whitespace.")

    return clean_revision


def get_revision_stored_locally():
    """
    Obtain the revision of data file present for loading.

    :return: (str) - present revision from local storage
    """
    revision = None
    path = get_config_value("LOCAL_PATH").rstrip("/")
    files = os.listdir(path)

    for file_name in files:
        if str(file_name).startswith(data_file_prefix):
            if not revision:
                revision = get_revision_from_filename(file_name)
            else:
                raise Exception("Multiple KickFire quarterly data files are present.")

    return revision


def get_revision_stored_remotely():
    """
    Obtain the revision of data file present for loading from cloud storage.

    :return: (str) - present revision from cloud storage
    """
    revision = None
    gcp_project = get_config_value("GCP_PROJECT")
    storage_bucket_name = get_config_value("STORAGE_BUCKET")
    retrieve_file_prefix = get_config_value("CLOUD_STORAGE_PATH").rstrip("/") + "/" + data_file_prefix

    storage_client = storage.Client(project=gcp_project)
    blobs = list(storage_client.list_blobs(bucket_or_name=storage_bucket_name, prefix=retrieve_file_prefix))

    for blob in blobs:
        remote_file_path, remote_file_name = os.path.split(blob.name)

        if not revision:
            revision = get_revision_from_filename(remote_file_name)
        else:
            raise Exception("Multiple KickFire quarterly data files are present.")

    return revision


def get_revision_to_process():
    """
    Determine from local or remote data file presence the revision of data file present to load.
    The naming convention of files is twinranges-<revision>.csv, ispfilter-<revision>.csv, and
    wififilter-<revision>.csv.  Only the twinranges presence will be used to obtain present revision.

    :return (str) revision: None if not determined; otherwise quarter revision found
    """
    revision = get_revision_stored_locally() if get_config_value("LOCAL_DEV") else get_revision_stored_remotely()
    return revision


def get_local_path_to_data_files():
    """
    Obtain the location of data files to use.  When in local dev mode, this
    is usually a location corresponding to committed sample files, and when
    deployed as a cloud function, the temporary storage location of the downloaded
    files.  Used to abstract out the local/cloud location.

    :return: (str) - of path to data files
    """
    local_path_ref = get_config_value("LOCAL_PATH") if get_config_value("LOCAL_DEV") else get_config_value("TEMP_STORAGE")
    return local_path_ref.rstrip("/")


def retrieve_files_from_storage(retrieve_files):
    """
    Obtain from cloud storage and store locally for further processing.

    For cloud deployment, we need to retrieve from cloud storage
    the data files used by the function for processing.
    If in "local dev" mode, then the local file references will be
    used instead of retrieving from cloud storage.

    :param (list) retrieve_files: names of files to download
    :return: None
    """
    if get_config_value("LOCAL_DEV"):
        logger.info("Local Dev mode - no cloud storage retrieval performed")
        return

    logger.info("Starting data file retrieval...")

    gcp_project = get_config_value("GCP_PROJECT")
    storage_bucket_name = get_config_value("STORAGE_BUCKET")
    temp_storage = get_config_value("TEMP_STORAGE")
    retrieve_file_prefix = get_config_value("CLOUD_STORAGE_PATH").rstrip("/") + "/"

    os.makedirs(name=temp_storage, exist_ok=True)

    storage_client = storage.Client(project=gcp_project)
    bucket = storage_client.bucket(storage_bucket_name)
    blobs = list(
        storage_client.list_blobs(bucket_or_name=storage_bucket_name, prefix=retrieve_file_prefix, delimiter="/"))

    for blob in blobs:
        remote_file_path, remote_file_name = os.path.split(blob.name)
        if remote_file_name in retrieve_files:
            local_file_name = os.path.join(temp_storage, remote_file_name)
            download_blob = bucket.blob(blob.name)
            download_blob.download_to_filename(client=storage_client, filename=local_file_name)
            logger.info("Downloaded '{}' to local file '{}'".format(blob.name, local_file_name))

    logger.info('Completed data file retrieval.')


def clean_local_temp_storage():
    """
    When running in cloud, we have to retrieve from cloud storage
    since the data files will change as we receive new configurations.
    We only remove prior to retrieving new files since the same
    function instance may be used multiple times, reading from data file.

    :return: None
    """
    if get_config_value("LOCAL_DEV"):
        logger.info("Local Dev mode - no cleanup performed")
        return

    # remove the tmp files since they are stored in-memory
    # in cloud function
    logger.info("Starting cleanup routine...")

    temp_storage = get_config_value("TEMP_STORAGE")

    try:
        for root_folder, directories, files in os.walk(temp_storage, topdown=False):
            for name in files:
                current_file_ref = os.path.join(root_folder, name)
                logger.debug("removing file: {}".format(current_file_ref))
                os.remove(current_file_ref)

        logger.debug("removing temp storage folder")
        os.rmdir(temp_storage)
    except Exception as e:
        logger.debug("Exception cleaning temp storage: {}".format(str(e)))

    logger.info("Completed file cleanup routine.")


def load_data_files(revision):
    """
    Retrieve from cloud storage if necessary the data files corresponding to the specified revision.
    Then initialize the isp and wifi sets for lookup of domains during processing of data file.
    The name of the data file is stored for use during iterations of the large file to
    process in batches.

    :param (str) revision: of data files to use for initializing data structures
    :return: None
    """
    global data_file_reference
    global isp_domains
    global wifi_domains
    global loaded_data_revision

    path_to_data_files = get_local_path_to_data_files()
    data_file_name = data_file_prefix + revision + ".csv"
    isp_file_name = isp_file_prefix + revision + ".csv"
    wifi_file_name = wifi_file_prefix + revision + ".csv"

    retrieve_files = {data_file_name, isp_file_name, wifi_file_name}
    clean_local_temp_storage()
    retrieve_files_from_storage(retrieve_files)

    data_file_reference = path_to_data_files + "/" + data_file_name
    isp_domains = parse_reference_to_list(path_to_data_files + "/" + isp_file_name)
    wifi_domains = parse_reference_to_list(path_to_data_files + "/" + wifi_file_name)

    loaded_data_revision = revision


def initialize_data_files(requested_revision):
    """
    Ensure the requested_revision is the data files that are currently loaded
    for iteration and lookups.  Since cloud function instances may linger, we
    make sure the requested_revision corresponds to the loaded data, otherwise
    we obtain and load the requested_revision.  If no requested_revision is
    explicitly provided, we look up from local or cloud storage the quarter data
    file that is present to load.

    :param (str) requested_revision: may be None; otherwise the revision of data files to load for referencing
    :return: None
    """
    global loaded_data_revision

    revision_to_load = requested_revision

    # None or empty, then get single occurrence or fail
    if not revision_to_load:
        revision_to_load = get_revision_to_process()

    if not revision_to_load:
        raise Exception("A KickFire quarterly data file is not present.")

    if revision_to_load != loaded_data_revision:
        loaded_data_revision = None
        load_data_files(revision_to_load)
