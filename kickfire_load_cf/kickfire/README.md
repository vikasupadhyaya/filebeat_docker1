# Local Data Files

The files located in this folder are for local development.
The following files are expected to be present:
`twinranges-<revision>.csv`
`ispfilter-<revision>.csv`
`wififilter-<revision>.csv`

The main data file (twinranges prefix) is a small portion of entries for commit reference.
Pull full files as needed from cloud storage:
* tt-pd-eng/ibc-configurations-eng/kickfire
