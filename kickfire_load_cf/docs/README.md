# Overview
Kickfire data loading is performed on a quarterly basis by an automated data
services process which retrieves the most recent data, performs a load for data
services and then will drop the files into a cloud storage bucket which has a trigger
defined to begin our load process.

## Cloud Storage Bucket monitored
See storage_configs_cf for implementation.

* ibc-configurations-{environment} - /kickfire folder  
  twinranges-{revision}.csv - the main data file
  ispfilter-{revision}.csv - domains which are isp providers  
  wififilter-{revision}.csv - domains which are wifi based  


Where {revision} is the quarterly revision number that will be used in tracking and naming of the firestore collection created (kickfire-{revision})

## Process Data File - Create Batch insert records
Given size of the main data file, we perform two stages of batching to distribute the processing to create the insert records.
1. Cloud Storage trigger will provide the {revision} that is to be loaded
2. The data file will be read to separate out into a reasonable processing batch size such as 'process lines starting at line 1 thru {batch size}'
3. The same pub/sub topic is used for initial revision start and subsequent batch handling from a starting line
4. The batch request with a starting line will create insert messages of final firestore records to insert and publish to ibc.kickfire-insert (see kickfire_insert_cf)


# Test message structure
To support direct testing, an unencoded message structure is one of the following:

* Initial starting point to process a new quarterly file: (if REVISION is empty, then a single data file is looked for in cloud storage)
* An override property "ALLOW_RELOAD" may be specified with value "YES" to skip the prevention of reprocessing a file already marked as loaded in event we want to recreate insert messages.
```
{
    "attributes": {
        "REVISION": "Q1-2021"
    }
}
```

* To process a batch, which is a range of lines from a quarterly data file:
```
{
    "attributes": {
        "REVISION": "Q1-2021",
        "STARTING_RECORD_NUMBER": 1
    }
}
```

Batch processing only specifies a starting record number and will process the configured number of lines that represent a batch.
The creation of batch messages is performed from initial message to start the load process.