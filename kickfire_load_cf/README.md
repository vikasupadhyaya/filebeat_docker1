# Loading Kickfire Data

The Kickfire data set is comprised of three data files on a quarterly basis:
* `twinranges-<revision>.csv` - large data file of domains and ip addresses
* `ispfilter-<revision>.csv` - the domains which are isps
* `wififilter-<revision>.csv` - the domains which are wifi based

This cloud function serves two purposes:
* Create batch messages to process the large twinranges file in chunks
* Process to handle batch messages to create messages for firestore insertion

The initial starting step will be triggered by a cloud storage addition of the Kickfire data
which will begin the process.  When loading is completed, the configuration of which kickfire collection
to use in other IBC flow processes will be updated to new collection (revision from filename).
