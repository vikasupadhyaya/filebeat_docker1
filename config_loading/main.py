"""
Helper file to load the config to Firestore
"""
import logging
import uuid
from datetime import datetime, timezone
from google.cloud import firestore
from tt_common.tt_logging import Logging

Logging().init_logging()
logger = logging.getLogger(__name__)


def load_config(request):
    fs_client = firestore.Client()
    collection_name = 'config_donotprocess_cids'
    logger.info("Processing request to load configuration into {} Firestore collection".format(collection_name))

    with open('config/do_not_process_cids.txt') as f:
        bl_cids_list = f.read().splitlines(keepends=False)

    if not bl_cids_list:
        print("List of CIDs to upload is empty - exiting")
        return

    # datetime has to be in UTC for the doc_id
    now_datetime_local = datetime.now()
    now_datetime_utc = datetime.utcnow()
    now_timestamp = now_datetime_local.timestamp()
    now_timestamp_millis = int(now_timestamp * 1000)
    datetime_utc_str = now_datetime_local.astimezone(timezone.utc).strftime('%Y-%m-%d_%H%M%S_%Z')

    document_id = datetime_utc_str + '_' + str(uuid.uuid4())
    document_body = {'cids': bl_cids_list,
                     'created_at_timestamp_ms': now_timestamp_millis,
                     'created_at_datetime_utc': now_datetime_utc,
                     'created_at_datetime_utc_str': datetime_utc_str}
    logger.info("document_body={}".format(document_body))

    fs_client.collection(collection_name).document(document_id).set(document_body)
    logger.info("uploaded document_id={} OK".format(document_id))


if __name__ == "__main__":
    load_config(None)