import pytest
import main
from unittest.mock import patch
from tests.lib.gcp import bigquery
from tests.conftest import generate_context

@pytest.mark.parametrize(
    "directory, event_filename, expected_filename, event_data_tag", [
        ("handle_failed_event", "failed_event_1.json", "1_expected.json", "data"),
    ]
)
def test_handle_failed_event(directory, create_event_from_json, event_filename,
                             generate_expected, expected_filename, event_data_tag):
    with patch('main.bigquery.Client') as mock_bigquery:
        with patch.object(mock_bigquery, 'insert_rows_json', None):
            mock_bigquery.return_value = bigquery.Client()
            # data = base64.b64decode(copy.deepcopy(create_event_from_json)['data'].decode('UTF-8')).decode('UTF-8')
            main.handle_failed_event(create_event_from_json, generate_context)
