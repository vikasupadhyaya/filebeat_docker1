"""
main.py contains the main processing logic
"""
import json
import copy
import os
import base64
import logging

from google.cloud import bigquery
from tt_common.tt_logging import Logging

project = os.getenv("PROJECT")
Logging().init_logging()
logger = logging.getLogger(__name__)


def get_config():
    return {
        "tables": [
            {
                "table_name": "{}.edc_inboundconverter.inbound_converter_failures",
                "columns": [
                    "LOGSTASH_ID", "ACTIVITY_TYPE_ID", "VERSION", "CID", "REF_PARAM", "REFERER", "USER_AGENT",
                    "REGION_CD", "ACTIVITY_DATE", "EU_ALLOWED", "DOMAIN", "ACTIVITY_MILLIS", "IS_ISP",
                    "DATE_CREATED", "DATE_MODIFIED", "PAGE_TYPE", "PAGE_TITLE", "PAGE_TEXT_COUNT", "ERRORS"
                ],
                "enabled": True
            }
        ]
    }


def handle_failed_event(event, context):
    # pylint: disable=unused-argument
    """
    Cloud Function to load to BQ
    :param event: (dict) Dictionary that contains both the configuration and the post-processed data from IBC.
    :param context: (google.cloud.functions.Context) The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time
    :return: None
    """

    data = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    bq_client = bigquery.Client()
    config = get_config()
    tables = config.get('tables', [])
    for table in tables:
        if table.get("enabled", True):
            load_data = []
            filtered_rec = filter_columns(data=data, columns=table.get('columns'))
            if filtered_rec != {}:
                load_data.append(filtered_rec)
                if "ERRORS" in filtered_rec:
                    filtered_rec["ERRORS"] = json.dumps(filtered_rec["ERRORS"])

            if load_data:
                logger.info('\nabout to load data into table: ' + table['table_name'].format(project) + '; data: ' +
                      json.dumps(load_data))
                errors = bq_client.insert_rows_json(table['table_name'].format(project), load_data)

                if errors:
                    logger.error('\nFailed to insert data for a failed event into BigQuery: ' + table['table_name'].format(
                        project) + '-' +
                          json.dumps(load_data) + '-' + json.dumps(errors))


def filter_columns(data, columns):
    """
    Rules are based on the original keys based in the dictionary. We dont want to load all those keys so we need a way
    to reduce the number of columns in any input to fit the format of the table
    :param data: (dict) input dictionary to be used for processing
    :param columns: (list) List of all columns to keep
    :return: (dict) resultant object with only the columns to be loaded into BQ
    """

    filter_data = copy.deepcopy(data)
    if columns is not None and data != {}:
        filter_data = {}
        for key in columns:
            if key in data:
                filter_data[key] = data[key]

    return filter_data
