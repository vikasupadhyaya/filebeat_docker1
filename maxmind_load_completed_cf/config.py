"""
Centralized location for configuration options specific to the maxmind processing.

Startup Parameters
PROJECT - stored as GCP_PROJECT in cf_config dictionary
CF_ENV - Necessary when deployed as cloud function with value True
       - to control whether cloud storage or local file access is used


Common configuration options are expected to be present in config based dictionary definitions
for passing to common routines that take in a config object for operation.
GCP_PROJECT - the GCP Project name
LOCAL_DEV - boolean - false will retrieve data files from cloud storage

Additional Configuration properties:
CONFIG_COLLECTION_NAME - root firestore collection which contains cloud function configurations
DATA_LOADS_COLLECTION_NAME - collection name of maxmind configurations to hold data loads
SHARDS_COLLECTION_NAME - collection name containing the partition documents
MAXMIND_DOC_ID - document identifier containing the maxmind configuration statuses
COLLECTION_PREFIX - a prefix added to a maxmind revision to arrive at final collection name
"""
import os

cf_config = {}


def innovation_configs():
    """
    Example of overriding and setting explicit configuration values
    such as when in development mode and accessing the innovation
    GCP environment and a local microservice

    :return: no return value
    """
    global cf_config

    cf_config["GCP_PROJECT"] = "tt-temp-2021030444"
    cf_config["LOCAL_DEV"] = True


def init_configs():
    """
    The default implementation of configuration process to initialize
    the configurations
    """
    global cf_config
    cf_config = {
        "GCP_PROJECT": os.getenv("PROJECT"),
        "LOCAL_DEV": True,
        "CONFIG_COLLECTION_NAME": "ibc-cf-configurations",
        "DATA_LOADS_COLLECTION_NAME": "data-loads",
        "SHARDS_COLLECTION_NAME": "shards",
        "MAXMIND_DOC_ID": "maxmind-config",
        # The prefix added to a maxmind revision to name the collection
        "COLLECTION_PREFIX": "maxmind-"
    }

    in_cloud_env = os.getenv("CF_ENV", "False").lower() == "true"
    cf_config["LOCAL_DEV"] = not in_cloud_env


def get_config_value(key):
    """
    Retrieve configuration value with specified key.
    Use a method instead of referring to variable directly in other
    modules since changes to entries are not reflected after initial inclusion
    in other files.  eg. init, change, lookup uses value from init not change

    :param key: to obtain value of
    :return: type varies by key
    """
    return cf_config.get(key)


init_configs()
