"""
Manual tests used during development

Additional manual steps may be needed for certain tests to initialize the firestore
collection used by tests.
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
import helpers.shards_helper as shards_helper

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test with setup needed")
def test_is_collection_load_complete():
    """
    Requires setup to add/remove handled and expected batche ids
    to test the different scenarios.
    1) add to expected_batch_ids in a shard to include 9999999999
       - expect not complete status
    2) have expected_batch_ids and handled_batch_ids contain same values
       - expect complete status
    3) use different expected_record_count values

    :return: None
    """
    config.innovation_configs()
    data_revision = "Nov-2019-10k"
    expected_record_count = 9999
    is_complete = shards_helper.is_collection_load_complete(data_revision, expected_record_count)
    logger.info("Result of complete check: {}".format(is_complete))
