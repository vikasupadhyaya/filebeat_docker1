"""
Unit tests for shards_helper
"""
import pytest
import logging
import helpers.shards_helper as shards_helper
from tt_common.tt_logging import Logging

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "data_revision, expected_id", [
        ("Nov-2019", "ibc-cf-configurations/maxmind-config/data-loads/Nov-2019/shards"),
        ("Feb-2020", "ibc-cf-configurations/maxmind-config/data-loads/Feb-2020/shards")
    ]
)
def test_get_shards_collection_id(data_revision, expected_id):
    """
    Verify construction of shards collection id, with expectation that the configuration values
    are as follows: (changes would have wider impact on other cloud functions since the base structure
    provides access to the maxmind-config document that is used to indicate active collection)
    "CONFIG_COLLECTION_NAME": "ibc-cf-configurations"
    "DATA_LOADS_COLLECTION_NAME": "data-loads"
    "SHARDS_COLLECTION_NAME": "shards"
    "MAXMIND_DOC_ID": "maxmind-config"

    :param (str) data_revision: to include in path construction
    :param (str) expected_id: expected path to shard
    :return: None
    """
    doc_id = shards_helper.get_shards_collection_id(data_revision)
    assert doc_id == expected_id
