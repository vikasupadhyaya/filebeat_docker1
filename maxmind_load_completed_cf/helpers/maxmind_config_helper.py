"""
Provide routines for managing the maxmind configurations.

The overview of collection and document structure is:
ibc-cf-configurations/maxmind-config/data-loads/<data_revision>/shards/<0-99>

ibc-cf-configurations - the root collection of inbound converter configurations
maxmind-config - the document containing:
                  active_collection - the collection which cloud functions should be using
                  staging_collection - a collection being populated that will eventually become the active_collection
data-loads - collection of quarterly files so we can track when and what was loaded
<data_revision> - document for each quarterly file loaded containing:
                  staging_status - progress of state of data load
                  record_count - number of configurations in main data file, and expected to match load count in shards
                  last_updated - timestamp of last change
                  created_timestamp - timestamp of when record was initially created
shards - collection to contain documents to allow for more efficient tracking of written records
         based on firestore distributed counters strategy, with additional detail for determining load failures
<0-99> - one hundred documents, each containing a portion of the beginning record ip addresses and tracking details:
         handled_batch_ids - array - starting ip address of first record in batch that has been written
         expected_batch_ids - array - starting ip address of first record in batch from all records in data file that
                                      would is expected to be associated and tracked in the specific shard instance
        records_written - int - total number of written documents (if same data set is manually fed into system for
                                loading, the documents will be overwritten and this count indicating a higher value
                                than the actual number of documents present in collection when summed
        last_updated - timestamp of last change to the shard instance
"""
import logging
from google.cloud import firestore
from config import get_config_value

# Initialize logging
logger = logging.getLogger(__name__)


def get_revision_from_collection_name(collection_name):
    """
    Obtain the data revision component from a collection name.
    We prefix 'maxmind-' to a revision to easily identify the collection contents at root level.

    :param (str) collection_name: of maxmind collection that is being checked
    :return: (str) - the data revision
    """
    temp_coll_name = str(collection_name)
    prefix = get_config_value("COLLECTION_PREFIX")
    data_revision = temp_coll_name
    if temp_coll_name.startswith(prefix):
        data_revision = temp_coll_name[len(prefix):]

    return data_revision


def get_maxmind_config_doc_id():
    """
    Use the values from the cloud function configuration instance to construct a firestore
    document path to the maxmind configuration document.

    Uses the following configuration keys in construction of path value:
    CONFIG_COLLECTION_NAME
    MAXMIND_DOC_ID

    :return: (str): a firestore document id, with full path information
    """
    return str(get_config_value("CONFIG_COLLECTION_NAME")) + "/" + str(get_config_value("MAXMIND_DOC_ID"))


def get_maxmind_revision_doc_id(data_revision):
    """
    Use the values from the cloud function configuration instance to construct a firestore
    document path to the data_revision document which contains expected record count for full load.

    Uses the following configuration keys in construction of path value:
    CONFIG_COLLECTION_NAME
    MAXMIND_DOC_ID
    DATA_LOADS_COLLECTION_NAME

    :param: (str) data_revision: the revision to include as part of document id
    :return: (str): a firestore document id, with full path information
    """
    return get_maxmind_config_doc_id() + "/" + str(get_config_value("DATA_LOADS_COLLECTION_NAME")) + "/" + \
        str(data_revision)


def update_active_maxmind_collection(new_active_collection):
    """
    Update the configuration used by other cloud functions to specify which maxmind collection
    name should be used when retrieving maxmind related information.

    :param (str) new_active_collection: a collection confirmed to be fully loaded and should be promoted
    :return: None
    """
    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    maxmind_config_doc_ref = fs_client.document(get_maxmind_config_doc_id())
    maxmind_config = maxmind_config_doc_ref.get().to_dict()
    # use bracket access because we want to fail if this property is not present
    current_active_collection = maxmind_config["active_collection"]
    current_staging_collection = maxmind_config["staging_collection"]

    if current_staging_collection != new_active_collection:
        logger.info(("ERROR - Collection NOT promoted.  Collection provided to promote is not the staging " +
                    "collection.  Active: '{}'; Staging: '{}'; Collection provided to promote: '{}'").format(
                        current_active_collection, current_staging_collection, new_active_collection
                    ))
        return

    if new_active_collection == current_active_collection:
        logger.info("Provided new collection is the same as active collection - no update performed.")
        return
    else:
        maxmind_config_doc_ref.update({
            "active_collection": new_active_collection,
            "previous_active_collection": current_active_collection,
            "active_collection_last_updated": firestore.SERVER_TIMESTAMP,
            "last_updated": firestore.SERVER_TIMESTAMP
        })


def get_staging_collection_for_completion_check():
    """
    Obtain the staging collection name that needs to be checked for completion to become new
    active collection.  If active collection and staging is the same, then nothing to check.

    :return: (str) - staging collection name to check; None if no pending promotion available
    """
    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    maxmind_config_doc = fs_client.document(get_maxmind_config_doc_id()).get()

    if maxmind_config_doc.exists:
        maxmind_config = maxmind_config_doc.to_dict()
        # use bracket access because we want to fail if these properties are not present
        active_collection = maxmind_config["active_collection"]
        staging_collection = maxmind_config["staging_collection"]
        if active_collection == staging_collection:
            return None
        return staging_collection
    else:
        raise Exception("Maxmind configuration document not found")


def get_maxmind_revision_expected_count(data_revision):
    """
    Obtain the number of records which are expected to be written into firestore collection
    for the specified data_revision as stored in the revision document.

    :param: (str) data_revision: the revision to obtain record_count from
    :return: (int): number of records expected to be written when fully loaded
    """
    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    maxmind_revision_doc = fs_client.document(get_maxmind_revision_doc_id(data_revision)).get().to_dict()

    # use bracket access because we want to fail if property is not present
    return int(maxmind_revision_doc["record_count"])
