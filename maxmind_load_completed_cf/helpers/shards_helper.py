"""
Provide common routines for dealing with shards, which are individual documents used
to collect information for a batch of records to provide more efficient handling of
tracking and counts of inserts since a single document write has limited change rate of
once per second that is not suitable for the volume of data being loaded in most cases.
"""
import logging
from google.cloud import firestore
from config import get_config_value

# Initialize logging
logger = logging.getLogger(__name__)


def get_shards_collection_id(data_revision):
    """
    Use the values from the cloud function configuration instance to construct a firestore
    collection path to the data_revision shards collection.

    Uses the following configuration keys in construction of path value:
    CONFIG_COLLECTION_NAME
    MAXMIND_DOC_ID
    DATA_LOADS_COLLECTION_NAME
    SHARDS_COLLECTION_NAME

    :param (str) data_revision: to include in path of shard document reference
    :return: (str): a firestore collection id, with full path information
    """
    return str(get_config_value("CONFIG_COLLECTION_NAME")) + "/" + str(get_config_value("MAXMIND_DOC_ID")) + \
        "/" + str(get_config_value("DATA_LOADS_COLLECTION_NAME")) + "/" + str(data_revision) + "/" + \
        str(get_config_value("SHARDS_COLLECTION_NAME"))


def is_collection_load_complete(data_revision, expected_record_count):
    """
    Perform check to determine if the maxmind_collection load has completed by comparing the
    details in the shards sub-collection of the revision.  The revision is after 'maxmind-' prefix.

    :param (str) data_revision: corresponding to the collection being checked - such as Nov-2019 from maxmind-Nov-2019
    :param (int) expected_record_count: number of records expected to be written
    :return: (boolean) - True if maxmind_collection is determined to be fully loaded; False otherwise
    """
    shards_collection_id = get_shards_collection_id(data_revision)
    logger.info("Performing check on load completion for revision '{}' on collection '{}'".format(data_revision,
                                                                                                  shards_collection_id))
    fs_client = firestore.Client(get_config_value("GCP_PROJECT"))
    shards = fs_client.collection(shards_collection_id).list_documents()

    total_records_written = 0
    all_loaded_batches = True
    for current_shard in shards:
        shard = current_shard.get().to_dict()
        total_records_written += int(shard.get("records_written", 0))
        if all_loaded_batches:
            # only need to iterate additional shards expected and handled
            # if we have not already found a mismatch. continue with iteration
            # to gather counts as final check
            expected_batch_ids = shard.get("expected_batch_ids", [])
            handled_batch_ids = set(shard.get("handled_batch_ids", []))
            for current_expected in expected_batch_ids:
                if current_expected not in handled_batch_ids:
                    logger.info("Found a missing batch id: {}; in shard {}".format(current_expected, shard))
                    all_loaded_batches = False
                    break

    all_records_written = total_records_written >= expected_record_count
    load_complete = all_loaded_batches and all_records_written
    logger.info("Revision '{}' - expected records: {}; written records: {}".format(
        data_revision, expected_record_count, total_records_written))
    logger.info("Complete check for revision '{}': {} (all batches: {}; all records written: {})".format(
        data_revision, load_complete, all_loaded_batches, all_records_written))
    return load_complete
