"""
Determine if a Maxmind load has completed by checking all shards
for any remaining differences from expected batch ids and handled batch ids.

First check is to see if we are still pending a publish of latest staging collection to
become active.  If staging and active are the same, then no further checks needed.
"""
import json
import base64
import logging
import helpers.maxmind_config_helper as maxmind_config_helper
import helpers.shards_helper as shards_helper
from tt_common.tt_logging import Logging
from config import init_configs

"""
For local testing and development, use pympler for logging object sizes
from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize the configurations for process
init_configs()

# Initialize logging
Logging().init_logging()
logger = logging.getLogger(__name__)

# Informational detail to determine re-use of instance
execution_count = 0


def process_event(received_event_param, context):
    """
    Perform the check and promotion of a complete staging collection, provided
    that option to skip the promotion is not present in event.

    :param received_event_param: message from pubsub or direct test
    :param context: cloud function context
    :return: None
    """
    global execution_count
    execution_count += 1
    logger.info("Function instance execution count: {}".format(execution_count))

    try:
        received_event = dict() if not received_event_param else received_event_param
        if "data" in received_event:
            event = json.loads(base64.b64decode(received_event['data']).decode('utf-8'))
        elif "attributes" in received_event:
            event = json.loads(json.dumps(received_event['attributes']))
        else:
            event = dict()

        """
        Use 'get' style access in case we do not receive a value from legitimate message
        structure we can fall back to full flow.
        """
        override_mode = event.get("MODE", "Full")
        do_not_promote_collection = override_mode == "DO_NOT_PROMOTE"
        check_collection_only = override_mode == "CHECK_COLLECTION"

        logging.info("Processing event: {}".format(event))

        if check_collection_only:
            # use bracket access to fail if not present
            do_not_promote_collection = True
            staging_collection_to_check = event["COLLECTION"]
        else:
            staging_collection_to_check = maxmind_config_helper.get_staging_collection_for_completion_check()

        if staging_collection_to_check is not None:
            data_revision = maxmind_config_helper.get_revision_from_collection_name(staging_collection_to_check)
            expected_record_count = maxmind_config_helper.get_maxmind_revision_expected_count(data_revision)
            load_completed = shards_helper.is_collection_load_complete(data_revision, expected_record_count)
            if load_completed:
                if not do_not_promote_collection:
                    logger.info("Promoting active Maxmind version to '{}'".format(staging_collection_to_check))
                    maxmind_config_helper.update_active_maxmind_collection(staging_collection_to_check)
                else:
                    logger.info("Not performing promotion of Maxmind version due to override in event.")
            else:
                logger.info("Staging load is not complete for version '{}'".format(staging_collection_to_check))
        else:
            logger.info("No pending Maxmind staging collection to check - active and staging equal")

    except Exception as e:
        logger.error("Fatal error checking Maxmind load completion.  Error: {}".format(str(e)))
