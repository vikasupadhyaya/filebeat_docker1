#
# command to deploy in innovation project for testing
#
gcloud functions deploy storage_configs_cf \
--project tt-temp-2021030444 \
--region=us-east4 \
--runtime python37 \
--service-account tt-ibc-innovation@tt-temp-2021030444.iam.gserviceaccount.com \
--source=. \
--trigger-resource ibc-configurations-innovation \
--trigger-event google.storage.object.finalize \
--entry-point handle_storage_event \
--stage-bucket ibc-client-activity-input-innovation \
--memory=128MB \
--timeout=60 \
--retry \
--max-instances=1 \
--set-env-vars ENV=tt-temp-2021030444,PROJECT=tt-temp-2021030444,CF_ENV=True,STORAGE_BUCKET=ibc-configurations-innovation \
--update-labels=tt-component=cloud-function,tt-environment=temp,tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi

