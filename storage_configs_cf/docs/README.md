# Testing

Add a file to monitored cloud storage bucket.

For unit testing, providing an event and context is necessary since we use age of event in flow.

Event structure (only used properties listed):
```
{
    "bucket": "ibc-configurations-innovation",
    "name": "kickfire/filename-revision.csv"
}
```

* For Kickfire loads, the files are expected to be in a kickfire folder
* For Maxmind loads, the files are expected to be in a maxmind folder

* The context is an instance of google.cloud.functions.Context - see https://cloud.google.com/functions/docs/writing/background
