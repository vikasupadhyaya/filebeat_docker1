"""
Entry point for determining action to take on cloud storage addition/change.
"""
import logging
import helpers.kickfire_helper as kickfire_helper
import helpers.maxmind_helper as maxmind_helper
from datetime import datetime, timezone
from tt_common.tt_logging import Logging
from lib.pubsub import publish_events_to_topic
from lib.manage_futures import wait_for_futures_to_complete
from config import init_configs
from config import get_config_value
from dateutil import parser

# Initialize the configurations for process
init_configs()

# Initialize logging
Logging().init_logging()
logger = logging.getLogger(__name__)

# Informational detail to determine re-use of instance
execution_count = 0

# Folder roots of supported configuration files
kickfire_root = "kickfire/"
maxmind_root = "maxmind/"


def ignore_aged_event(event, context):
    """
    Determine if the age of the event is older than the configured value to prevent
    continuous processing since we retry failures.

    :param (dict) event: containing details of file added
    :param (google.cloud.functions.Context) context: of invocation containing event time
    :return: (boolean) - True if event is to be discarded; False to process
    """
    ignore_event = False
    try:
        timestamp = context.timestamp
        event_time = parser.parse(timestamp)
        event_age = (datetime.now(timezone.utc) - event_time).total_seconds()
        file_name = event["name"]
        if event_age > int(get_config_value("MAX_SECONDS_EVENT_AGE")):
            logging.info(
                "File '{}' event is being dropped due to age. (age of event: {} seconds)".format(
                    file_name, event_age))
            ignore_event = True
    except Exception as e:
        logging.error("Problem in age of event check - aborting and not retrying. {}".format(str(e)))
        ignore_event = True

    return ignore_event


def publish_messages(publish_to_topic, messages):
    """
    Publishes and waits for all futures to complete before returning.

    :param (str) publish_to_topic: name of topic
    :param (list) messages: list of messages to publish
    :return: None
    """
    if not messages:
        logging.debug("No messages to publish.")
        return

    logger.info("Publishing {} messages to topic {}: {}".format(len(messages), publish_to_topic, messages))
    publish_events_to_topic(messages, get_config_value("GCP_PROJECT"), publish_to_topic)

    wait_for_futures_to_complete()


def handle_storage_event(event, context):
    """
    Handle the received event from a cloud storage file addition/change.

    :param (dict) event: containing details of file added
    :param (google.cloud.functions.Context) context: of invocation containing event time
    :return: None
    """
    global execution_count
    execution_count += 1
    logger.info("Function instance execution count: {}".format(execution_count))

    try:
        if ignore_aged_event(event, context):
            return

        file_name = str(event.get("name"))
        bucket = str(event.get("bucket"))

        """
        Use 'get' style access in case we do not receive a value from legitimate message
        structure we can fall back to begin processing and create batch records
        with obtained revision from storage location.
        """
        logging.info("Handling storage event: {}".format(event))

        publish_to_topic = None
        messages = []
        if file_name.startswith(kickfire_root):
            # delegate to kickfire helper
            messages = kickfire_helper.get_messages_for_publishing(bucket, kickfire_root, file_name)
            publish_to_topic = get_config_value("START_KICKFIRE_LOAD_TOPIC")
        elif file_name.startswith(maxmind_root):
            # delegate to maxmind helper
            messages = maxmind_helper.get_messages_for_publishing(bucket, maxmind_root, file_name)
            publish_to_topic = get_config_value("START_MAXMIND_LOAD_TOPIC")
        else:
            logging.info("No action taken on file: {}".format(file_name))

        if messages:
            if publish_to_topic:
                publish_messages(publish_to_topic, messages)
                wait_for_futures_to_complete()
            else:
                logging.error("ERROR - no topic specified for obtained messages.  Discarding Event: {}".format(event))

    except Exception as e:
        logger.error("Raising Error to retry storage event: {}".format(str(e)))
        # we re-raise problem to retry message
        # initial check of event age as entry condition will stop any further handling
        raise e
