# Description

This cloud function is triggered by file additions and changes (finalized events) within a cloud storage bucket.

The bucket is used for receiving configuration related data and when key files are added, a pub/sub message is published to being a loading process.

## Kickfire
The Kickfire data set is comprised of three data files on a quarterly basis:
* `twinranges-<revision>.csv` - large data file of domains and ip addresses
* `ispfilter-<revision>.csv` - the domains which are isps
* `wififilter-<revision>.csv` - the domains which are wifi based

The naming convention must conform to the above since the loading process looks for these files and also is used for a verification step before publishing the load message.
