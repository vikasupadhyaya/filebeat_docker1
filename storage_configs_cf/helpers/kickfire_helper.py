"""
Entry point for determining action to take on cloud storage addition/change.
"""
import os
import logging
import time
import helpers.string_helper as string_helper
from google.cloud import storage
from config import get_config_value

# Initialize logger
logger = logging.getLogger(__name__)

# The kickfire configuration files we require
# The data_file is largest and the one we base the start of loading on when it is created
# and the other two are present.
data_file_prefix = "twinranges-"
isp_file_prefix = "ispfilter-"
wifi_file_prefix = "wififilter-"

# The number of seconds we pause before raising exception to retry when all data files are not present
# The data file is the largest and should be last received, but rather than immediate failure to retry, we
# pause a bit to allow for other files to be uploaded.
delay_seconds_before_retry = 30


def get_revision_from_filename(file_name):
    """
    Obtain the revision from file_name, which is the quarter name of data to load.

    :param (str) file_name: example twinranges-Q1-2021.csv; must be twinranges- prefixed filename
    :return (str) revision: extracted from file_name
    """
    suffix = ".csv"
    extracted_revision = file_name
    if extracted_revision.endswith(suffix):
        extracted_revision = extracted_revision[:-len(suffix)]
    if extracted_revision.startswith(data_file_prefix):
        extracted_revision = extracted_revision[len(data_file_prefix):]

    clean_revision = extracted_revision.strip()

    if not clean_revision or clean_revision != extracted_revision:
        logger.error(("Error - revision must be specified and may not contain leading or trailing " +
                     "whitespace: '{}'").format(extracted_revision))
        clean_revision = ""

    return clean_revision


def create_kickfire_load_record(data_revision):
    """
    Create message to begin the load process of a kickfire data file.

    :param (str) data_revision: that created messages will specify as source file to process
    :return: (dict) - pubsub message
    """
    record = {
        "REVISION": str(data_revision)
    }
    return record


def all_configuration_files_present(storage_bucket_name, folder, data_revision):
    """
    Perform the check that the isp and wifi files are present for data revision we are attempting to load.

    :param (str) storage_bucket_name: containing configuration files
    :param (str) folder: that is expected to contain the configuration files
    :param (str) data_revision: of files that are expected
    :return: True if configuration files present; False otherwise
    """
    gcp_project = get_config_value("GCP_PROJECT")
    retrieve_file_prefix = folder

    isp_file_name = isp_file_prefix + data_revision + ".csv"
    wifi_file_name = wifi_file_prefix + data_revision + ".csv"

    required_files = {isp_file_name, wifi_file_name}
    present_files = 0

    logger.info("Starting check for required configuration files: {}".format(required_files))
    storage_client = storage.Client(project=gcp_project)
    blobs = list(
        storage_client.list_blobs(bucket_or_name=storage_bucket_name, prefix=retrieve_file_prefix, delimiter="/"))

    for blob in blobs:
        remote_file_path, remote_file_name = os.path.split(blob.name)
        if remote_file_name in required_files:
            logger.info("Required file is present: {}".format(remote_file_name))
            present_files += 1

    if present_files == len(required_files):
        return True
    else:
        return False


def get_messages_for_publishing(bucket, trigger_folder, event_file_name):
    """
    Check if the necessary configuration files are present, returning a pub/sub message
    to begin kickfire load if so.  Since we need multiple configuration files to perform load,
    we wait for the largest file to begin checks on all 3, raising exception to retry if
    all 3 are not present.
    
    :param (str) bucket: containing configuration files
    :param (str) trigger_folder: that is expected to contain the configuration files
    :param (str) event_file_name: the name of file added in trigger_folder
    :return: (list) messages: to be published to pub/sub to begin kickfire load
    """
    messages = []

    file_name = string_helper.remove_prefix(trigger_folder, str(event_file_name))
    if file_name.startswith(data_file_prefix) and file_name.endswith(".csv"):
        data_revision = get_revision_from_filename(file_name)
        if not data_revision:
            logger.info("No revision obtained from file name.  No action taken.")
            return messages
        if all_configuration_files_present(bucket, trigger_folder, data_revision):
            logger.info(("All required configuration files present. " +
                         "Creating message to initiate kickfire load process for revision {}").format(data_revision))
            messages.append(create_kickfire_load_record(data_revision))
        else:
            logger.info(("All configuration files for starting load process not present.  " +
                         "Retrying file {} after pause.").format(event_file_name))
            time.sleep(delay_seconds_before_retry)
            raise Exception("All configuration files not present.  Retry event.")
    else:
        logging.info("No action taken at this time on file: {}".format(event_file_name))

    return messages
