"""
Helper for string manipulation.
"""


def remove_prefix(prefix, from_value):
    """
    Remove the full prefix from provided from_value if present.

    :param (str) prefix: to remove from from_value
    :param (str) from_value: to remove prefix from
    :return: (str): the from_value with prefix removed if present; otherwise from_value
    """
    return_value = from_value
    if from_value.startswith(prefix):
        return_value = from_value[len(prefix):]
    return return_value
