"""
Unit tests for string_helper
"""
import pytest
import logging
import helpers.string_helper as string_helper
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "prefix, from_value, expected", [
        ("kickfire/", "kickfire/kick123", "kick123"),
        ("kickfire/", "kickfire123", "kickfire123"),
        ("kickfire/", "kickfire/filename", "filename")
    ]
)
def test_remove_prefix(prefix, from_value, expected):
    return_value = string_helper.remove_prefix(prefix, from_value)
    assert return_value == expected
