"""
Manual tests for kickfire_helper
"""
import pytest
import logging
import config
import helpers.kickfire_helper as kickfire_helper
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test requiring special setup")
def test_all_configuration_files_present():
    config.innovation_configs()
    storage_bucket_name = "ibc-configurations-innovation"
    folder = "kickfire/"
    data_revision = "Q2-2021-2m"
    all_present = kickfire_helper.all_configuration_files_present(storage_bucket_name, folder, data_revision)
    logger.info("isp and wifi files present for revision '{}' - {}".format(data_revision, all_present))


@pytest.mark.parametrize(
    "event_file_name, expected, revision", [
        ("kickfire/twinranges-Q2-2021-2m-v2.csv", 1, "Q2-2021-2m-v2"),
        ("kickfire/ispfilter-Q2-2021-2m-v2.csv", 0, ""),
        ("kickfire/wififilter-Q2-2021-2m-v2.csv", 0, ""),
        ("kickfire/subfolder/twinranges-Q2-2021-2m-v2.csv", 0, "")
    ]
)
@pytest.mark.skip(reason="manual test requiring special setup")
def test_get_messages_for_publishing(event_file_name, expected, revision):
    config.innovation_configs()

    storage_bucket_name = "ibc-configurations-innovation"
    folder = "kickfire/"

    messages = kickfire_helper.get_messages_for_publishing(storage_bucket_name, folder, event_file_name)
    logger.info("obtained messages: {}".format(messages))
    assert len(messages) == expected
    if expected > 0:
        assert messages[0].get("REVISION") == revision
