"""
Unit tests for kickfire_helper
"""
import pytest
import logging
import helpers.kickfire_helper as kickfire_helper
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "file_name, expected", [
        ("twinranges-Q2-2020-T1.csv", "Q2-2020-T1"),
        ("tQ2-2020-T1.csv", "tQ2-2020-T1"),
        ("twinranges-  .csv", ""),
        ("twinranges- 1 .csv", "")
    ]
)
def test_get_revision_from_filename(file_name, expected):
    """
    Test extraction of revision from a file_name.
    Was determined that rstrip and lstrip do not do what was intended on removing the full string.
    Any character appearing in value to lstrip or rstrip was removed which was not correct.
    Modified to remove only the prefix and suffix if present.

    :param (str) file_name:
    :param (str) expected:
    :return: None
    """
    revision = kickfire_helper.get_revision_from_filename(file_name)
    assert revision == expected


@pytest.mark.parametrize(
    "data_revision", [
        ("Q2-2020-T1"),
        ("Nov-2019-manual")
    ]
)
def test_create_kickfire_load_record(data_revision):
    message = kickfire_helper.create_kickfire_load_record(data_revision)
    assert message.get("REVISION") == data_revision
