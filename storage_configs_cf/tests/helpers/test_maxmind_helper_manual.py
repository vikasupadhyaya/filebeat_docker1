"""
Manual tests for maxmind_helper
"""
import pytest
import logging
import config
import helpers.maxmind_helper as maxmind_helper
from tt_common.tt_logging import Logging

"""
Not part of requirements of cloud function and used during development to obtain object size

from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))
"""

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


@pytest.mark.skip(reason="manual test requiring special setup")
def test_all_configuration_files_present():
    config.innovation_configs()
    storage_bucket_name = "ibc-configurations-innovation"
    folder = "maxmind/"
    data_revision = "Nov-2019-manual-test"
    all_present = maxmind_helper.all_configuration_files_present(storage_bucket_name, folder, data_revision)
    logger.info("required maxmind files present for revision '{}' - {}".format(data_revision, all_present))


@pytest.mark.parametrize(
    "event_file_name, expected, revision", [
        ("maxmind/GeoIP2-City-Blocks-IPv4-Nov-2019-manual-test-v1.csv", 1, "Nov-2019-manual-test-v1"),
        ("maxmind/GeoIP2-City-Locations-en-Nov-2019-manual-test-v1.csv", 0, ""),
        ("maxmind/someotherfile-Nov-2019-manual-test-v1.csv", 0, ""),
        ("maxmind/subfolder/GeoIP2-City-Blocks-IPv4-Nov-2019-manual-test-v1.csv", 0, "")
    ]
)
@pytest.mark.skip(reason="manual test requiring special setup")
def test_get_messages_for_publishing(event_file_name, expected, revision):
    """
    Manual setup to have maxmind/<files>-Nov-2019-manual-test-v1.csv files present.

    :param event_file_name:
    :param expected:
    :param revision:
    :return: None
    """
    config.innovation_configs()

    storage_bucket_name = "ibc-configurations-innovation"
    folder = "maxmind/"

    messages = maxmind_helper.get_messages_for_publishing(storage_bucket_name, folder, event_file_name)
    logger.info("obtained messages: {}".format(messages))
    assert len(messages) == expected
    if expected > 0:
        assert messages[0].get("REVISION") == revision
