"""
Unit tests for config
"""
import pytest
import config
import logging
from tt_common.tt_logging import Logging
from config import get_config_value

# from pympler.asizeof import asizeof
# logging.info("{} records size: {} MB".format(len(messages), asizeof(messages) / 1000000))

# Initialize logging
Logging().init_logging()
# log_record_format = "%(asctime)s %(levelname)8s: %(message)s (%(name)s)"
# logging.basicConfig(level=logging.INFO, format=log_record_format, force=True)

logger = logging.getLogger(__name__)


def test_config_timeout():
    """
    Verify age of event is expected value

    :return: None
    """
    config.innovation_configs()
    timeout = get_config_value("MAX_SECONDS_EVENT_AGE")
    assert timeout == 1800
