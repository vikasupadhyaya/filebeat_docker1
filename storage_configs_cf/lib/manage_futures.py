"""
Routines for managing futures.

Any process which adds a future should call the
wait_for_futures_to_complete function prior to exiting to
ensure that all callbacks have completed before exiting
process.  This is especially important in cloud function
deployments since the cloud function may stop receiving cpu
cycles, but if same instance is restarted, those futures would
then complete which may lead to obscure logging or event tracking.
"""
import time
import logging

logger = logging.getLogger(__name__)
futures = dict()


def get_callback(future_id, action_description):
    """
    Obtain a callback routine that will be called when the
    future action completes.

    :param future_id: unique identifier to coincide with action associated with future
    :param action_description: such as 'publishing'
    :return: callback
    """

    def callback(future):
        try:
            futures.pop(future_id)
        except Exception as e:
            err_str = str(e)
            logger.error("Fatal exception: {}; while {} for {}.".format(err_str, action_description, future_id))

    return callback


def add_future(future_id, future, action_description):
    """
    Perform the steps to add future to pending completion list
    and associate a callback to the future for cleaning up.

    :param future_id: unique identifier of future
    :param future: to create callback for
    :param action_description: one or two words string describing what
           was done to create the future
    :return: no return value
    """
    futures.update({future_id: None})
    futures[future_id] = future
    future.add_done_callback(get_callback(future_id, action_description))


def wait_for_futures_to_complete():
    """
    Block until all futures are completed

    :return: no return value
    """
    while True:
        num_futures_left = len(futures)
        if num_futures_left == 0:
            break

        logger.info("Waiting for all futures to complete: {}".format(num_futures_left))
        """
        wait 50ms since cloud function pricing is billed in 100ms
        increments - otherwise we are waiting and paying when we could
        otherwise just exit
        """
        time.sleep(0.05)
