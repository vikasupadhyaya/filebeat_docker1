Product: gcp_inboundconverter
Release version: TBD
Author: Jeff Brown <jkbrown@techtarget.com>
Last Updated: Septempber 1, 2021
Created: TBD
Release Date: TBD
Project Name: Inbound Converter Cloud Functions
Project Number:

Release Approved by QA: TBD

Business Owner:

Project Manager:
    Winston Tamisin <wtamisin@techtarget.com>

Developers:
    Jeff Brown <jkbrown@techtarget.com>
    Marina Popova <mpopova@techtarget.com>
    Evelyn Munkel <emunkel@techtarget.com>

QA resources:
    Alex Thornton-Clark <athorntonclark@techtarget.com>
    Kanaka Sreepada <ksreepada@techtarget.com>
    Gregory Paley <GPaley@techtarget.com>

Release Engineer:
    Jeff Brown <jkbrown@techtarget.com>
    Marina Popova <mpopova@techtarget.com>
    Kanaka Sreepada <ksreepada@techtarget.com>

====================================================================
Impact Statement:

====================================================================
Changes to product:

    1) 

====================================================================
All features and defects:

    * 
    

====================================================================
Pre-Release Steps:

    - 


====================================================================
Release Steps:

    1) Deploy cloud functions
       http://jenkinsci.techtarget.com/job/edc-activity/job/ibc/
       and then selection "ibc-deploy-cloud-functions"
       - Choose the following cloud functions for deployment
       -- Do not select "Execute Tests"
       -- Select:
       --- 
       
    
====================================================================
Post-Release Steps:
    1) 

====================================================================
Changes to system:
    - None

====================================================================
Changes to script server:
    - None

====================================================================
Changes to database:
    - None

====================================================================
Changes to build/install:
    - None
