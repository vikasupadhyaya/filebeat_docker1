from unittest.mock import patch

from main import process_webscrape_event


def get_method_info(topic):
    topic_id = topic.split('/')[-1]
    topic_info = {
        "ibc.events-to-webscrape": {"function": process_webscrape_event, "config": "WebScraper"}
    }

    method = topic_info[topic_id]

    return method


def get_libs_to_patch(topic, **kwargs):

    topic_id = topic.split('/')[-1]
    topic_info = {
        "ibc.events-to-webscrape": [
            {"lib": 'main.CONFIG', "new": kwargs['config']}
        ]
    }

    libs_to_patch = topic_info[topic_id]

    return libs_to_patch


def patch_libs(topic, libs, patched={}):

    if not patched.get(topic):
        patched[topic] = []
    for lib in libs:
        if 'new' in lib:
            patcher = patch(lib['lib'], lib['new'])
        else:
            patcher = patch(lib['lib'])
        mock_class = patcher.start()
        if "return_value" in lib:
            mock_class.return_value = lib["return_value"]

        patched[topic].append({"lib": lib, "patcher": patcher})

    return patched


def stop_patcher(topic, patchers):
    for patcher in patchers[topic]:
        try:
            patcher['patcher'].stop()

        except Exception as e:
            # work around for patcher being stopped already. This is due to how the patchers are being handled
            # currently. We really only need to start 1 of any single libray so we just need to check for it
            pass
