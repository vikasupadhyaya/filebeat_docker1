import pytest
import string
from unittest.mock import patch
from lib.page_content import *


@pytest.mark.parametrize(
    'title, body, expected', [
        ('a title', 'body with \nnewlines and : \nother tokens', 9),
        ('title', 'body   multiple spaces and   tokens  :', 7),
        (None, None, 0),
        ('title with special characters 中国-最专业的企业级IT网站群', 'body with special characters 最专业的企业级IT网站群', 10)
    ]
)
def test_calculate_text_count(
        title, body, expected
):
    assert calculate_text_count(title, body) == expected


@pytest.mark.parametrize(
    'input_url, timeout, expected_status, expected_error', [
        ('http://www.techtarget.com', 5, 'OK', None),                         # Happy path
        ('https://www.techtarget.com.cn/', 5, 'OK', None),                    # Site with special characters
        ('techtarget.com', 5, 'ValueError', 'ValueError'),                    # Invalid input url - missing protocol
        ('https://www.x.com', 5, 'Other', 'SSL: CERTIFICATE_VERIFY_FAILED'),  # Error - SSL failure
        ('http://localhost', 5, 'Other', 'Connection refused'),               # Error - connection refused
        ('https://www.techtarget.com', 1, 'Other', 'The read operation timed out'),  # Error - Time out
    ]
)
def test_get_html(input_url, timeout, expected_status, expected_error):
    status, html, error = get_html(input_url, timeout)

    assert status == expected_status
    if expected_error:
        assert expected_error in error
    else:
        assert html is not None


@pytest.mark.parametrize(
    'directory, html_filename, input_filename, expected_title, expected_error', [
        ('scrape_html', '1_input_good.html', '1_expected_text.txt',
         'Production-Grade Container Orchestration - Kubernetes', None),
        ('scrape_html', '2_input_no_title.html', '2_expected_no_title_text.txt', None, None),
        ('scrape_html', '3_input_empty.html', '3_expected_empty.txt', None, 'No html to parse'),
        ('scrape_html', '4_input_no_text.html', '3_expected_empty.txt', None, None),
        ('scrape_html', '5_input_invalid.html', '3_expected_empty.txt', None, None),
        ('scrape_html', '6_input_special_characters.html', '6_expected_special_characters.txt',
         'TechTarget中国-最专业的企业级IT网站群', None),
    ]
)
def test_extract_from_html(
        generate_html, html_filename,
        generate_input_text, input_filename,
        directory, expected_title, expected_error
):
    title, text, error = extract_from_html(generate_html)
    expected_text = generate_input_text.translate((str.maketrans('', '', string.whitespace)))

    assert title == expected_title
    if expected_text:
        assert text.translate(str.maketrans('', '', string.whitespace)) == expected_text
    assert error == expected_error


@pytest.mark.parametrize(
    'directory, input_filename, expected_filename, status, get_error, title, text, scrape_error', [
        ('webscrape_page', 'general_process_data.json', '1_expected_valid.json', 'OK', None,
         "Some Cool Title", "Some\nCool\nWords", None),
        ('webscrape_page', 'general_process_data.json', '2_expected_scrape_error.json', 'BLAH', "Some Error", None, None,
         None),
        ('webscrape_page', 'general_process_data.json', '3_expected_no_page_title_error.json', 'OK', None, None,
         "Some\nCool\nWords", None),
        ('webscrape_page', 'general_process_data.json', '4_expected_no_title_no_text_error.json', 'OK', None, None, "",
         None),
        ('webscrape_page', 'general_process_data.json', '4_expected_no_title_no_text_error.json', 'OK', None, "", None,
         None),
        ('webscrape_page', 'general_process_data.json', '4_expected_no_title_no_text_error.json', 'OK', None, None, None,
         None),
        ('webscrape_page', 'general_process_data.json', '5_expected_no_text_error.json', 'OK', None, "A really cool title"
         , "", None),
        ('webscrape_page', 'general_process_data.json', '5_expected_no_text_error.json', 'OK', None, "A really cool title"
         , None, None),
        ('webscrape_page', 'general_process_data.json', '6_expected_special_characters.json', 'OK', None,
         "最专业 \n 的企 业级IT网站群", "最专业的企业级 IT网站群", None),
    ]
)
def test_page_content(
        generate_input, input_filename,
        generate_expected, expected_filename,
        directory, status, get_error, title, text, scrape_error
):
    with patch('lib.page_content.get_html') as mock_get_html:
        with patch('lib.page_content.extract_from_html') as mock_scrape_html:
            with patch('lib.store_content.storage.Client') as mock_storage_client:
                with patch('lib.store_content.uuid') as mock_uuid:
                    mock_storage_client.return_value.bucket.return_value.blob.return_value.upload_from_string \
                        .return_value = 1
                    mock_uuid.uuid4.return_value = 'TEST_UUID'
                    mock_get_html.return_value = status, "", get_error
                    mock_scrape_html.return_value = title, text, scrape_error
                    assert webscrape_page(input_event=generate_input, timeout=5) == generate_expected


@pytest.mark.skip(reason="manual test for obtaining html for url - no assertions")
def test_manually_get_html():
    status, html, error = get_html('http://www.yahoo.com?manual=1')

    print(' ')
    print('html length: ', len(html))
    print('errors: ', error)
    print('status: ', status)
    print('html content: ', html)


@pytest.mark.skip(reason="manual test for testing storage of scraped content")
def test_manually_page_content():
    """
    Required environment variables:
        PROJECT - GCP_PROJECT target
        STORAGE_BUCKET - the cloud storage bucket name for storing scraped content

    The content of scraped page to be placed in cloud storage location for
    specified PROJECT in STORAGE_BUCKET - scraped/CID/uuid-value
    """
    testurl = 'http://www.yahoo.com?test=manual1'
    process_data = {'REF_PARAM': 'http://www.yahoo.com?test=manual1', 'CID':'123'}
    process_data_results = webscrape_page(process_data, 1)

    print(' ')  # clear end of line test output
    print('results :', process_data_results)
