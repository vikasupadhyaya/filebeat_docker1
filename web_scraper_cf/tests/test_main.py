import json

import pytest
import main
import requests
import requests_mock
import statistics
from unittest.mock import patch
from lib.page_content import webscrape_page
from tests.conftest import read_resource_file


@pytest.mark.parametrize(
    "directory, input_filename, expected_filename, edc_service_status_code, event_service_call_count", [
        ("store_webscraping_results_to_db", "1_input_good.json", "1_expected_good.json", requests.codes.ok, 1),
        ("store_webscraping_results_to_db", "1_input_good.json", "1_expected_rest_error.json",
         requests.codes.not_found, 1)
    ]
)
def test_store_webscraping_results_to_db(directory, input_filename, expected_filename, edc_service_status_code,
                                         event_service_call_count, generate_expected, generate_input, monkeypatch):
    with requests_mock.Mocker(real_http=True) as request_mocker:
        with patch("main.EDC_REST_URL", "https://primary-edc-inboundconverter.eng.techtarget.com") as edc_rest_url:
            monkeypatch.setattr("main.project", "tt-pd-eng")
            response_json = json.loads(read_resource_file("test_rest_store", "1_expected_good.json"))
            request_mocker.register_uri('POST', edc_rest_url, status_code=edc_service_status_code, json=response_json)
            result_event = main.store_webscraping_results_to_db(generate_input, edc_rest_url)

            assert result_event == generate_expected
            assert len(request_mocker.request_history) == event_service_call_count


@pytest.mark.parametrize(
    "directory, event_filename, expected_filename, event_data_tag, event_service_call_count", [
        ("process_webscrape_event", "1_input_good.json", "1_expected_good.json", "data", 1),
        ("process_webscrape_event", "2_input_missing_required_fields.json", "2_expected_missing_required_fields.json",
         "data", 0),
        ("process_webscrape_event", "3_input_title_with_spaces.json", "3_expected_title_with_spaces.json", "data", 2),
        ("process_webscrape_event", "4_input_title_with_special_characters.json",
         "4_expected_title_with_special_characters.json", "data", 2)
    ]
)
def test_process_webscrape_event(
        directory, create_event_from_json, event_data_tag,
        event_filename, generate_expected, generate_context, expected_filename, event_service_call_count, monkeypatch
):
    with patch('lib.store_content.storage.Client') as mock_storage_client:
        with patch("main.EDC_REST_URL", "https://primary-edc-inboundconverter.eng.techtarget.com") as edc_rest_url:
            with patch('main.futures') as mock_futures:
                with requests_mock.Mocker(real_http=True) as request_mocker:
                    with patch('lib.store_content.uuid') as mock_uuid:
                        monkeypatch.setattr("main.project", "tt-pd-eng")
                        response_json = json.loads(read_resource_file("test_rest_store", "1_expected_good.json"))
                        request_mocker.register_uri('POST', edc_rest_url + '/api/persist/url',
                                                    status_code=requests.codes.ok, json=response_json)
                        mock_futures.return_value = None
                        mock_storage_client.return_value.bucket.return_value.blob.return_value.upload_from_string \
                            .return_value = 1
                        mock_uuid.uuid4.return_value = 'TEST_UUID'

                        result_event = main.process_webscrape_event(create_event_from_json, generate_context)

                        # validate the standard deviation of the text count as it changes for the same url
                        if 'PAGE_TEXT_COUNT' in result_event:
                            assert statistics.pstdev([result_event['PAGE_TEXT_COUNT'],
                                                      generate_expected['PAGE_TEXT_COUNT']]) <= 10
                            result_event.pop('PAGE_TEXT_COUNT')
                            generate_expected.pop('PAGE_TEXT_COUNT')

                        assert len(request_mocker.request_history) == event_service_call_count
                        assert result_event == generate_expected


@pytest.mark.skip(reason="manual test for testing scrape and storage of url and scraped content into cloud storage")
def test_manually_store_webscraping():
    """
    Required environment variables:
        PROJECT - GCP_PROJECT target
        STORAGE_BUCKET - the cloud storage bucket name for storing scraped content

    REF_PARAM should contain a url that is not saved for CID - a unique
    url ensures that new entry is created, otherwise if page text count
    is same as present in db, then this run will not have reference
    to the stored content file name

    Verification that url is added and pageContent of entity column refers
    to saved file in PROJECT storage Bucket - <PROJECT>-ibc-content
    - and pageContent path corresponds to file in storage Bucket
    """

    process_data = {'REF_PARAM': 'http://www.yahoo.com?test=manual16', 'CID': '123', 'PAGE_TYPE': 'other'}
    # DP-405: below URL results in an exception from the Extractor.get_content() method
    # process_data = {'REF_PARAM': 'https://fractal.ai/', 'CID': '123', 'PAGE_TYPE': 'other'}
    process_data_results = webscrape_page(process_data, 10)

    print(' ')  # clear end of line test output
    edc_url = "https://primary-edc-inboundconverter.eng.techtarget.com/api/persist/url"

    result = main.store_webscraping_results_to_db(process_data_results, edc_url)

    # PAGE_CONTENT output here corresponds to file in storage Bucket
    # in event that we did not use a unique url/text count
    print('result: ', result)
