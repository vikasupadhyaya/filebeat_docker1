
def get_web_scraper_config():
    """

    """
    return {
        "data_tag": "data",
        "edc_service_store_method_path": "/api/persist/url",
        "completed_events_topic": "ibc.completed-activities",
        "failed_events_topic": "ibc.failed-events",
        "webscrape_timeout": 5
     }
