"""

"""

from urllib.request import Request
from urllib.request import urlopen
from urllib.error import HTTPError, URLError
import charade
import socket

from lxml.html import fromstring
from boilerpy3 import extractors

from lib.error_handler import append_error
from lib.store_content import save_scraped_content

"""
Should use a better user-agent with reference to TechTarget
Reference: https://developers.whatismybrowser.com/useragents/explore/software_type_specific/analyser/
POC used: "my web scraping program. contact me at admin@domain.com"
"""
headers = {'User-Agent': 'Mozilla/5.0 (compatible; TechTarget Analyzer/1.0; +http://techtarget.com/ )'}


def get_html(url, timeout=None):
    """
    Adopted from boilerpipe3 __init__.py.
    Gets html for a url in exact same way boilerpipe would.
    Only difference is can control timeout which was not possible via boilerpipe.

    Obtain the full html content for the specified url.

    Parameters:
        url (str) - to retrieve
        timeout (number) - number of seconds before terminating retrieval

    Return: status (str), html (str), error (str)
        status - OK if successful; error reason for problem
        html - the url content
        error - None; text of exception otherwise

    """
    html, error, status = None, None, None

    try:
        request = Request(url, headers=headers)
        connection = urlopen(request, timeout=timeout)
        # Extract and encode html.
        data = connection.read()
        encoding = connection.headers['content-type'].lower().split('charset=')[-1]
        if encoding.lower() == 'text/html':
            encoding = charade.detect(data)['encoding']
        if encoding is None:
            encoding = 'utf-8'
        html = str(data, encoding)
        status = 'OK'
    except URLError as urlerror:
        if type(urlerror.reason) == socket.timeout:
            status = 'Timeout'
            error = str(urlerror)
        else:
            status = 'Other'
            error = str(urlerror)

    except HTTPError:
        status = 'HTTPError'
        error = str(HTTPError)

    except ValueError:
        status = 'ValueError'
        error = str(ValueError)

    except Exception as myex:
        status = 'Other'
        error = str(myex)

    return status, html, error


def extract_from_html(html, extractor=None):
    """
    Use boilerpipe to extract from the provided html using
    supplied extractor strategy.
    Given html extract text and title.
    Note: moved input from url to html in order to control timeout.

    Example
    -------
    url = 'http://hs-sites.origin.hubspot.net'
    status, html = get_html(url, timeout=1)
    title, text = extract_from_html(html)

    Parameters:
        html (str) - the fully retrieved html of a url
        extractor (boilerpy3.extractors) - strategy to perform in extracting 
            content from html; KeepEverythingExtractor is default

    Return: title (str), text (str), error (str)
        title - extracted from html using lxml, then trimmed to remove leading
            and trailing whitespace
        text - extracted from html using extractor strategy
        error - string of reason for failure
    """
    if extractor is None:
        extractor = extractors.KeepEverythingExtractor()

    title, text, error = None, None, None

    if not html:
        return title, text, 'No html to parse'

    try:
        # Extract text from HTML via Boilerpipe.
        text = extractor.get_content(text=html)
        # Extract title from html using lxml.
        tree = fromstring(html)
        title = tree.findtext('.//title')
    except Exception as my_exception:
        # sometimes exceptions have no content/description, and, thus, produce empty strings
        # as the value of 'error' - which does not get added to the ERRORS array and provides
        # no indication that there were exceptions happening at all;
        # to indicate there was an exception - add our own error description
        description = str(my_exception)
        if not description:
            description = "--"
        error = "Exception extracting text and title: {}".format(description)

    if title:
        title = title.strip()

    return title, text, error


def calculate_text_count(title, body):
    """
    Determine the web pages text count using strategy
    which best determines a pages uniqueness.  POC
    determined that combining title of scraped page
    along with the extracted content produced sufficient
    uniqueness.  Concatenate title and body with a newline,
    then split on all newlines and spaces, with count being
    final collection of tokens (or words).

    Parameters:
        title (str) - extracted title of scraped web page
        body (str) - extracted content from web page

    Return: (number)
        (number) - representing text count of applied strategy
    """
    useTitle = ''
    useBody = ''
    if title:
        useTitle = title
    if body:
        useBody = body
    text = useTitle + '\n' + useBody
    words = ' '.join(text.split('\n')).split()
    word_cnt = len(words)
    return word_cnt


def webscrape_page(input_event, timeout):
    """
    Perform the web scrape and store collected details in the
    provided dictionary, returning updated dictionary details

    Parameters:
        input_event (dict) - dictionary of key/value pairs;
            REF_PARAM key expected to contain url (str) to scrape
        timeout (number) - number of seconds to wait for page retrieval

    Return: input_event (dict)
        input_event - provided event with additional content added
        that may include the following:
            PAGE_CONTENT (str) - the full html retrieved from url
            PAGE_TITLE (str) - determined title of scraped url (trimmed value)
            PAGE_TEXT_COUNT (number) - determined text count of scraped url
            ERRORS (list) - list of encountered error objects
    """
    if 'REF_PARAM' not in input_event:
        append_error(input_event, 'failure_scrape', 'Missing required REF_PARAM field.')
        return input_event
    url = input_event.get("REF_PARAM")
    status, html, error = get_html(url, timeout=timeout)

    if status == "OK":
        save_scraped_content(input_event, html)
        title, text, error = extract_from_html(html)
        if error:
            append_error(input_event, "extract_from_html_error", error)

        if not title:
            append_error(input_event, "no_page_title", error)
        else:
            input_event['PAGE_TITLE'] = title

        if not text:
            append_error(input_event, "no_page_text", error)
        else:
            input_event['PAGE_TEXT_COUNT'] = calculate_text_count(title, text)

    else:
        append_error(input_event, 'failure_scrape', error)

    return input_event
