"""
Required environment variables:
    PROJECT - GCP_PROJECT target
    STORAGE_BUCKET - the cloud storage bucket name for storing scraped content
"""
import json
import os
import uuid
import logging

from lib.error_handler import append_error
from google.cloud import storage

project = os.getenv("PROJECT")
storage_bucket_name = os.getenv("STORAGE_BUCKET")
logger = logging.getLogger(__name__)


def save_scraped_content(input_event, html):
    """
    Save the html to cloud storage location, adding the file path and name in
    process_data.

    This method does not capture any Exceptions that may arise so that
    we can fall thru to abort processing and not persist a url entry.
    We do not want to create a url and/or page entry in database since we
    would not have the scraped content for further processing if classification
    process was determined to be performed.

    Args:
        input_event:
            dictionary - will add PAGE_CONTENT entry with value of file name
        html:
            scraped html to save

    Returns:
        No return value
    """

    # path of file: scraped/client-id/uuid
    scraped_filename = 'scraped/' + input_event.get('CID') + '/' + str(uuid.uuid4())
    input_event['PAGE_CONTENT'] = scraped_filename

    try:
        storage_client = storage.Client()
        bucket = storage_client.bucket(bucket_name=storage_bucket_name, user_project=project)
        blob = bucket.blob(blob_name=scraped_filename)
        blob.upload_from_string(data=html)
    except Exception as e:
        err_str = "web_scraper CF: Exception storing full content to GCP bucket {} - ERROR: - {}".format(
            storage_bucket_name, json.dumps(input_event), str(e))
        logger.exception(err_str)
        append_error(input_event, 'save_html_error', err_str)

