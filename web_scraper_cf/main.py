"""
main.py contains the main processing logic for processing
"""
import json
import os
import base64
import time
import logging

import requests
from google.cloud import pubsub_v1
from config import get_web_scraper_config
from lib.error_handler import append_error
from lib.page_content import webscrape_page
from tt_common.tt_logging import Logging

project = os.getenv("PROJECT")
EDC_REST_URL = os.getenv("EDC_REST_URL")

futures = dict()
Logging().init_logging()
logger = logging.getLogger(__name__)


def get_callback(logstash_id):
    def callback(future):
        try:
            logger.info("Future completed for {} with result {}.".format(logstash_id, future.result()))
            if futures.get(logstash_id):
                futures.pop(logstash_id)
        except Exception as e:
            err_str = str(e)
            logger.exception("Fatal exception while publishing {} for {}.".format(err_str, logstash_id))
            append_error(None, 'fatal_error', err_str)
    return callback


def publish_event_to_topic(event, topic):
    publisher = pubsub_v1.PublisherClient()
    logstash_id = event.get("LOGSTASH_ID")
    data = json.dumps(event).encode("utf-8")
    futures.update({logstash_id: None})
    # When you publish a message, the client returns a future.
    topic_path = publisher.topic_path(project, topic)
    future = publisher.publish(topic_path, data)
    futures[logstash_id] = future
    # Publish failures shall be handled in the callback function.
    future.add_done_callback(get_callback(logstash_id))

    # Wait for the future to resolve before exiting.
    while len(futures) != 0:
        time.sleep(1)


def process_webscrape_event(event, context):
    """
    Method to read event form PubSub topic, do webscraping for the REF_PARAM URL,
    and send the resulting enriched event off to another topic to be stored into BigQuery
    :param event: (dict) The dictionary with data specific to this type of
         event. The `data` field contains the PubsubMessage message. The
         `attributes` field will contain custom attributes if there are any.
    :return: (dict) post-processed data dictionary
    """
    input_event = {}
    try:
        config = get_web_scraper_config()
        failed_events_topic = config['failed_events_topic']
        completed_events_topic = config['completed_events_topic']
        if config['data_tag'] == 'attributes':
            input_event = json.loads(json.dumps(event['attributes']))
        elif config['data_tag'] == 'data':
            encoded_event = event['data']
            if encoded_event is not None:
                input_event = json.loads(base64.b64decode(encoded_event).decode('utf-8'))
            else:
                raise Exception('Wrong format of input event - no data key found; event: ' + event)
        else:
            raise Exception('No Data tag found in config')

        timeout = config['webscrape_timeout']
        if context:
            input_event['event_id'] = context.event_id

        logger.info("Processing input event: {}".format(json.dumps(input_event)))

        if get_missing_required_fields(input_event):
            input_event['STATUS'] = "MISSING_REQUIRED_FIELDS"
            return input_event

        # TODO - move trim of title to extraction
        # TODO - refactor order of processing to do save before publishing to next topic;
        #        need db record for accurate association of stored activity
        # TODO - how to fail message handling for an automated retry
        result_message = webscrape_page(input_event, timeout)
        # remove leading and trailing white spaces and tabs from the page title
        page_title = result_message.get('PAGE_TITLE')
        if page_title:
            page_title_stripped = page_title.strip()
            result_message['PAGE_TITLE'] = page_title_stripped
        result_message['STATUS'] = "NO_EARLY_EXIT"
        logger.info("Successfully webscraped event, sending it to the {} topic ".format(completed_events_topic))
        # store results of the webscraping into the MySQL DB via EDC REST call
        edc_rest_endpoint = EDC_REST_URL + config["edc_service_store_method_path"]
        result_message = store_webscraping_results_to_db(result_message, edc_rest_endpoint)
        # publish after storing to ensure failures logged and status correct
        publish_event_to_topic(result_message, completed_events_topic)
        return result_message

    except Exception as e:
        err_str = "web_scraper CF: Exception processing event: {} - ERROR: - {}".format(json.dumps(input_event), str(e))
        logger.exception(err_str)
        append_error(input_event, 'fatal_error', err_str)
        event['STATUS'] = "ERROR_EXIT"
        publish_event_to_topic(input_event, failed_events_topic)


def store_webscraping_results_to_db(event, edc_service_url):
    """
    TODO - use return object values before sending on to next topic
    and use a property for determining if tax classification should be performed
    which would indicate we have a new page entry

    function to call EDC microservice via a REST API to store webscraped page info for URL+CID
    example REST API: ???
    """
    url = event.get("REF_PARAM")
    cid = event.get("CID")

    request_data = {
        "clientId": cid,
        "clientUrl": url,
        "pageContent": event.get('PAGE_CONTENT'),
        "pageTextCount": event.get('PAGE_TEXT_COUNT'),
        "pageTitle": event.get('PAGE_TITLE'),
        "pageType": event.get('PAGE_TYPE')
    }

    try:
        response = requests.post(edc_service_url, json=request_data)
        if not response.ok:
            logger.error("Error calling REST API {} to save request_data: {}: error={}".format(
                edc_service_url, request_data, response.reason))
            append_error(event, 'failure_to_store_scraped_data',
                         'Call failed to {} error: {}, unable to store results to the db .'.format(
                             edc_service_url, response.reason))
            event['STATUS'] = "ERROR_EXIT"
            return event
        json_response = response.json()
        if not json_response:
            logger.info("Completed call to {} OK; request_data={} - no response was received".format(
                edc_service_url, request_data))
        else:
            logger.info("Completed call to {}: request_data: {}; response: {}".format(
                edc_service_url, request_data, json_response))
    except Exception as e:
        err_str = "Error calling REST API {} to save request_data: {}: error={}".format(
            edc_service_url, request_data, str(e))
        logger.exception(err_str)
        append_error(event, 'failure_to_store_scraped_data', 'Error calling REST API {}; error: {}, unable to store results to the DB'
                     .format(edc_service_url, err_str))
        event['STATUS'] = "ERROR_EXIT"

    return event


def get_missing_required_fields(input_event):
    required_fields = ["LOGSTASH_ID", "CID", "REF_PARAM", "ACTIVITY_DATE"]
    missing_fields = []
    for field in required_fields:
        if not input_event.get(field):
            missing_fields.append(field)

    if missing_fields:
        append_error(input_event, "MISSING_REQUIRED_FIELDS",
                     description="this event is missing required fields: {}".format(missing_fields))
    return missing_fields
