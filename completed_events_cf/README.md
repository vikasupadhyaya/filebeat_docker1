# Description
The purpose of this Cloud Function is to store events received from a configured PubSub topic into configured BigQuery table[s]

# Example format of incoming events
```buildoutcfg
{
	"data": 
	{
	  "LOGSTASH_ID": "marina_id_0326_1",
	  "REFERER": "https://blog.zoom.us/",
	  "ACTIVITY_IP": "1.0.0.0",
	  "ACTIVITY_DATE": "2021-03-18",
	  "ACTIVITY_TYPE_ID": "31",
	  "CID": "17872382",
	  "VERSION": "2.0",
	  "REF_PARAM": "https://www.yahoo.com",
	  "USER_AGENT": "Mozilla/5.0 (X11; CrOS aarch64 13421.102.0) AppleWebKit/537.36 (KHTML, like Gecko)Chrome/86.0.4240.199 Safari/537.36",
	  "REGION_CD": "code1",
	  "IS_ISP": false,
	  "EU_ALLOWED": true,
	  "ACTIVITY_MILLIS": 1607557877000,
	  "DOMAIN": "yahoo.com",
	  "PAGE_TYPE": "type1",
	  "PAGE_TITLE": "page-title1",
	  "PAGE_TEXT_COUNT": 1234,
	  "DATE_CREATED": "2021-03-18 10:00:22",
	  "DATE_MODIFIED": "2021-03-18 10:00:22"
	}

}
```

# Testing
To test this Cf in GCP - you have to provide and incoming event in a specific format:

1. event content is the value of the "data" key 
2. this CF expects one event only as input
3. the content has to be encoded - use this site to do it: https://www.base64encode.org/
4. ACTIVITY_DATE has to be in the "YYY-MM-DD" format
5. ACTIVITY_MILLIS - has to be a corresponding timestamp value in milliseconds
   

Using the plain text example of an event above - this is how a valid incoming encoded event should look:
```
{
	"data": "W3sKCSAgIkxPR1NUQVNIX0lEIjogIm1hcmluYV9sb2dzdGFzaF9pZDMiLAoJICAiUkVGRVJFUiI6ICJodHRwczovL2Jsb2cuem9vbS51cy8iLAoJICAiQUNUSVZJVFlfSVAiOiAiMS4wLjAuMCIsCgkgICJBQ1RJVklUWV9EQVRFIjogIjIwMjEtMDMtMTYiLAoJICAiQUNUSVZJVFlfVFlQRV9JRCI6ICIzMSIsCgkgICJDSUQiOiAiMTc4NzIzODIiLAoJICAiVkVSU0lPTiI6ICIyLjAiLAoJICAiUkVGX1BBUkFNIjogImh0dHBzOi8vd3d3LnlhaG9vLmNvbSIsCgkgICJVU0VSX0FHRU5UIjogIk1vemlsbGEvNS4wIChYMTE7IENyT1MgYWFyY2g2NCAxMzQyMS4xMDIuMCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbylDaHJvbWUvODYuMC40MjQwLjE5OSBTYWZhcmkvNTM3LjM2IiwKCSAgIlJFR0lPTl9DRCI6ICJjb2RlMSIsCgkgICJJU19JU1AiOiBmYWxzZSwKCSAgIkVVX0FMTE9XRUQiOiB0cnVlLAoJICAiQUNUSVZJVFlfTUlMTElTIjogMTYwNzU1Nzg3NzAwMCwKCSAgIkRPTUFJTiI6ICJ5YWhvby5jb20iLAoJICAiUEFHRV9UWVBFIjogInR5cGUxIiwKCSAgIlBBR0VfVElUTEUiOiAicGFnZS10aXRsZTEiLAoJICAiUEFHRV9URVhUX0NPVU5UIjogMTIzNCwKCSAgIkRBVEVfQ1JFQVRFRCI6ICIyMDIxLTAzLTE2IDEwOjAwOjIyIiwKCSAgIkRBVEVfTU9ESUZJRUQiOiAiMjAyMS0wMy0xNiAxMDowMDoyMiIKCX1d"
}
```

