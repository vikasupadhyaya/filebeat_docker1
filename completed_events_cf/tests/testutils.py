"""
a utility file with helper methods for unit testing
"""

import json
import base64
from types import SimpleNamespace
import os
import pytest
import codecs


def create_event_from_json(directory, event_filename, event_data_tag):
    """
    Function to return a json representation of a pubsub event
    :param directory: (str) the directory the pubsub event is located in
    :param event_filename: (str) the name of the pubsub event
    :return: (dict) A json representation of the pubsub event
    """
    event = read_resource_file_as_json(directory, event_filename)
    if event_data_tag == "attributes":
        return {"attributes": event}
    else:
        message_bytes = json.dumps(event).encode('utf-8')
        return {"data": base64.b64encode(message_bytes)}


def read_resource_file_as_json(directory, file_name):
    """
    Function to return a json representation of a resource
    :param directory: (str) the directory the resource is located in
    :param file_name: (str) the name of the file
    :return: (dict) A json representation of the resource
    """
    return json.loads(read_resource_file(directory, file_name))


def read_resource_file(directory, file_name):
    """
    Function to return a string representation of the contents of a file
    :param directory: (str) the directory the file is located in
    :param file_name: (str) the name of the file
    :return: (str) A string representation of the file
    """
    with open(os.path.join(get_base_dir(), directory, file_name)) as file:
        data = file.read()
    return data


def get_base_dir():
    """
    Function to return the base directory of the file for reference in testing
    :return: (str) The path to the file that is being tested
    """
    my_path = os.path.abspath(os.path.dirname(__file__))
    base_dir = os.path.join(my_path, 'resources')
    return base_dir


def generate_context():
    """
    Function to return a test representation of the context provided to a cloud functions
    :return: (dict) A test context
    """
    context = {
        "event_id": 12345678,
        "timestamp": "2020-08-20T21:26:30.600Z"
    }
    return SimpleNamespace(**context)


if __name__ == '__main__':
    get_base_dir()
