import base64
import json
import pytest

import main
from unittest.mock import patch

from tests.lib.gcp import bigquery
from tests.testutils import read_resource_file_as_json, generate_context


@pytest.mark.parametrize(
    "event_filename, expected_filename", [
        ("1_input_all_fields_good.json", "1_expected_result.json"),
        ("2_input_eu_not_allowed.json", "2_expected_eu_not_allowed.json"),
        ("3_input_with_errors_field.json", "3_expected_with_errors_field.json"),
        ("4_input_eu_allowed_missing.json", "4_expected_eu_allowed_missing.json")
    ]
)
def test_write_to_bq(event_filename, expected_filename, monkeypatch):
    with patch('main.bigquery.Client') as mock_bigquery:
        with patch.object(mock_bigquery, 'insert_rows_json', None):
            mock_bigquery.return_value = bigquery.Client()
            monkeypatch.setattr("main.project", "unit_test_project")
            input_event = create_encoded_event_from_json("", event_filename)
            expected_result_event = read_resource_file_as_json("", expected_filename)
            result_event = main.write_to_bq(input_event, generate_context)
            assert result_event == expected_result_event


def create_encoded_event_from_json(directory, event_filename):
    """
    Function to return a json representation of a pubsub event
    :param directory: (str) the directory the pubsub event is located in
    :param event_filename: (str) the name of the pubsub event
    :return: (dict) A json representation of the pubsub event
    """
    event = read_resource_file_as_json(directory, event_filename)
    message_bytes = json.dumps(event).encode('utf-8')
    return {"data": base64.b64encode(message_bytes)}

