"""
Helper module to make pylint work as expected by ignoring certain paths
"""
from pylint.utils import utils


class PylintIgnorePaths:
    """
    Helper class implementation
    """
    def __init__(self, *paths):
        self.paths = paths
        self.original_expand_modules = utils.expand_modules
        utils.expand_modules = self.patched_expand

    def keep_item(self, item):
        """
        Helper method for exclusion
        """
        if any(1 for path in self.paths if item['path'].startswith(path)):
            return False

        return True

    def patched_expand(self, *args, **kwargs):
        """

        """
        result, errors = self.original_expand_modules(*args, **kwargs)
        result = list(filter(self.keep_item, result))

        return result, errors
