"""
main.py contains the main processing logic for processing
"""
import json
import os
import base64
import logging

from google.cloud import pubsub_v1, bigquery
from config import get_config
from tt_common.tt_logging import Logging

project = os.getenv("PROJECT")
Logging().init_logging()
logger = logging.getLogger(__name__)


def write_to_bq(event, context):
    # pylint: disable=unused-argument
    """
    Cloud Function to load events to BQ
    :param event: (dict) Dictionary that contains key,value pairs per each event field (potential column in DB)
    :param context: (google.cloud.functions.Context) The Cloud Functions event
         metadata. The `event_id` field contains the Pub/Sub message ID. The
         `timestamp` field contains the publish time
    :return: inserted_events: events inserted into each table - to enable Unit testing
    """

    input_event = json.loads(base64.b64decode(event['data']).decode('utf-8'))
    logger.debug("Processing event: {}".format(input_event))
    bq_client = bigquery.Client()
    config = get_config()
    tables = config.get('tables', [])
    inserted_events = {}
    for table in tables:
        if not table.get("enabled", True):
            # not writing into this table - as the writes are disabled
            continue
        table_name = table['table_name'].format(project)
        columns_to_insert = table.get('columns')
        event_to_insert = {}
        inserted_events[table_name] = event_to_insert
        # check if this table has EU restrictions - and if so - check if this event
        # is allowed to be stored into this table
        if table.get("filter_eu_not_allowed"):
            if not input_event.get('EU_ALLOWED'):
                # this event is not allowed to be stored into this table - get out of the table processing
                logger.debug("Event is not written into the {} table due to EU_ALLOWED == FALSE; event: {}".format(
                    table_name, input_event))
                continue
        # filter out fields not needed to insert as columns into the target table -
        # leave only those fields/columns that are specified in the "columns" configuration for the table
        for field in input_event:
            if should_insert_column(field, columns_to_insert):
                event_to_insert[field] = input_event.get(field)

        # if this event has fields (not empty) after filtering - insert into BQ
        if event_to_insert:
            try:
                # This is required to flatten the ERRORS element in order to insert the value in the table
                if "ERRORS" in event_to_insert:
                    event_to_insert["ERRORS"] = json.dumps(event_to_insert["ERRORS"])
                events_to_insert_list = []  # we pass a list of events to insert into BQ - even though it is only one event, always
                events_to_insert_list.append(event_to_insert)
                errors = bq_client.insert_rows_json(table_name, events_to_insert_list)
                if errors:
                    # may have to loop over the indices of the return to get the actual rows that errored?
                    append_error(event_to_insert, errors, description="Could not write to {}".format(table_name))
                    logger.error("Failed to write event {} into the table {} due to error[s]: {}".format(
                        event_to_insert, table_name, errors))
                    publish_event_to_topic(event_to_insert, "ibc.failed-events")
                else:
                    logger.debug("Successfully wrote event {} into the table {}".format(event_to_insert, table_name))
            except Exception as e:
                logger.exception("Exception caught: failed to write event {} into the table {} due to error[s]: {}".format(
                    event_to_insert, table_name, json.dumps([str(e)])))
                append_error(event_to_insert, json.dumps([str(e)]), "Fatal Error occurred writing to {}".format(table_name))
                publish_event_to_topic(event_to_insert, "ibc.failed-events")

    # return events inserted into each table - to enable Unit testing
    return inserted_events


def publish_event_to_topic(event, topic):
    publisher = pubsub_v1.PublisherClient()
    data = json.dumps(event).encode("utf-8")
    # When you publish a message, the client returns a future.
    topic_path = publisher.topic_path(project, topic)
    publisher.publish(topic_path, data)


def should_insert_column(field_name, columns_to_insert):
    if columns_to_insert is not None and field_name is not None:
        for column_name in columns_to_insert:
            if column_name == field_name:
                return True
    # field name from the event did not match any configured column
    return False


def append_error(process_data, error, description=""):
    error_col = "ERRORS"
    error_obj = {"ERROR": error}
    if description:
        error_obj["DESCRIPTION"] = description.replace('\n', '')
    if process_data.get(error_col, []):
        process_data[error_col].append(error_obj)
    else:
        process_data[error_col] = [error_obj]
