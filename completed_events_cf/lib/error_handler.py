
error_col = "ERRORS"


def append_error(process_data, error, description=""):
    error_col = "ERRORS"
    error_obj = {"ERROR": error}
    if description:
        error_obj["DESCRIPTION"] = description.replace('\n', '')

    if process_data.get(error_col, []):
        process_data[error_col].append(error_obj)
    else:
        process_data[error_col] = [error_obj]
