"""
Configuration for this cloud Function
"""


def get_config():
    return {
        "tables": [
            {
                "table_name": "{}.edc_inboundconverter.inbound_converter",
                "columns": [
                    "LOGSTASH_ID", "ACTIVITY_TYPE_ID", "VERSION", "CID", "REF_PARAM", "REFERER", "USER_AGENT",
                    "REGION_CD", "ACTIVITY_DATE", "EU_ALLOWED", "DOMAIN", "ACTIVITY_MILLIS", "IS_ISP",
                    "DATE_CREATED", "DATE_MODIFIED", "PAGE_TYPE", "PAGE_TITLE", "PAGE_TEXT_COUNT", "STATUS", "ERRORS"
                ],
                "enabled": True
            },
            {
                "table_name": "{}.edc_inboundconverter.inbound_converter_ip_lookup",
                "columns": ["LOGSTASH_ID", "ACTIVITY_IP", "ACTIVITY_DATE"],
                "enabled": True,
                "filter_eu_not_allowed": True
            }
        ]
    }
