#!groovy

@Library('jenkins-shared') _

def buildCreatePythonVenv(cloud_func_dir) {
    // initial version is 3.6.8 - should be using a 3.7.1 or greated to match cloud env
    // pending Jenkins update and may affect tests
    return 'python3 -m virtualenv --clear --python python3 ' + cloud_func_dir + '/venv/build'
}

def buildActivateVenv(cloud_func_dir) {
    return '. ${WORKSPACE}/' + cloud_func_dir + '/venv/build/bin/activate'
}

def buildInstallReqsVenv() {
    def installReqsCommands = 'venv/build/bin/pip install --upgrade pip '
    installReqsCommands += '&& venv/build/bin/pip install -r ./requirements.txt'
    return installReqsCommands
}

def buildTestCommands(cloud_func_dir) {
    echo "Building test setup and execution commands for: " + cloud_func_dir

    // activate venv
    def testCommands = buildActivateVenv(cloud_func_dir) 
    // cd into cloud function dir
    testCommands += " && cd ${WORKSPACE}/" + cloud_func_dir
    // install requirements
    testCommands += " && " + buildInstallReqsVenv()
    // run tests
    testCommands += " && pytest "

    echo "Test setup and execution commands for: " + cloud_func_dir + "; " + testCommands

    return testCommands
}

pipeline {
    agent {
        label 'jenkinsci-deploy'
    }

    environment {
        region = "us-east4"
        project = "tt-pd-${ENVIRONMENT}"
        runtime = "python37"
        service_account = "tt-ibc@${project}.iam.gserviceaccount.com"
        runner_account = "tt-ci-cd-exec@${project}.iam.gserviceaccount.com"
        storage_bucket = "ibc-client-activity-input-${ENVIRONMENT}"
        ibc_configurations_storage_bucket = "ibc-configurations-${ENVIRONMENT}"
        source = "."
        memory = "128MB"
        
        // limit instances for cache lookup of microservice
        activity_cf_max_instances = "500"

        // limit writes of details
        completed_cf_max_instances = "500"

        // limit chosen based on last 30 days and writes of results to microservice
        web_scraper_cf_max_instance = "100"

        // default limit
        failed_cf_max_instances = "100"
        
        // kickfire load configurations
        kickfire_load_cf_max_instances = "5"
        kickfire_load_cf_memory = "2048MB"
        kickfire_load_cf_timeout = "180"
        
        // kickfire insert configurations
        kickfire_insert_cf_max_instances = "50"
        kickfire_insert_cf_memory = "128MB"
        kickfire_insert_cf_timeout = "180"
        
        // kickfire insert configurations
        kickfire_load_completed_cf_max_instances = "1"
        kickfire_load_completed_cf_memory = "512MB"
        kickfire_load_completed_cf_timeout = "180"
        
        // maxmind load configurations
        maxmind_load_cf_max_instances = "5"
        maxmind_load_cf_memory = "2048MB"
        maxmind_load_cf_timeout = "180"
        
        // maxmind insert configurations
        maxmind_insert_cf_max_instances = "50"
        maxmind_insert_cf_memory = "128MB"
        maxmind_insert_cf_timeout = "180"
        
        // maxmind insert configurations
        maxmind_load_completed_cf_max_instances = "1"
        maxmind_load_completed_cf_memory = "512MB"
        maxmind_load_completed_cf_timeout = "180"
        
        // cloud storage configurations - bucket monitoring
        storage_configs_cf_max_instances = "1"
        storage_configs_cf_memory = "128MB"
        storage_configs_cf_timeout = "60"

        // ibc_config_load CF shoudl have 1 instance only
        ibc_config_load_cf_max_instances = "1"

        // or change actions to always create accepting failure and then
        // since scheduler job is either create or update, set here
        // do a separate update
        scheduler_action = "create"

        labels = "tt-component=cloud-function,tt-environment=${ENVIRONMENT},tt-product=inbound-converter,tt-region=us-east4,tt-service=cloud-functions,tt-version=na,tt-zone=multi"

    }

    stages {
        stage('Configure Build') {
            steps {
                script {
                    env.ABORT_BUILD = 'true'
                    
                    // default log level of deployed functions if Debug is not set from user input
                    env.LOG_LEVEL = 'info'

                    env.DEPLOY_CF_DEFAULT = 'false'
                    env.DEPLOY_SCHEDULE_DEFAULT = 'false'
                    env.EXECUTE_TESTS = 'false'
                    // execute if not deploying is not a prompt and used
                    // for cases where we do a build but do not deploy any CF
                    env.EXECUTE_TESTS_IF_NOT_DEPLOYING = 'true'
                    env.DEPLOY_ACTIVITY_DETAILS_CF = 'false'
                    env.DEPLOY_COMPLETED_EVENTS_CF = 'false'
                    env.DEPLOY_WEB_SCRAPER_CF = 'false'
                    env.DEPLOY_FAILED_EVENTS_CF = 'false'
                    env.DEPLOY_IBC_CONFIG_LOAD_CF = 'false'
                    env.DEPLOY_IBC_CONFIG_SCHEDULER = 'false'
                    env.DEPLOY_KICKFIRE_LOAD_CF = 'false'
                    env.DEPLOY_KICKFIRE_INSERT_CF = 'false'
                    env.DEPLOY_KICKFIRE_LOAD_COMPLETED_CF = 'false'
                    env.DEPLOY_STORAGE_CONFIGS_CF = 'false'
                    env.DEPLOY_KICKFIRE_COMPLETE_CHECK_SCHEDULER = 'false'
                    env.DEPLOY_MAXMIND_LOAD_CF = 'false'
                    env.DEPLOY_MAXMIND_INSERT_CF = 'false'
                    env.DEPLOY_MAXMIND_LOAD_COMPLETED_CF = 'false'
                    env.DEPLOY_MAXMIND_COMPLETE_CHECK_SCHEDULER = 'false'
                }
                continueAfter(time: 5, unit: 'MINUTES') {
                    script {
                        def userInput = input message: 'Deployment Options:', ok: 'Ok',
                            parameters: [
                                booleanParam(name: 'Execute Tests', defaultValue: "${env.EXECUTE_TESTS}"),
                                booleanParam(name: 'Log Debug Level', defaultValue: "false", description: 'Deploy with logging at Debug Log Level'),
                                booleanParam(name: 'Deploy Activity Details CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which performs initial event checks'),
                                booleanParam(name: 'Deploy Completed Events CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which processes completed events'),
                                booleanParam(name: 'Deploy Web Scraper CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which performs web scraping of a url'),
                                booleanParam(name: 'Deploy Failed Events CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which processes failed events'),
                                booleanParam(name: 'Deploy IBC Config Loading CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which checks config in BQ and uploads to Firestore'),
                                booleanParam(name: 'Deploy IBC Config Scheduler', defaultValue: "${env.DEPLOY_SCHEDULE_DEFAULT}", description: 'Job to invoke the IBC Config Loading CF'),
                                
                                booleanParam(name: 'Deploy Kickfire Load CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which starts kickfire quarterly load'),
                                booleanParam(name: 'Deploy Kickfire Insert CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which inserts kickfire collection data'),
                                booleanParam(name: 'Deploy Kickfire Load Complete CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which checks for load complete and promotes kickfire collection configuration'),
                                booleanParam(name: 'Deploy Kickfire Complete Scheduler', defaultValue: "${env.DEPLOY_SCHEDULE_DEFAULT}", description: 'Job to invoke the Kickfire Load Complete Check CF'),
                                
                                booleanParam(name: 'Deploy Maxmind Load CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which starts maxmind data load'),
                                booleanParam(name: 'Deploy Maxmind Insert CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which inserts maxmind collection data'),
                                booleanParam(name: 'Deploy Maxmind Load Complete CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which checks for load complete and promotes maxmind collection configuration'),
                                booleanParam(name: 'Deploy Maxmind Complete Scheduler', defaultValue: "${env.DEPLOY_SCHEDULE_DEFAULT}", description: 'Job to invoke the Maxmind Load Complete Check CF'),
                                
                                booleanParam(name: 'Deploy Storage Configs CF', defaultValue: "${env.DEPLOY_CF_DEFAULT}", description: 'CF which monitors cloud storage for configuration changes')
                           ]

                        env.EXECUTE_TESTS = userInput.'Execute Tests'?:'false'
                        env.DEPLOY_ACTIVITY_DETAILS_CF = userInput.'Deploy Activity Details CF'?:'false'
                        env.DEPLOY_COMPLETED_EVENTS_CF = userInput.'Deploy Completed Events CF'?:'false'
                        env.DEPLOY_WEB_SCRAPER_CF = userInput.'Deploy Web Scraper CF'?:'false'
                        env.DEPLOY_FAILED_EVENTS_CF = userInput.'Deploy Failed Events CF'?:'false'
                        env.DEPLOY_IBC_CONFIG_LOAD_CF = userInput.'Deploy IBC Config Loading CF'?:'false'
                        env.DEPLOY_IBC_CONFIG_SCHEDULER = userInput.'Deploy IBC Config Scheduler'?:'false'
                        env.DEPLOY_KICKFIRE_LOAD_CF = userInput.'Deploy Kickfire Load CF'?:'false'
                        env.DEPLOY_KICKFIRE_INSERT_CF = userInput.'Deploy Kickfire Insert CF'?:'false'
                        env.DEPLOY_KICKFIRE_LOAD_COMPLETED_CF = userInput.'Deploy Kickfire Load Complete CF'?:'false'
                        env.DEPLOY_STORAGE_CONFIGS_CF = userInput.'Deploy Storage Configs CF'?:'false'
                        env.DEPLOY_KICKFIRE_COMPLETE_CHECK_SCHEDULER = userInput.'Deploy Kickfire Complete Scheduler'?:'false'
                        
                        env.DEPLOY_MAXMIND_LOAD_CF = userInput.'Deploy Maxmind Load CF'?:'false'
                        env.DEPLOY_MAXMIND_INSERT_CF = userInput.'Deploy Maxmind Insert CF'?:'false'
                        env.DEPLOY_MAXMIND_LOAD_COMPLETED_CF = userInput.'Deploy Maxmind Load Complete CF'?:'false'
                        env.DEPLOY_MAXMIND_COMPLETE_CHECK_SCHEDULER = userInput.'Deploy Maxmind Complete Scheduler'?:'false'
                        env.ABORT_BUILD = 'false'
                        
                        if (userInput.'Log Debug Level'?:'false' == 'true') {
                            env.LOG_LEVEL = "debug"
                        }
                    }
                }
            }
            post {
                always {
                    echo "Configured Build with values:"
                    echo "  Execute Tests ------------------------------- ${env.EXECUTE_TESTS}"
                    echo "  Logging Level ------------------------------- ${env.LOG_LEVEL}"
                    echo "  Deploy Activity Details --------------------- ${env.DEPLOY_ACTIVITY_DETAILS_CF}"
                    echo "  Deploy Completed Events --------------------- ${env.DEPLOY_COMPLETED_EVENTS_CF}"
                    echo "  Deploy Web Scraper -------------------------- ${env.DEPLOY_WEB_SCRAPER_CF}"
                    echo "  Deploy Failed Events ------------------------ ${env.DEPLOY_FAILED_EVENTS_CF}"
                    echo "  Deploy IBC Config Loading ------------------- ${env.DEPLOY_IBC_CONFIG_LOAD_CF}"
                    echo "  Deploy IBC Config Scheduler ----------------- ${env.DEPLOY_IBC_CONFIG_SCHEDULER}"
                    echo "  Deploy Kickfire Load ------------------------ ${env.DEPLOY_KICKFIRE_LOAD_CF}"
                    echo "  Deploy Kickfire Insert ---------------------- ${env.DEPLOY_KICKFIRE_INSERT_CF}"
                    echo "  Deploy Kickfire Load Complete --------------- ${env.DEPLOY_KICKFIRE_LOAD_COMPLETED_CF}"
                    echo "  Deploy Kickfire Complete Check Scheduler ---- ${env.DEPLOY_KICKFIRE_COMPLETE_CHECK_SCHEDULER}"
                    echo "  Deploy Storage Configs ---------------------- ${env.DEPLOY_STORAGE_CONFIGS_CF}"
                    echo "  Deploy Maxmind Load ------------------------- ${env.DEPLOY_MAXMIND_LOAD_CF}"
                    echo "  Deploy Maxmind Insert ----------------------- ${env.DEPLOY_MAXMIND_INSERT_CF}"
                    echo "  Deploy Maxmind Load Complete ---------------- ${env.DEPLOY_MAXMIND_LOAD_COMPLETED_CF}"
                    echo "  Deploy Maxmind Complete Check Scheduler ----- ${env.DEPLOY_MAXMIND_COMPLETE_CHECK_SCHEDULER}"

                    // for debugging contents of workspace
                    // sh "ls"
                }
            }
        }
        
        stage ('Unit Testing') {
            when {
                expression { return env.EXECUTE_TESTS == 'true' && env.ABORT_BUILD == 'false' }
            }
            steps {
                script {
                    def prepVenv = 'cd ${WORKSPACE}'
            
                    def current_cf = 'gcf_common'
                    prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                    env.TEST_STEPS_COMMON_LIB = buildTestCommands(current_cf)
                    
                    env.TEST_STEPS_ACTIVITY_DETAILS_CF = "echo 'Skipped Activity Details Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_ACTIVITY_DETAILS_CF == 'true') {
                        current_cf = 'activity_details_resolution_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_ACTIVITY_DETAILS_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_COMPLETED_EVENTS_CF = "echo 'Skipped Completed Events Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_COMPLETED_EVENTS_CF == 'true') {
                        current_cf = 'completed_events_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_COMPLETED_EVENTS_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_WEB_SCRAPER_CF = "echo 'Skipped Web Scraper Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_WEB_SCRAPER_CF == 'true') {
                        current_cf = 'web_scraper_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_WEB_SCRAPER_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_FAILED_EVENTS_CF = "echo 'Skipped Failed Events Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_FAILED_EVENTS_CF == 'true') {
                        current_cf = 'failed_events_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_FAILED_EVENTS_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_KICKFIRE_LOAD_CF = "echo 'Skipped Kickfire Load Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_KICKFIRE_LOAD_CF == 'true') {
                        current_cf = 'kickfire_load_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_KICKFIRE_LOAD_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_KICKFIRE_INSERT_CF = "echo 'Skipped Kickfire Insert Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_KICKFIRE_INSERT_CF == 'true') {
                        current_cf = 'kickfire_insert_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_KICKFIRE_INSERT_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_KICKFIRE_LOAD_COMPLETED_CF = "echo 'Skipped Kickfire Load Completed Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_KICKFIRE_LOAD_COMPLETED_CF == 'true') {
                        current_cf = 'kickfire_load_completed_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_KICKFIRE_LOAD_COMPLETED_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_MAXMIND_LOAD_CF = "echo 'Skipped Maxmind Load Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_MAXMIND_LOAD_CF == 'true') {
                        current_cf = 'maxmind_load_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_MAXMIND_LOAD_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_MAXMIND_INSERT_CF = "echo 'Skipped Maxmind Insert Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_MAXMIND_INSERT_CF == 'true') {
                        current_cf = 'maxmind_insert_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_MAXMIND_INSERT_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_MAXMIND_LOAD_COMPLETED_CF = "echo 'Skipped Maxmind Load Completed Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_MAXMIND_LOAD_COMPLETED_CF == 'true') {
                        current_cf = 'maxmind_load_completed_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_MAXMIND_LOAD_COMPLETED_CF = buildTestCommands(current_cf)
                    }
                    
                    env.TEST_STEPS_STORAGE_CONFIGS_CF = "echo 'Skipped Storage Configs Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_STORAGE_CONFIGS_CF == 'true') {
                        current_cf = 'storage_configs_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_STORAGE_CONFIGS_CF = buildTestCommands(current_cf)
                    }

                    env.TEST_STEPS_IBC_CONFIG_LOAD_CF = "echo 'Skipped IBC Config Loading Tests'"
                    if (env.EXECUTE_TESTS_IF_NOT_DEPLOYING == 'true' || env.DEPLOY_IBC_CONFIG_LOAD_CF == 'true') {
                        current_cf = 'ibc_config_load_cf'
                        prepVenv += ' && ' + buildCreatePythonVenv(current_cf)
                        env.TEST_STEPS_IBC_CONFIG_LOAD_CF = buildTestCommands(current_cf)
                    }

                    env.PREP_TEST_VENVS = prepVenv
                }
		
                // prep all of the venv folders for cloud functions being tested
                echo "Preparing virtual environments for tests."
                sh("${env.PREP_TEST_VENVS}")
                
                // test common lib
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_COMMON_LIB}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_COMMON_LIB}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_ACTIVITY_DETAILS_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_ACTIVITY_DETAILS_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_COMPLETED_EVENTS_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_COMPLETED_EVENTS_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_WEB_SCRAPER_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_WEB_SCRAPER_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_FAILED_EVENTS_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_FAILED_EVENTS_CF}")
                }

                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_IBC_CONFIG_LOAD_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_IBC_CONFIG_LOAD_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_KICKFIRE_LOAD_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_KICKFIRE_LOAD_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_KICKFIRE_INSERT_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_KICKFIRE_INSERT_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_KICKFIRE_LOAD_COMPLETED_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_KICKFIRE_LOAD_COMPLETED_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_MAXMIND_LOAD_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_MAXMIND_LOAD_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_MAXMIND_INSERT_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_MAXMIND_INSERT_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_MAXMIND_LOAD_COMPLETED_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_MAXMIND_LOAD_COMPLETED_CF}")
                }
                
                // test each cloud function
                catchError(buildResult: 'FAILURE', stageResult: 'FAILURE') {
                    echo "Executing Tests using setup and steps: ${env.TEST_STEPS_STORAGE_CONFIGS_CF}"

                    // must be last command to return sh exit value
                    sh("${env.TEST_STEPS_STORAGE_CONFIGS_CF}")
                }
                
            }
            post {
                always {
                    echo "Completed with Unit Tests"
                    // cleanWs cleanWhenFailure: false, cleanWhenUnstable: false, deleteDirs: true, notFailBuild: true, disableDeferredWipeout: true
                }
                success {
                    echo "Unit Tests - Successful"
                    script {
                        env.ABORT_BUILD = 'false'
                    }
                }
                failure {
                    echo "Unit Tests - Failed - aborting build"
                    script {
                        env.ABORT_BUILD = 'true'
                    }
                }
                aborted {
                    echo "Unit Tests - Aborted - aborting build"
                    script {
                        env.ABORT_BUILD = 'true'
                    }
                }
                cleanup {
                    echo "Cleanup after Unit Tests - change to workspace dir"
                    sh "cd ${WORKSPACE}"
                }
            }
        }

        stage ('Deploy') {
            when {
                expression { return env.ABORT_BUILD == 'false' }
            }
            stages {
                stage ('Activate SA') {
                    steps {
                        sh("gcloud config set account ${env.runner_account}")
                    }
                }
                stage ('Deploy Cloud Functions') {
                    parallel {
                        stage ("Deploy Activity Details CF") {
                            when {
                                expression { return env.DEPLOY_ACTIVITY_DETAILS_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy activity_details_resolution_cf --project ${env.project} --region=${env.region} --vpc-connector ${ENVIRONMENT}-serverless-connector --egress-settings=private-ranges-only --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/activity_details_resolution_cf --trigger-topic ibc.client-activity-input --entry-point process_event --stage-bucket ${env.storage_bucket} --memory=${env.memory} --max-instances=${env.activity_cf_max_instances} --set-env-vars PROJECT=${env.project},EDC_REST_URL=${EDC_REST_URL},GLOBAL_WEBSCRAPING_ENABLED=${GLOBAL_WEBSCRAPING_ENABLED},LOG_LEVEL=${env.LOG_LEVEL} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Completed Events CF") {
                            when {
                                expression { return env.DEPLOY_COMPLETED_EVENTS_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy completed_events_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/completed_events_cf --trigger-topic ibc.completed-activities --entry-point write_to_bq --stage-bucket ${env.storage_bucket} --memory=${env.memory} --max-instances=${env.completed_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Web Scraper CF") {
                            when {
                                expression { return env.DEPLOY_WEB_SCRAPER_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy web_scraper_cf --project ${env.project} --region=${env.region} --vpc-connector ${ENVIRONMENT}-serverless-connector --egress-settings=private-ranges-only --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/web_scraper_cf --trigger-topic ibc.events-to-webscrape --entry-point process_webscrape_event --stage-bucket ${env.storage_bucket} --memory=${env.memory} --max-instances=${env.web_scraper_cf_max_instance} --set-env-vars PROJECT=${env.project},EDC_REST_URL=${EDC_REST_URL},STORAGE_BUCKET=${env.storage_bucket},LOG_LEVEL=${env.LOG_LEVEL} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Failed Events CF") {
                            when {
                                expression { return env.DEPLOY_FAILED_EVENTS_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy failed_events_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/failed_events_cf --trigger-topic ibc.failed-events --entry-point handle_failed_event --stage-bucket ${env.storage_bucket} --memory=${env.memory} --max-instances=${env.failed_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy IBC Config Loading CF") {
                            when {
                                expression { return env.DEPLOY_IBC_CONFIG_LOAD_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy ibc_config_load_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/ibc_config_load_cf --trigger-topic ibc.ibc_config_load_trigger --entry-point load_config --stage-bucket ${env.storage_bucket} --memory=${env.memory} --max-instances=${env.ibc_config_load_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Kickfire Load CF") {
                            when {
                                expression { return env.DEPLOY_KICKFIRE_LOAD_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy kickfire_load_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/kickfire_load_cf --trigger-topic ibc.kickfire-load --entry-point load_kickfire --stage-bucket ${env.storage_bucket} --memory=${env.kickfire_load_cf_memory} --timeout=${env.kickfire_load_cf_timeout} --retry --max-instances=${env.kickfire_load_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True,STORAGE_BUCKET=${env.ibc_configurations_storage_bucket} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Kickfire Insert CF") {
                            when {
                                expression { return env.DEPLOY_KICKFIRE_INSERT_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy kickfire_insert_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/kickfire_insert_cf --trigger-topic ibc.kickfire-insert --entry-point add_to_firestore --stage-bucket ${env.storage_bucket} --memory=${env.kickfire_insert_cf_memory} --timeout=${env.kickfire_insert_cf_timeout} --retry --max-instances=${env.kickfire_insert_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True --update-labels=${env.labels}")
                            }
                        }                        
                        stage ("Deploy Kickfire Load Completed CF") {
                            when {
                                expression { return env.DEPLOY_KICKFIRE_LOAD_COMPLETED_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy kickfire_load_completed_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/kickfire_load_completed_cf --trigger-topic ibc.kickfire-load-completed --entry-point process_event --stage-bucket ${env.storage_bucket} --memory=${env.kickfire_load_completed_cf_memory} --timeout=${env.kickfire_load_completed_cf_timeout} --max-instances=${env.kickfire_load_completed_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Storage Configs CF") {
                            when {
                                expression { return env.DEPLOY_STORAGE_CONFIGS_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy storage_configs_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/storage_configs_cf --trigger-resource ${env.ibc_configurations_storage_bucket} --trigger-event google.storage.object.finalize --entry-point handle_storage_event --stage-bucket ${env.storage_bucket} --memory=${env.storage_configs_cf_memory} --timeout=${env.storage_configs_cf_timeout} --retry --max-instances=${env.storage_configs_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Maxmind Load CF") {
                            when {
                                expression { return env.DEPLOY_MAXMIND_LOAD_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy maxmind_load_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/maxmind_load_cf --trigger-topic ibc.maxmind-load --entry-point load_maxmind --stage-bucket ${env.storage_bucket} --memory=${env.maxmind_load_cf_memory} --timeout=${env.maxmind_load_cf_timeout} --retry --max-instances=${env.maxmind_load_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True,STORAGE_BUCKET=${env.ibc_configurations_storage_bucket} --update-labels=${env.labels}")
                            }
                        }
                        stage ("Deploy Maxmind Insert CF") {
                            when {
                                expression { return env.DEPLOY_MAXMIND_INSERT_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy maxmind_insert_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/maxmind_insert_cf --trigger-topic ibc.maxmind-insert --entry-point add_to_firestore --stage-bucket ${env.storage_bucket} --memory=${env.maxmind_insert_cf_memory} --timeout=${env.maxmind_insert_cf_timeout} --retry --max-instances=${env.maxmind_insert_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True --update-labels=${env.labels}")
                            }
                        }                        
                        stage ("Deploy Maxmind Load Completed CF") {
                            when {
                                expression { return env.DEPLOY_MAXMIND_LOAD_COMPLETED_CF == 'true' }
                            }
                            steps {
                                sh("gcloud alpha functions deploy maxmind_load_completed_cf --project ${env.project} --region=${env.region} --runtime ${env.runtime} --service-account ${env.service_account} --source=${env.source}/maxmind_load_completed_cf --trigger-topic ibc.maxmind-load-completed --entry-point process_event --stage-bucket ${env.storage_bucket} --memory=${env.maxmind_load_completed_cf_memory} --timeout=${env.maxmind_load_completed_cf_timeout} --max-instances=${env.maxmind_load_completed_cf_max_instances} --set-env-vars PROJECT=${env.project},LOG_LEVEL=${env.LOG_LEVEL},CF_ENV=True --update-labels=${env.labels}")
                            }
                        }
                        
                    }
                }
                stage ('Deploy Config Loader Scheduler') {
                    when {
                        expression { env.DEPLOY_IBC_CONFIG_SCHEDULER == 'true' }
                    }
                    steps {
                        sh("gcloud scheduler jobs ${env.scheduler_action} pubsub ibc_config_load_job --project ${env.project} --schedule \"0 * * * *\" --time-zone=\"America/New_York\" --topic ibc.ibc_config_load_trigger --message-body \"{}\" --description \"trigger ibc_config_load_cf CF to update configuration in Firestore\"")
                    }
                }                
                stage ('Deploy Kickfire Complete Check Scheduler') {
                    when {
                        expression { env.DEPLOY_KICKFIRE_COMPLETE_CHECK_SCHEDULER == 'true' }
                    }
                    steps {
                        // scheduled time of execution the hour before caches cleared in cloud function instances to minimize overlap of possibly using both collections
                        // once a new collection becomes active, any new instances of cloud functions would query for active and start using the new active collection
                        // and existing instances would continue using cached collection
                        sh("gcloud scheduler jobs ${env.scheduler_action} pubsub kickfire_complete_check_job --project ${env.project} --schedule \"30 2 * * *\" --time-zone=\"America/New_York\" --topic ibc.kickfire-load-completed --message-body \"{}\" --description \"trigger kickfire_load_completed_cf CF to check for Kickfire load completion and promote to active collection\"")
                    }
                }
                stage ('Deploy Maxmind Complete Check Scheduler') {
                    when {
                        expression { env.DEPLOY_MAXMIND_COMPLETE_CHECK_SCHEDULER == 'true' }
                    }
                    steps {
                        // scheduled time of execution the hour before caches cleared in cloud function instances to minimize overlap of possibly using both collections
                        // once a new collection becomes active, any new instances of cloud functions would query for active and start using the new active collection
                        // and existing instances would continue using cached collection
                        sh("gcloud scheduler jobs ${env.scheduler_action} pubsub maxmind_complete_check_job --project ${env.project} --schedule \"30 2 * * *\" --time-zone=\"America/New_York\" --topic ibc.maxmind-load-completed --message-body \"{}\" --description \"trigger maxmind_load_completed_cf CF to check for Maxmind load completion and promote to active collection\"")
                    }
                }
            }
        }
    }
}

